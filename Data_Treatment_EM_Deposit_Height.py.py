########################################################
# Analysis of the relationship between height of deposit 
# layer and electric conductivity (EM)
# 
# Programm to create a graph of EM mappings
########################################################

def Fusion_Frames (Frame_EM, Frame_Deposit):
    import pandas as pd
    import numpy  as np
    """ Returns a new frame where a column with the data of "Deposit Heights" belonging to Frame_Deposit 
        is added to Frame_EM at the corresponding coordinates (# of filter, X and Y).
        If data is missing, the value is filled with np.nan
        
        Output: Data_EM_DH (pandaDataFrame) "
    """
    # FUSIONING BOTH FRAMES
    # The number of points in both frames is different. 
    # Here I fuse the frames if the coordinates are the same
    # if not a np.nan is stored
    
    # Selecting the columns corresponding to the coordinates
    CoordinatesEM = Frame_EM     [["Filtre_#","x_[m]_by_hand","y_[m]_by_hand"]]
    CoordinatesDP = Frame_Deposit[["Filtre_#","x_[m]_by_hand","y_[m]_by_hand"]]
    # Initializing the new columns Heights and Comments
    Dep_Heights = np.zeros(len(Frame_EM)) # initialize Deposit Heights
    Dep_Heights.fill(np.nan)
    # In this loop I import the values of Height and comment if coordinates are the same
    # I need two for loops to study all possibilies    
    for index1 in CoordinatesEM.index:    
        for index2 in CoordinatesDP.index:
            Same_Coords = CoordinatesEM.ix[index1] == CoordinatesDP.ix[index2] # this is a series-type object
            Same_Coords = Same_Coords.all() #= True if the three (#Filter, X and Y) coordinates are equal
            if Same_Coords:
                Dep_Heights[index1] = Frame_Deposit.ix[index2,["h(cm)"  ]]
                CoordinatesDP       = CoordinatesDP.drop(index2)
                break # break the loop
    # Checking if all deposit depths were imported
    if len(CoordinatesDP) != 0:
        print('WARNING, fuision of data is not complete !')
        print('There are Deposit Heights data whose coordinates are not present in the EM_Data')
    else:
        print('Fusion of data OK!')
    Data_EM_DH            = Frame_EM.copy()
    Data_EM_DH["h(cm)"  ] = Dep_Heights # Deposit Heights
    return(Data_EM_DH)

def Remove_Outliers(Frame):
    """ Discards data points of the PandaDataFrame that were affected by the presence of a metalic (or others)
    object while performing the EM """
    import numpy as np
    # Deleting non consisting values
    Outliers1  = Frame["Inphase_[ppt]"] > Max_Inphase # 2.5 
    Outliers2a = Frame["x_[m]_by_hand"] == 0 # Parce qu'il sont proche d'un talud (surface non plate)
    Outliers2b = Frame["x_[m]_by_hand"] == 8 # Parce qu'il sont proche d'un talud (surface non plate)
    Outliers2  = Outliers2a | Outliers2b   
    Outliers3a = Frame["y_[m]_by_hand"]  == 0
    Outliers3b = Frame["Filtre_#"]       == 1
    Outliers3  = Outliers3a & Outliers3b
    Outliers   = Outliers1 | Outliers2 | Outliers3 # Parce qu'il sont proche d'un talud (surface non plate)
    Frame_Outliers = Frame[Outliers]
    Frame_Good     = Frame.copy()    
    Frame_Good[Outliers] = np.nan
    return(Frame_Good, Frame_Outliers)

     
def Figure_1 (Frame): # SCATTER OF CONDUCTIVITY VS DEPOSIT HEIHGT
    import matplotlib.pyplot as plt
    """ Plots the a scatter of electric conductivity and Deposit Height
        taking all the points into account """
    Figure, Ax = plt.subplots()
    Ax.scatter   (Frame["h(cm)"  ], Frame["Cond_[mS/m]"])
    Ax.set_xlabel('Deposit Height (cm)')
    Ax.set_ylabel('Conductivity (mS/m)')
    Ax.set_title ('Scatter with all points')
    Fig_Name = 'Scatter_Cond_vs_DepHeight_all_points.png' 
    plt.savefig(Fig_Name, dpi = 300)
    plt.close()

def Figure_2 (Frame_Good):# SCATTER OF CONDUCTIVITY VS DEPOSIT HEIHGT
    import matplotlib.pyplot as plt
    """ Plots the a scatter of electric conductivity and Deposit Height
        taking only points that were not affected by the presence of a metalic
        object while performing the EM
        
        Input Frame_Good: is the data frame where outliers are already np.nan"""
        
    Figure, Ax = plt.subplots()
    Ax.scatter   (Frame_Good["h(cm)"  ], Frame_Good["Cond_[mS/m]"])
    Ax.set_xlabel('Deposit Height (cm)')
    Ax.set_ylabel('Conductivity (mS/m)')
    Ax.set_title ('Scatter without outliers points')
    Fig_Name = 'Scatter_Cond_vs_DepHeight_NoOutliers.png' 
    plt.savefig(Fig_Name, dpi = 300)
    plt.close()
    

def Figure_3 (Frame, Filter_num):# 3D SURFACE OF CONDUCTIVITY ALL POINTS
    """ Creates a 3D - Surface Plot of electric conductivity using electric conductivity EM data   """
    import matplotlib.pyplot as plt
    from matplotlib import cm # color maps
    from mpl_toolkits.mplot3d import Axes3D
    Selection = (Frame["Filtre_#"] == Filter_num)
    Frame = Frame[Selection]
    X     = Frame["x_[m]_by_hand"]
    Y     = Frame["y_[m]_by_hand"]
    Z     = Frame["Cond_[mS/m]"  ]
    interpolation = 'linear' # see documentation of scipy.interpolate.Rbf
    X, Y, Z = create_2D_arrays(X,Y,Z, interpolation)
    Fig   = plt.figure() 
    Ax    = Axes3D(Fig)
    Ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.viridis)
    Ax.set_xlabel("X (m)")
    Ax.set_ylabel("Y (m)")
    Ax.set_zlabel("Conductivity mS/m")
    Title = "Conductivity 3d-surface (all points) of filter " + str(Filter_num)
    Ax.set_title(Title)
    Fig.set_size_inches(6,6)
    Fig_Name = Title + '.png' 
    plt.savefig(Fig_Name, dpi = 300)
    plt.close()


def Figure_4(Frame, Filter_num): # 3D SURFACE OF IN-PHASE ALL POINTS
    """ Creates a 3D - Surface Plot of "in-phase" EM data   
        Returns: X,Y,P the grids of the interpolated values of in-phase data
        These grids will be used to create masks of the electric conductivity
        mapping where the in-phase values are "aberrant" (see function: remove outliers)"""
    import matplotlib.pyplot as plt
    from matplotlib import cm # color maps
    from mpl_toolkits.mplot3d import Axes3D        
    # Create an interpolated grid with "in-phase" values
    Selection = (Frame["Filtre_#"] == Filter_num)
    Frame = Frame[Selection]
    X     = Frame["x_[m]_by_hand"]
    Y     = Frame["y_[m]_by_hand"]
    P     = Frame["Inphase_[ppt]"] 
    interpolation = 'linear' # see documentation of scipy.interpolate.Rbf
    X, Y, P = create_2D_arrays(X,Y,P, interpolation)
    # Creating a graph to be sure it is working
    Fig   = plt.figure() 
    Ax    = Axes3D(Fig)
    Ax.plot_surface(X, Y, P, rstride=1, cstride=1, cmap=cm.viridis)
    Ax.set_xlabel("X (m)")
    Ax.set_ylabel("Y (m)")
    Ax.set_zlabel("Inphase_[ppt]")
    Title = "In_phase 3d-surface of filter " + str(Filter_num)
    Ax.set_title(Title)
    Fig_Name = Title + '.png' 
    plt.savefig(Fig_Name, dpi = 300)
    plt.close()
    return (X,Y,P)

def Figure_5(Frame, P, Filter_num): 
    # 3D SURFACE OF CONDUCTIVITY NO OUTLIERS
    # and map of conductivity
    
    """ Creates 2 figures (3D-surface and Contours) for the mapping 
        of electric conductivity taking off outliers (see as well the function
        Remove_Outliers """
    import matplotlib.pyplot as plt
    from matplotlib import cm # color maps
    from mpl_toolkits.mplot3d import Axes3D
    import numpy as np
    plt.close("all")
    # Create an interpolated grid with "electric conductity" values
    Selection = (Frame["Filtre_#"] == Filter_num)
    Frame = Frame[Selection]
    X     = Frame["x_[m]_by_hand"]
    Y     = Frame["y_[m]_by_hand"]
    C     = Frame["Cond_[mS/m]"] 
    interpolation = 'linear' # see documentation of scipy.interpolate.Rbf
    X, Y, C = create_2D_arrays(X,Y,C, interpolation)
    
    Outliers_Inphase    = (P > Max_Inphase)
    C[Outliers_Inphase] = np.nan
    
    Outliers_X0    = (X <= 0.5) | (X > 7.5) # points near the left border
    C[Outliers_X0] = np.nan
    
    if Filter_num == 1:
        Outliers_Y0    = (Y <= 0.5)
        C[Outliers_Y0] = np.nan

    if Filter_num == 2:
        Outliers_anomalie    = (Y <= 3) & (X >= 5)
        C[Outliers_anomalie] = np.nan
    
    Cond_ref = np.nanmean(C)
    print("Cond_ref = ", Cond_ref)
    
    # compting percentage of the surface that is 'homogeneous'
    homogeneus_surface_1 = np.count_nonzero((C > Cond_ref-1) & (C < Cond_ref+1))
    all_surface = np.count_nonzero(~np.isnan(C))
    percentage = homogeneus_surface_1/all_surface
    print(percentage)
    
    homogeneus_surface_2 = np.count_nonzero((C > Cond_ref-2) & (C < Cond_ref+2))
    all_surface = np.count_nonzero(~np.isnan(C))
    percentage = homogeneus_surface_2/all_surface
    print(percentage)
    
    homogeneus_surface_3 = np.count_nonzero((C > Cond_ref-3) & (C < Cond_ref+3))
    all_surface = np.count_nonzero(~np.isnan(C))
    percentage = homogeneus_surface_3/all_surface
    print(percentage)
    
    
    # Creating a graph to be sure it is working
    Fig   = plt.figure() 
    Ax    = Axes3D(Fig)
    Ax.plot_surface(X, Y, C, rstride=1, cstride=1, cmap=cm.viridis)
    Ax.set_xlabel("X (m)")
    Ax.set_ylabel("Y (m)")
    Ax.set_zlabel("Conductivuty (mS/m)")
    Title = "Electric Conductivity surface without outliers of filter " + str(Filter_num)
    Ax.set_title(Title)
    Fig_Name = Title + '.png'
    chdir("/home/german.martinez-carvajal/Desktop/These/EM_Electromag_multifrequentiel/180329_Montromant_EM/Data_Treatment")
    plt.savefig(Fig_Name, dpi = 300)
    plt.close()

    Fig1, Ax1 = plt.subplots()
    l_min = np.round(np.nanmin(C), decimals = 0)
    l_max = np.round(np.nanmax(C), decimals = 0)
    Levels = np.arange(l_min, l_max + 1 , 1) #  for the contour plot
    Levels = np.round(Levels, decimals = 0)
    CS1 = Ax1.contourf(X,Y,C, Levels, cmap=cm.jet) # contour filled
    CS2 = Ax1.contour (X,Y,C, Levels, colors='k', linewidths = 0.3)
    Ax1.set_xlabel("X (m)")
    Ax1.set_ylabel("Y (m)")   
    plt.clabel(CS2, colors='k', fontsize=6, fmt = '%1.0f')
    cbar= plt.colorbar(CS1) # color bar
    cbar.ax.set_ylabel('Conductivity (mS/m)')
    Title = 54*" " + "$\overline{\sigma}\pm\Delta\sigma \Rightarrow $ 74%$_{Filter's surface}$\n"
    Title = Title + "$\overline{\sigma} = 15 mS/m$,   $\Delta\sigma = 1 mS/m$" + 10*" "+ "$\sigma\pm2\Delta\sigma \Rightarrow $ 95%$_{Filter's surface}$\n"
    Title = Title + 52*" " + "$\overline{\sigma}\pm3\Delta\sigma \Rightarrow $ 98%$_{Filter's surface}$"
    Ax1.set_title(Title)
    Title = "Electric Conductivity map without outliers of filter " + str(Filter_num)
    Fig_Name = Title + '.png' 

    Ax1.set_xlabel("X (m)")
    Ax1.set_ylabel("Y (m)")   
    Ax1.set_xlim(0,8)
    Ax1.set_ylim(0,8)
    if Filter_num == 2:
        # ading lables of the sampling points
        #bbox_props = dict(boxstyle="circle", fc="w", ec="orange", alpha=0.8)
        #Ax1.text(4, 2, "Samples\n1A, 1B", ha="center", va="center", size=8, bbox=bbox_props)
        #Ax1.text(6, 4, "Samples\n2A, 2B", ha="center", va="center", size=8, bbox=bbox_props)
        #Ax1.text(4, 7, "Samples\n3A, 3B", ha="center", va="center", size=8, bbox=bbox_props)
        bbox_props = dict(boxstyle="round", fc="w", ec='w')
        Ax1.text(0.5, 4, "Border artefact exclusion", ha="center", va="center", size=10, bbox=bbox_props, rotation=90)
        Ax1.text(7.5, 4, "Border artefact exclusion", ha="center", va="center", size=10, bbox=bbox_props, rotation=270)
        Ax1.text(6.5, 1.0, "Influence zone of \nmetallic water collector ", ha="center", va="center", size=8, bbox=bbox_props, rotation=0)
        
        bbox_props = dict(boxstyle="circle", fc="black", alpha = 1, ec="black")
        Ax1.text(2, 2.0, "FP", ha="center", va="center", size=8, bbox=bbox_props, zorder = 1, alpha = 1.0, color = 'w', weight = "bold")
        Ax1.text(2, 6.5, "FP", ha="center", va="center", size=8, bbox=bbox_props, zorder = 1, alpha = 1.0, color = 'w', weight = "bold")
        Ax1.text(6, 6.5, "FP", ha="center", va="center", size=8, bbox=bbox_props, zorder = 1, alpha = 1.0, color = 'w', weight = "bold")
        Ax1.text(6, 2.0, "FP", ha="center", va="center", size=8, bbox=bbox_props, zorder = 1, alpha = 1.0, color = 'w', weight = "bold")
    
    if Filter_num == 2:
        # ading lables of the sampling points
        bbox_props = dict(boxstyle="circle", fc="w", ec="orange", alpha=0.9)
        Ax1.text(4, 2, "Samples\n1A, 1B", ha="center", va="center", size=8, bbox=bbox_props)
        Ax1.text(6, 4, "Samples\n2A, 2B", ha="center", va="center", size=8, bbox=bbox_props)
        Ax1.text(4, 7, "Samples\n3A, 3B", ha="center", va="center", size=8, bbox=bbox_props)    
    chdir("/home/german.martinez-carvajal/Desktop/These/EM_Electromag_multifrequentiel/180329_Montromant_EM/Data_Treatment")
    Fig1.set_size_inches(20/2.54, 16/2.54)    
    Fig1.savefig(Fig_Name, dpi = 300)
    
    plt.close()
    
    
    return (X,Y,C)
        
def Figure_6(X,Y,C, Filter_num):
    """ Plots contours of humidity variation as a ratio """
    # HRV = Humidity variation - calculated according Archie's law
    # Assuming n = uniform over the surface : warning: terrible assumption
    # Setting a reference value of conductivity
    import matplotlib.pyplot as plt
    from   matplotlib import cm # color maps
    import numpy as np
    
    Cond_ref = np.nanmean(C)
    print("Cond_ref = ", Cond_ref)
    d        = 2 # saturation inndex for consolidated rocks
    global HVR # Humidity variation
    HVR      = (C/Cond_ref)**(1/d)
    global a
    a = (C/Cond_ref)
    N        = 6 # number of contour zones
    global Levels
    Levels = np.linspace(np.nanmin(HVR), np.nanmax(HVR), N) #  for the contour plot
    Levels = np.round(Levels, decimals = 1)
    Levels = np.round(np.append(np.linspace(0.8,1,int(N/2)), np.linspace(1,1.2,int(N/2))[1:]), decimals = 3)
    
    Fig1, Ax1 = plt.subplots()
    CS1 = Ax1.contourf(X,Y,HVR, Levels, cmap=cm.viridis, extend = 'both') # contour filled
    if N <= 10:
        CS2 = Ax1.contour (X,Y,HVR, Levels, colors='k')
        plt.clabel(CS2, colors='k', fontsize=7, fmt = '%1.1f')
    
    cbar= plt.colorbar(CS1) # color bar
    if N >= 10:
        cbar.set_ticklabels(np.round(np.append(np.linspace(0.8,1,5), np.linspace(1,1.2,5)[1:]), decimals = 3))
        
    cbar.ax.set_ylabel(r'Saturation($S/S_{ref}$)$_{estimated}$')
    Ax1.set_xlabel("X (m)")
    Ax1.set_ylabel("Y (m)")   
    Ax1.set_xlim(0,8)
    Ax1.set_ylim(0,8)
    
    Fig_Name = "Map_of_Delta_S_of filter_{}_num_czones_{}.png".format(Filter_num, N) 
    
    # adding sampling points 
    if Filter_num == 2:
        # ading lables of the sampling points
        bbox_props = dict(boxstyle="circle", fc="w", ec="orange", alpha=0.8)
        Ax1.text(4, 2, "Samples\n1A, 1B", ha="center", va="center", size=8, bbox=bbox_props)
        Ax1.text(6, 4, "Samples\n2A, 2B", ha="center", va="center", size=8, bbox=bbox_props)
        Ax1.text(4, 7, "Samples\n3A, 3B", ha="center", va="center", size=8, bbox=bbox_props)
        bbox_props = dict(boxstyle="round", fc="w", ec='w', alpha=0.5)
        Ax1.text(0.5, 4, "Border artefact exclusion", ha="center", va="center", size=10, bbox=bbox_props, rotation=90)
        Ax1.text(7.5, 4, "Border artefact exclusion", ha="center", va="center", size=10, bbox=bbox_props, rotation=270)
        Ax1.text(6.5, 0.5, "Influence zone of \nmetallic water collector ", ha="center", va="center", size=8, bbox=bbox_props, rotation=0)
        
        # compting percentage of the surface that is 'homogeneous'
        homogeneus_surface = np.count_nonzero(np.logical_and(HVR >= 0.9 , HVR <= 1.1))
        all_surface = np.count_nonzero(~np.isnan(HVR))
        percentage = homogeneus_surface/all_surface
        print(percentage)
        
        bbox_props = dict(boxstyle="circle", fc="r", ec="black", alpha=1)
        Ax1.text(2, 2, "FP", ha="center", va="center", size=8, bbox=bbox_props)
        Ax1.text(2, 6, "FP", ha="center", va="center", size=8, bbox=bbox_props)
        Ax1.text(6, 6, "FP", ha="center", va="center", size=8, bbox=bbox_props)
        Ax1.text(6, 2, "FP", ha="center", va="center", size=8, bbox=bbox_props)
    plt.savefig(Fig_Name, dpi = 300)
    #plt.close()
    
def create_2D_arrays(X,Y,Z, interpolation):
    """Transforms lists of values X,Y Z, into 2D arrays, using interpolation
    to use them when you want to draw 3D plots or contours, 
    See: https://stackoverflow.com/questions/9008370/python-2d-contour-plot-from-3-lists-x-y-and-rho   """
    import scipy.interpolate
    import numpy as np
    # Set up a regular grid of interpolation points
    xi, yi = np.linspace(X.min(), X.max(), 100), np.linspace(Y.min(), Y.max(), 100)
    xi, yi = np.meshgrid(xi, yi)
    # Interpolate
    rbf = scipy.interpolate.Rbf(X, Y, Z, function= interpolation)
    zi = rbf(xi, yi)
    return (xi, yi, zi)

#################################################################################
## MAIN
#################################################################################
## Reading File frame with EM data (Carte à Electromagnetisme Multifrequentiel d'un FPR Filtre Planté des Roseaux)
#import pandas as pd
#File_Path = '/home/german.martinez-carvajal/Desktop/These/EM_Electromag_multifrequentiel/180329_Montromant_EM/donees_EM_pretreated'
#File_Name = File_Path + "/Data_Frame_EM_points.csv"
#Frame_EM  = pd.read_csv(File_Name, sep = ',')
## Reading Data frame with Deposit Height Data (Data noted by hand in the same FPR )
#File_Path = '/home/german.martinez-carvajal/Desktop/These/EM_Electromag_multifrequentiel/180329_Montromant_EM/Hauteurs_depot'
#File_Name = File_Path + "/Hauteur_depot.csv"
#Frame_Deposit = pd.read_csv(File_Name, sep = ',')
#
## Creating the new frame and saving it
#print("Fusing EM and Deposit Height data")  
#Frame_EM_DH = Fusion_Frames (Frame_EM, Frame_Deposit)
#from os import chdir
#File_Path = '/home/german.martinez-carvajal/Desktop/These/EM_Electromag_multifrequentiel/180329_Montromant_EM/Data_Treatment'
#chdir       (File_Path)
#Frame_EM_DH.to_csv('Frame_EM&DepHeight.csv') # EM electromagnetic methods mapping, DH = deposit heght
#
## Making a small change in the x coordinate (so the figures will be understood better)
#Frame_EM_DH["x_[m]_by_hand"] = Frame_EM_DH["x_[m]_by_hand"].max() -  Frame_EM_DH["x_[m]_by_hand"]
#
## Figure 1 - Scatter (Conductivty vs Dep Height) with all points
#import matplotlib.pyplot as plt
#plt.close("all")
#Figure_1(Frame_EM_DH)
#
## Figure 2 - Scatter (Conductivty vs Dep Height) without outliers
#global Max_Inphase
#Max_Inphase = 2.5 # Points with in_phase above the max will be discarded. See the funtion Remove_outliers
#Frame_EM_DH_Good, Frame_EM_DH_Outliers = Remove_Outliers(Frame_EM_DH)
#Figure_2(Frame_EM_DH_Good) # calling my function
#
## Figure 3 - Surface plots of electric conductivity (all points)
#Figure_3(Frame_EM_DH, Filter_num = 1)
#Figure_3(Frame_EM_DH, Filter_num = 2)
#
## Figure 4 - Surface plots of in-phase (phase of the EM map) (all points)
#X1,Y1,P1 = Figure_4(Frame_EM_DH, Filter_num = 1) # P means inPhase
#X2,Y2,P2 = Figure_4(Frame_EM_DH, Filter_num = 2)

# Figure 5 - Surface plots of electric conductivity without outliers
#X1,Y1,C1 = Figure_5(Frame_EM_DH_Good , P1, Filter_num = 1 ) # C means conductivity
X2,Y2,C2 = Figure_5(Frame_EM_DH_Good , P2, Filter_num = 2 )

# Figure  - Mapping humidity variation
File_Path = '/home/german.martinez-carvajal/Desktop/These/EM_Electromag_multifrequentiel/180329_Montromant_EM/Data_Treatment'
chdir       (File_Path)
#Figure_6(X1,Y1,C1,Filter_num = 1)
Figure_6(X2,Y2,C2,Filter_num = 2)
#plt.close('all')
print('finished')



