from __future__ import division
import math
from ij import IJ
from ij.io import FileSaver

def straighten(path, file_name, z0,x0_corner, y0_corner, z1, x1_corner, y1_corner, d ):
	
	# Computing centers
	# Actually, this stage is not necessary because d/2 will dissapear later , but its more clear like this
	x0,y0,z0 = x0_corner + d/2, y0_corner+ d/2 , z0
	x1,y1,z1 = x1_corner + d/2, y1_corner+ d/2 , z1
	print('Center 0 : x0, y0, z0 ', x0,y0,z0)
	print('Center 1 : x1, y1, z1 ', x1,y1,z1)
	
	alpha = math.atan((x1-x0)/(z1-z0))*360/(2*math.pi)
	beta = math.atan((y1-y0)/(z1-z0))*360/(2*math.pi)
	
	print('alpha =', alpha)
	print('beta  =', beta )

	print('Opening file: ' + file_name)
	image = IJ.openImage(file_name)
	image.show()

	IJ.run("Select None") #  no ROI must be active before running the command reslince
	
	print("first rotation")
	# Running reslice
	IJ.run(image , "Reslice [/]...", "output=1.000 start=Top rotate");
	# The command "Reslice" will create a new Image Plus Object, so you have to create a new pointer
	first_reslice = IJ.getImage()
	
	# Retrieving RAM
	image.close()
	# Rotation of -alpha
	IJ.run(first_reslice,"Rotate... ", "angle={} grid=1 interpolation=Bilinear stack".format(-alpha))
	
	print("second rotation")
	# Running reslice 
	IJ.run(first_reslice , "Reslice [/]...", "output=1.000 start=Top rotate")
	# The command "Reslice" will create a new Image Plus Object, so you have to create a new pointer
	second_reslice = IJ.getImage()
	
	# Retrieving RAM
	first_reslice.changes = False # To avoid the "Save changes?" dialog box
	first_reslice.close()
	# Rotation of beta
	IJ.run(second_reslice,"Rotate... ", "angle={} grid=1 interpolation=Bilinear stack".format(beta))
	
	# Running reslice 
	IJ.run(second_reslice , "Reslice [/]...", "output=1.000 start=Top rotate")
	# The command "Reslice" will create a new Image Plus Object, so you have to create a new pointer
	image_straight = IJ.getImage()
	# Retrieving RAM
	second_reslice.changes = False # To avoid the "Save changes?" dialog box
	second_reslice.close()
	
	print('Saving')
	fs = FileSaver(image_straight)
	filepath = path + "\Image_straight.tif"
	fs.saveAsTiff(filepath)
	image_straight.close()
	
	print("finished")

# MAIN
# Data of a circular section at the top
z0 = 914
x0_corner = 17
y0_corner = 72
# Data of a circular section at the top
z1 = 5020
x1_corner = 62
y1_corner = 20
# Diameter of the circles (is must be the same for both)
d = 1366
# File
path         = "C:\Users\german.martinez-carv\Desktop\_2018_07_23_MON3A"
file_name = path + "\MON3A_20180723_norm.tif"

# streighten
straighten(path, file_name, z0,x0_corner, y0_corner, z1, x1_corner, y1_corner, d )


