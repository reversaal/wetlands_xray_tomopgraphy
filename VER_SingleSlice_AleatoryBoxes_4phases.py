import tifffile
import os
from   os import chdir
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from   numpy.random import random_integers as rd_int
from matplotlib.patches import Rectangle

def sector_mask(shape,centre,radius,angle_range):
    """
    CODE FROM STACK OVER FLOW
    Return a boolean mask for a circular sector. The start/stop angles in  
    `angle_range` should be given in clockwise order.
    """

    x,y = np.ogrid[:shape[0],:shape[1]]
    cx,cy = centre
    tmin,tmax = np.deg2rad(angle_range)

    # ensure stop angle > start angle
    if tmax < tmin:
            tmax += 2*np.pi

    # convert cartesian --> polar coordinates
    r2 = (x-cx)*(x-cx) + (y-cy)*(y-cy)
    theta = np.arctan2(x-cx,y-cy) - tmin

    # wrap angles between 0 and 2*pi
    theta %= (2*np.pi)

    # circular mask
    circmask = r2 <= radius*radius

    # angular mask
    anglemask = theta <= (tmax-tmin)

    return circmask*anglemask
    
def relabel_the_outside(image,labels):
    """ if pixels outside the cylinder that contains the sample have the same value for air
        they will be relabeled with the value label[1]
        so, if you follow the conventions for label values
        label [0]= 1 = pixels outside the cylinder, 
        label [1]= 0 = air (i.e voids) pixels
        label [2]= 128 = fouling material
        label [3]= 255 = gravel
    """
    assert (np.count_nonzero(image == labels[0]) == 0 ), 'You must reserve label[0] = 1, for pixels outside the cylinder'
    print("relabeling from 0,128,255 to 0,1,1298255")

    # CREATING CIRCULAR MASK
    # The cylinder is supposed to be the biggest "circle" inscribed in a squared image
    x,y = np.shape(image)[0], np.shape(image)[1]
    center = int(x/2), int(y/2)
    radius = int(x/2)
    #Selecting circular ROI (region of interest)
    mask_2D = sector_mask((x,y),center,radius,(0,360 ))       
    image[np.logical_not(mask_2D)] =  labels[0] # outside the cilinder label = 1
    print("{} pixels have been attributed to the phase : outside the cylinder".format(np.count_nonzero(image == 1)))
    return image
    
def graph_r_std_vs_box_size(Box_lenghts_Rel, Std_Air_R, Std_Reed_R, Std_OrM_R, Std_Grv_R, slice_2D, dsave, num_slice, n_box, mode_edges, resolution ):
    plt.close('all')
    # Plotting (Relative standard deviations VS Box lenght)
    plt.figure()
    plt.plot(Box_lenghts_Rel,Std_Air_R,color="b",lw=1, label = "Air", linestyle = '-', marker = '*')
    plt.plot(Box_lenghts_Rel,Std_OrM_R,color="r",lw=1, label = "OrM", linestyle = '-', marker = '*')
    plt.plot(Box_lenghts_Rel,Std_Grv_R,color="k",lw=1, label = "Grv", linestyle = '-', marker = '*')
    plt.plot(Box_lenghts_Rel,Std_Reed_R,color="g",lw=1, label = "Reeds", linestyle = '-', marker = '*')

    title = "VER_Aleatory_Boxes_Slice_" + str(num_slice)
    title = title + "_N_boxes_" + str(n_box)
    title = title + "_Edges_mode_" + mode_edges
    title = title + "_TypeGraph_R_Std"
    annotation = "Region's lenght (mm) = " + str(int(len(slice_2D)*resolution))
    plt.xlabel('Relative Box Length - ' + annotation )
    plt.xlim(0)
    ylabel = "Relative Standard deviation - # Boxes = " + str(n_box)
    plt.ylabel(ylabel)
    plt.ylim(0)
    plt.minorticks_on()
    plt.grid(which = 'both')
    plt.legend(fontsize = "small", loc = 'best')
    chdir(dsave)
    plt.savefig(title +'.png')

def graph_std_vs_box_size(Box_lenghts_Rel, Std_Air, Std_Reed, Std_OrM, Std_Grv, slice_2D, dsave, num_slice, n_box, mode_edges, resolution ):
    # Plotting (standard deviations VS Box lenght)
    plt.close('all')
    plt.figure()
    plt.plot(Box_lenghts_Rel,Std_Air,color="b",lw=1, label = "Air", linestyle = '-', marker = '*')
    plt.plot(Box_lenghts_Rel,Std_OrM,color="r",lw=1, label = "OrM", linestyle = '-', marker = '*')
    plt.plot(Box_lenghts_Rel,Std_Grv,color="k",lw=1, label = "Grv", linestyle = '-', marker = '*')
    plt.plot(Box_lenghts_Rel,Std_Reed,color="g",lw=1, label = "Reeds", linestyle = '-', marker = '*')

    title = "VER_Aleatory_Boxes_Slice_" + str(num_slice)
    title = title + "_N_boxes_" + str(n_box)
    title = title + "_Edges_mode_" + mode_edges
    title = title + "_TypeGraph_Std"
    annotation = "Region's lenght (mm) = " + str(int(len(slice_2D)*resolution))
    plt.xlabel('Relative Box Length - ' + annotation )
    plt.xlim(0)
    ylabel = "Standard deviation - # Boxes = " + str(n_box)
    plt.ylabel(ylabel)
    plt.ylim(0, 0.5)
    plt.minorticks_on()
    plt.grid(which = 'both')
    plt.legend(fontsize = "small", loc = 'best')
    chdir(dsave)        
    plt.savefig(title +'.png')

def graph_percentiles_vs_box_size(Box_lenghts_Rel, P25, P75, P50, slice_2D, dsave, num_slice, n_box, mode_edges, resolution):        
    plt.close('all')        
    # Plotting (Percentiles VS Box lenght)
    labels = ["Air", "Reeds", "FM","Grv"]
    colors = ["b"  ,"g"  ,"r", 'k' ]
    plt.figure()        
    for i in range(4): # 0 = Air, 1 = OrM, 2 = Grv
        plt.plot(Box_lenghts_Rel, P25[:,i], color = colors[i], lw=1, linestyle = '-')
        plt.plot(Box_lenghts_Rel, P75[:,i], color = colors[i], lw=1, linestyle = '-')
        plt.plot(Box_lenghts_Rel, P50[:,i], color = colors[i], lw=2, linestyle = ':', label = "Percentile 50 of " + labels[i] + " v/v")
        plt.fill_between(Box_lenghts_Rel,P25[:,i],P75[:,i], color = colors[i], alpha = 0.5)
    
    title = "VER_Aleatory_Boxes_Slice_" + str(num_slice)
    title = title + "_N_boxes_" + str(n_box)
    title = title + "_Edges_mode_" + mode_edges
    title = title + "_TypeGraph_Percentiles"
    annotation = "Region's lenght (mm) = " + str(int(len(slice_2D)*resolution))
    plt.xlabel('Relative Box Length - ' + annotation)
    plt.xlim(0)
    ylabel = "Volume Fraction \n # Boxes = " + str(n_box)
    plt.ylabel(ylabel)
    plt.ylim(0, 1)
    plt.minorticks_on()
    plt.grid(which = 'both')
    plt.legend(fontsize = "small", loc = 'best')
    chdir(dsave)        
    plt.savefig(title +'.png')
    

def graph_mu_and_std_vs_box_size(Box_lenghts_Rel, Mu_minus_Std, Mu_plus__Std, Means, slice_2D, dsave, num_slice, n_box, mode_edges, resolution):        
    plt.close('all')        
    # Plotting (Mu +/- std VS Box lenght)
    labels = ["Air","Reeds","FM","Grv"]
    colors = ["b"  ,"g", "r"  ,"k"  ]
    plt.figure()        
    for i in range(4): # 0 = Air, 1 = OrM, 2 = Grv
        plt.plot(Box_lenghts_Rel, Mu_minus_Std[:,i], color = colors[i], lw=1, linestyle = '-')
        plt.plot(Box_lenghts_Rel, Mu_plus__Std[:,i], color = colors[i], lw=1, linestyle = '-')
        plt.plot(Box_lenghts_Rel, Means       [:,i], color = colors[i], lw=2, linestyle = ':', label = 'Mean of ' + labels[i] + " v/v")
        plt.fill_between(Box_lenghts_Rel,Mu_minus_Std[:,i],Mu_plus__Std[:,i], color = colors[i], alpha = 0.3)
    
    title = "VER_Aleatory_Boxes_Slice_" + str(num_slice)
    title = title + "_N_boxes_" + str(n_box)
    title = title + "_Edges_mode_" + mode_edges
    title = title + "_TypeGraph_mu_and_std"
    annotation = "Region's lenght (mm) = " + str(int(len(slice_2D)*resolution))
    plt.xlabel('Relative Box Length - ' + annotation )
    plt.xlim(0)
    ylabel = "Volume Fraction \n # Boxes = " + str(n_box)
    plt.ylabel(ylabel)
    plt.ylim(0, 1)
    plt.minorticks_on()
    plt.grid(which = 'both')
    plt.legend(fontsize = "small", loc = 'best')
    chdir(dsave)
    plt.savefig(title +'.png')
        
####################################################################

        
def Compute_Volume_Fractions (box):
    Out_Voxels  = np.count_nonzero((box==1 ))
    Air_Voxels  = np.count_nonzero((box==0 ))  #Air
    OrM_Voxels  = np.count_nonzero((box==128)) #OrM
    Grv_Voxels  = np.count_nonzero((box==255)) #Grv
    Reed_Voxels  = np.count_nonzero((box==64 ))

    Test =  ( np.product(box.shape) == ( Out_Voxels + Air_Voxels + OrM_Voxels + Grv_Voxels + Reed_Voxels) )   

    if not (Test == True):
        raise AssertionError("You must verify that all phase-labels are taken into account")
    
    Volume      =  Air_Voxels + OrM_Voxels + Grv_Voxels + Reed_Voxels
    VF_Air, VF_Reed, VF_OrM, VF_Grv = Air_Voxels/Volume, Reed_Voxels/Volume, OrM_Voxels/Volume, Grv_Voxels/Volume
    return (VF_Air, VF_Reed, VF_OrM, VF_Grv)

####################################################################
####################################################################

def Make_Simetric_Image_2D (Box):
    Box = np.array(Box)
    Test =    (Box.shape[0] == Box.shape[1])
    if not (Test == True):
        raise AssertionError("2D array should have equal dimensions")
    SymBox = np.zeros(np.array(Box.shape)*3, dtype = Box.dtype)
    Y_len = Box.shape[0]
    X_len = Box.shape[1]
    for i in range(3):
        for j in range(3):
            S_distance = (i-1)**2 + (j-1)**2
            if   S_distance == 0:
                SymBox[Y_len*j:Y_len*(j+1), X_len*i:X_len*(i+1)] = Box
            elif (S_distance == 1 & j == 1):
                SymBox[Y_len*j:Y_len*(j+1), X_len*i:X_len*(i+1)] = np.fliplr(Box)
            elif (S_distance == 1 & i == 1):
                SymBox[Y_len*j:Y_len*(j+1), X_len*i:X_len*(i+1)] = np.flipud(Box)
            elif (S_distance == 2):
                SymBox[Y_len*j:Y_len*(j+1), X_len*i:X_len*(i+1)] = np.flipud(np.fliplr(Box))
    return SymBox

def check_centers(Radii, Centers_X, Centers_Y, slice_2D, n_box, path_save, mode_edges ):
# Graph to chek if aleatory centers are well distributed 
    print('Checking centers, creating graph...')
    for i in range(Radii.size):
        plt.close('all')
        x = Centers_X[i] - Radii[i]
        y = Centers_Y[i] - Radii[i] 
        dx = [Radii[i]*2+1]*len(Centers_X[i])
        fig, ax = plt.subplots()
        ax.set_aspect('equal')
        for x, y, h in zip(x, y, dx):
            ax.add_artist(Rectangle(xy=(x, y), color = 'black', alpha = 0.1, width=h, height=h))      # Gives a square of area h*h
        
        ax.add_artist(Rectangle(xy=(slice_2D.shape[0], slice_2D.shape[1]), fill = False, edgecolor = 'r', width=slice_2D.shape[0], height=slice_2D.shape[1]))
        if  mode_edges == 'Inside':
            shape = 1
        else:
            shape = 3
        ax.set_xlim((0,slice_2D.shape[0]*shape))
        ax.set_ylim((0,slice_2D.shape[1]*shape))
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        chdir(path_save)
        fig.savefig('Check_centers_Nbox_{}_radius_{}_Edges_Mode_{}.png'.format(n_box, Radii[i], mode_edges))

def analysis_for_single_slice_4phases (slice_2D, dir_saving, num_slice, resolution, mode_edges, n_box, n_radii, type_graph, flag_check_centers):
            
    ################################################################################0
    #VER analysis with aleatory growing boxes 2D(JUST ONE Z - slice)
    ################################################################################ 
    # slice_2D = 2D image that will pass through VER analysis in this funtion
    # slice_2D is (normally) a segmented image with only three phases labelled 0, 128 and 255   
    # dsave = the directory where you want to save the resulting figures
    # num_slice (int) is nunmber the slice in the sample that you want to analyze
    # num_slice will be part of the name of the figure when it is saved as png
    # N_box is the number of aleatory boxes
    # Check centers: Flag to see (or not) a graph showing the distribution of the centers of the aleatory boxes
    # Save_graphs: Flag to save (or not) the figures resulting from this function 
    # Radii number = the number of points in your VER graph --> Radii = np.linspace(1,Radiux_Max,Points_number)
    # Mode = Indicates how the sampling is pande if the radius of the aleatory box is larger than distance between its center and a border of the image
    #  Inside means that centers are always inside enough not to have indexes out of range

    Test =    (len(slice_2D.shape) == 2)
    if not (Test == True):
        raise AssertionError("Array must be 2D")
        
    Test =    (slice_2D.shape[0] == slice_2D.shape[1])
    if not (Test == True):
        raise AssertionError("2D array must have equal dimensions")
        
    # Center of the image (y,x coordinates)
    Center           = (int(0.5*slice_2D.shape[0]),int(slice_2D.shape[1]*0.5))
    
    # List of radii of the aleatory boxes
    print("Creating Radii")
    Points_number    = n_radii # Points number in the graph (same as number of radii)
    Radiux_Max       = int(0.5*np.min(slice_2D.shape)) 
    Radii            = np.linspace(1,Radiux_Max,Points_number,dtype = np.int)
     
    # Number of aleatory boxes
    Number_of_Boxes  = n_box
    
    # Creating centers of the boxes (aleatory)
    print("Creating Aleatory Centers")
    Centers_X  = list()
    Centers_Y  = list()
    X_len, Y_len = slice_2D.shape[1],slice_2D.shape[0]
    if  mode_edges == "Inside":
        # taking care of radii to avoid indexing problems
        for Radius in Radii:
            np.random.seed(0)
            Centers_X.append(rd_int(Radius, X_len-Radius, size = Number_of_Boxes))
            np.random.seed(1)
            Centers_Y.append(rd_int(Radius, Y_len-Radius, size = Number_of_Boxes))
        # Previous version to create the centers without np.seed
        #Centers_X    = [rd_int(Radius, X_len-Radius, size = Number_of_Boxes) for Radius in Radii]
        #Centers_Y    = [rd_int(Radius, Y_len-Radius, size = Number_of_Boxes) for Radius in Radii]
    elif (mode_edges == 'Reflect') or (mode_edges == 'Periodic')  :
        for Radius in Radii:
            np.random.seed(0)
            Centers_X.append(rd_int(X_len, 2*X_len, size = Number_of_Boxes))
            np.random.seed(1)
            Centers_Y.append(rd_int(Y_len, 2*Y_len, size = Number_of_Boxes))
    else:
        raise AssertionError("Mode {} not recognized".format(mode_edges)) 
    
    if flag_check_centers == True:
        check_centers(Radii, Centers_X, Centers_Y, slice_2D, n_box, dir_saving, mode_edges)

            
    # Creating limits of the boxes (based on the aleatory centers)
    print("Creating Boxes limits (indexes)")
    X_left      = [Centers_X[i] - Radii[i] for i in range(Radii.size)] # Creating boxes boundaries
    X_right     = [Centers_X[i] + Radii[i] for i in range(Radii.size)]
    Y_left      = [Centers_Y[i] - Radii[i] for i in range(Radii.size)]
    Y_right     = [Centers_Y[i] + Radii[i] for i in range(Radii.size)]
    
    # Sampling the boxes
    print("Sampling the boxes")
    if   mode_edges == 'Inside':
        # Sampling the of boxes 
        Boxes2D = [[slice_2D[Y_left[i][j]:Y_right[i][j], X_left[i][j]:X_right[i][j]] for j in range(Number_of_Boxes) ] for i in range (Radii.size)] 
    elif mode_edges == 'Periodic':
        Bigger = np.zeros(np.array(slice_2D.shape)*3)
        for i in range(3):
            for j in range(3):
                Bigger[Y_len*j:Y_len*(j+1), X_len*i:X_len*(i+1)] = slice_2D
        # Sampling the of boxes 
        Boxes2D = [[Bigger[X_left[i][j]:X_right[i][j], Y_left[i][j]:Y_right[i][j]] for j in range(Number_of_Boxes) ] for i in range (Radii.size)] 
    elif mode_edges == 'Reflect':
        Bigger = Make_Simetric_Image_2D (slice_2D)
         # Sampling the of boxes 
        Boxes2D = [[Bigger[X_left[i][j]:X_right[i][j], Y_left[i][j]:Y_right[i][j]] for j in range(Number_of_Boxes) ] for i in range (Radii.size)] 

  
   # Computing volume fractions
    print("Computing volume fractions")    
    VF        = [[Compute_Volume_Fractions(Box) for Box in Boxes2D [i]] for i in range (Radii.size)]
    VF        = np.array(VF)

   
    # Computing means and standard deviations of the volume fraction
    print("Computing statistics") 
    Means     = VF.mean(axis =1) # axis 1 means : for each the box radius
    Std       = VF.std (axis =1)
    
    Means_Air = Means[:,0]
    Means_Reed= Means[:,1]
    Means_OrM = Means[:,2]
    Means_Grv = Means[:,3]
    
    Std_Air   = Std[:,0]
    Std_Reed  = Std[:,1]
    Std_OrM   = Std[:,2]
    Std_Grv   = Std[:,3]
    
    # global volume fracions
    VF_G_Air, VF_G_Reed, VF_G_FM, VF_G_Grv = Compute_Volume_Fractions(slice_2D)
    
    # Computing relative standard deviations
    Std_Air_R = Std_Air/VF_G_Air#Means_Air
    Std_Reed_R= Std_Reed/VF_G_Reed#Means_Reed
    Std_OrM_R = Std_OrM/VF_G_FM#Means_OrM
    Std_Grv_R = Std_Grv/VF_G_Grv#Means_Grv
    
    # Computing percentiles (25,50,and 75)
    P25 = np.percentile(VF, 25, axis = 1)
    P50 = np.percentile(VF, 50, axis = 1)
    P75 = np.percentile(VF, 75, axis = 1)

    # Computing µ +/- std
    Mu_plus_Std =   Means + Std
    Mu_minus_Std =   Means - Std
    
    # Computing relative lenghts of the boxe    
    Box_lenghts       = Radii*2+1
    Box_lenghts_Rel   = Box_lenghts/np.min(slice_2D.shape)
    
    # saving data:
    columns = ['Box_lenghts', 'Box_lenghts_Rel'] #1
    columns.extend(['std_Voids', 'std_Reeds', 'std_FM', 'std_Grv' ]) #2
    columns.extend(['r_std_Voids', 'r_std_Reeds', 'r_std_FM', 'r_std_Grv']) #3
    columns.extend(['mu_Voids', 'mu_Reeds', 'mu_FM', 'mu_Grv']) #4
    columns.extend(['mu_plus_Voids', 'mu_plus_Reeds', 'mu_plus_FM', 'mu_plus_Grv']) #5
    columns.extend(['mu_minus_Voids', 'mu_minus_Reeds', 'mu_minus_FM', 'mu_minus_Grv']) #6
    columns.extend(['P25_Voids','P25_Reeds', 'P25_FM', 'P25_Grv']) #7
    columns.extend(['P50_Voids','P50_Reeds', 'P50_FM', 'P50_Grv']) #8
    columns.extend(['P75_Voids','P75_Reeds', 'P75_FM', 'P75_Grv']) #9
    data = [Box_lenghts, Box_lenghts_Rel] #1
    data.extend([Std_Air, Std_Reed, Std_OrM, Std_Grv]) #2
    data.extend([Std_Air_R, Std_Reed_R,  Std_OrM_R, Std_Grv_R]) #3
    data.extend([Means_Air, Means_Reed, Means_OrM, Means_Grv]) #4
    data.extend([Mu_plus_Std[:,0], Mu_plus_Std[:,1], Mu_plus_Std[:,2], Mu_plus_Std[:,3]]) #5
    data.extend([Mu_minus_Std[:,0], Mu_minus_Std[:,1], Mu_minus_Std[:,2], Mu_minus_Std[:,3]]) #6
    data.extend([P25[:,0], P25[:,1], P25[:,2], P25[:,3]]) #7
    data.extend([P50[:,0], P50[:,1], P50[:,2], P50[:,3]]) #8
    data.extend([P75[:,0], P75[:,1], P75[:,2], P75[:,3]]) #9
    
    data = np.transpose(data)
    
    data_results = pd.DataFrame(data, columns = columns)
    chdir(dir_saving)
    mode_edges, n_box, n_radii
    data_results.to_csv("VER_analysis_num_slice_{}_EdgesMode_{}_nbox_{}.csv".format(num_slice, mode_edges, n_box), sep = ';')
    
    
    # creating graphs
    if type_graph == 'r_std' or type_graph == 'all' :
        graph_r_std_vs_box_size(Box_lenghts_Rel, Std_Air_R, Std_Reed_R, Std_OrM_R, Std_Grv_R, slice_2D, dir_saving, num_slice, n_box, mode_edges, resolution)
    if type_graph == 'std' or type_graph == 'all' :
        graph_std_vs_box_size(Box_lenghts_Rel, Std_Air, Std_Reed, Std_OrM, Std_Grv, slice_2D, dir_saving, num_slice, n_box, mode_edges, resolution)
    if type_graph == 'percentiles' or type_graph == 'all' :
        graph_percentiles_vs_box_size(Box_lenghts_Rel, P25, P75, P50, slice_2D, dir_saving, num_slice, n_box, mode_edges, resolution )
    if type_graph == 'mu_and_std' or type_graph == 'all' :
        graph_mu_and_std_vs_box_size(Box_lenghts_Rel, Mu_minus_Std, Mu_plus_Std, Means, slice_2D, dir_saving, num_slice, n_box, mode_edges, resolution)
        

def isquare(A):
    print('Exctracting inscribed square...')
    x, y = A.shape
    # diameter of the sphere    
    d = x
    # center
    cx, cy = int(x/2), int(y/2)
    # lenght and half lenght of inscribed square
    l = (d/np.sqrt(2))
    l_2 = int(l/2)
    S = A[cx-l_2:cx+l_2,cy-l_2:cy+l_2]
    return S 

def check_paths_and_files(path_input, path_save, image_name):
    assert (os.path.isdir(path_input)) , "Folder: "    + path_input       + " IS NOT A FOLDER!"
    
    chdir (path_input)
    assert (os.path.exists(image_name)), "Scan_File: " + image_name + " DOES NOT EXIST!"
    
    assert (os.path.isdir(path_save)), "Folder: " + path_save + " IS NOT A FOLDER!"

    
####################################################################
# MAIN
####################################################################   
def main():
    plt.close('all')
    global image_Name
    # Directories and file nale
    dir_reading = "/home/german.martinez-carvajal/Desktop/These/VER_4phases/MON1A_20180723/Input"
    dir_saving  = "/home/german.martinez-carvajal/Desktop/These/VER_4phases/MON1A_20180723/results_aleatory_boxes/Periodic"
    image_Name = 'Segmentation_4phases_of_MON1A_20180723_3700.tif'
    check_paths_and_files(dir_reading, dir_saving, image_Name)
    
    # take the biggest square inscribed the circle (cylinder) ??
    flag_isquare = True
    # Resolution
    resolution = 0.035 # mm/voxel
    # number of radii
    n_radii = 20
    # number of sampling boxes
    n_box = 1000
    # mode for samplig near edges
    mode_edges = 'Periodic' # onde of these ["Inside", "Reflect", "Periodic"]
    # type of graph
    type_graph = 'all' # one of these ['r_std, 'std' , 'percentiles', 'mu_and_std', 'all']
    # the num of the slice 
    num_slice = 3700
    # labels of the phases
    labels = [1,0,64,128,255] # [outside the cylinder, voids, reeds, Fouling Material, gravel]
    flag_check_centers = False
    #############################################
    
    # Reading segmented image
    chdir(dir_reading)
    segmentation = tifffile.imread (image_Name)

    segmentation = relabel_the_outside(segmentation,labels)
    
    # Taking only inscribed square if necessary
    if flag_isquare == True:
        segmentation = isquare(segmentation)
    
    # saving slice that is beeing analyzed
    slice2D = segmentation
    chdir(dir_saving)
    tifffile.imsave('slice_{}.tif'.format(num_slice),slice2D)
    
    # Computing global volume fractions
    Compute_Volume_Fractions (slice2D)    
    
    #computing depth in mm        
    Depth = int(-num_slice*resolution)
    # assembling parameters        
    params = []
    params.append(slice2D)
    params.append(dir_saving)
    params.append(num_slice)
    params.append(resolution)
    params.append(mode_edges)
    params.append(n_box)
    params.append(n_radii)
    params.append(type_graph)
    params.append(flag_check_centers)
    analysis_for_single_slice_4phases(*params)

    print("FINISHED")
main()