# -*- coding: utf-8 -*-
"""
Created on Sat Jun 22 12:23:48 2019

@author: german.martinez-carvajal
"""
import tifffile
from os import chdir
import numpy as np

print('Reading segmentation...')
chdir('/home/german.martinez-carvajal/Bureau/These/Hashemi_segmentation/Tomos_2018/Manip_juillet/MON1A_20180723/heavy_results')
#chdir('/home/german.martinez-carvajal/Desktop/These/PSD_Local_Thickness/MON1A')
A = tifffile.imread('Hashemi_Segmentation_of_MON1A_20180723_ROI_Filtered_aniso_Num_VS_1relabeled.tif')
#A = tifffile.imread('seg_test.tif')

print('Reading PSD LcThk...')
chdir('/home/german.martinez-carvajal/Desktop/These/PSD_Local_Thickness/MON1A')
import tifffile
B = tifffile.imread('LocThk_MON1A_20180723_1_vir.tif')

print('Computing...')

# indexing segmentation
C = A[157:157+len(B)]
assert (len(C) == len (B))

del A

# Set gravel to white
for i in range(len(B)):
    mask = (C[i] == 255)
    B[i,mask] = (255, 255, 255)

# Set deposit to red
for i in range(len(B)):
    mask = (C[i] == 128)
    B[i,mask] = (255,165,0)
#    B[i,mask] = (0,0,0)

# Set outside to black
for i in range(len(B)):
    mask = (C[i] == 1)
    B[i,mask] = (0,0,0)

#del mask, C
print('saving...')
tifffile.imsave('LocThk_MON1A_20180723_1_vir.tif', B)
#del B
print('finished!')
