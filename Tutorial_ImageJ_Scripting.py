# Fiji scripting pythonic-way 

# This file is to be executed in ImageJ as a script

# Look at the source of this tutorial, it is very good !
# http://www.ini.uzh.ch/~acardona/fiji-tutorial/#imageplus
# Check this out too !
# http://book.pythontips.com/en/latest/lambdas.html

# Getting started


from ij import IJ 		# Import the namespace ImageJ from the package ImageJ
IJ.run("Boats (356K)")
imp = IJ.getImage() 	# Points to imp the last image you have opened
print(imp)				# imp is a common name to refer to an instance of an "ImagePlus" object

# Saving an image with a file dialog

from ij.io import FileSaver # io is the input_output namespace
							# File saver is also a name space that creates an instance of FileSaver
fs = FileSaver(imp)			# fs would be the name of the instance
#fs.save()					# invoke the save function on the instance you have created

# Saving an image directly to a file

folder   = "/home/german.martinez-carvajal/Desktop/2018/Tutorial_FIJI_Scripting"
filepath = folder + "/" + "boats_copy.tif"
fs.saveAsTiff(filepath)		# The file saverinstance has more methods as well like saveAsPng and as Jpegt
							# But FileSaver will overwrite the file

# Checking overwriting and if paths are OK is a good idea

from os import path
filename = 'boats_copy_2'
if path.exists(folder):
	if path.isdir(folder):
		filepath = path.join(folder, filename)
		if path.exists(filepath):
			print "File exist, not saving the image, it would overwrite a file!"
		else:
			fs.saveAsTiff(filepath)
			print("File saved succesfully at", filepath)
	else:
		print("Folder is not a directory")
else:
	print("Folder does not exist")

# Inspecting the properties and pixels of an image

print ('title: ' ,  imp.title)
print ('width: ' ,  imp.width)
print ('height:',  imp.height)
print ('number of pixels:      ', imp.width * imp.height)
print ('number of slices:      ', imp.getNSlices())
print ('number of channels:    ', imp.getNChannels())
print ('number of time frames: ', imp.getNFrames())
from ij import ImagePlus
types = {ImagePlus.COLOR_RGB : "RGB"   , # use dot to access the fields of an instance
		 ImagePlus.GRAY8     : "8-bit" ,
		 ImagePlus.GRAY16    : "16-bit",
		 ImagePlus.GRAY32    : "32-bit",
		 ImagePlus.COLOR_256 : "8-bit color"}
print('keys:  '   , types.keys  ())
print('values:'   , types.values())
print('Image type', imp.type, types[imp.type])

# Inspecting the pixels of an image

ip = imp.getProcessor() # The pixels of an image are wrapped in a ImageProcessor instance
from ij.process import ImageStatistics as IS
options = IS.MEAN | IS.MEDIAN | IS.MIN_MAX
print(IS.MEAN , IS.MEDIAN , IS.MIN_MAX)
print('Options: ', options)
print('Calibration: ', imp.getCalibration())
stats = IS.getStatistics(ip, options, imp.getCalibration())
print("Image statistics for: ", imp.title)
print("Mean  ", stats.mean)
print("Median", stats.median)
print("Min   ", stats.min)
print("Max   ", stats.max)

# (NOTE: if the images are stacks, use StackStatistics instead.)

# Iterating pixels
ip     = imp.getProcessor().convertToFloat() # Convert the image processor instance(ip) to float to avoid problems with bytes
pixels = ip.getPixels() 				     # Pixels points to an array of floats

# Iterating over pixels to obtain the minimum value

minimum = 256
for i in xrange(len(pixels)): 	# for loop C style using the Xrange function
	if pixels[i] < minimum:
		minimum = pixels[i]
print('len of pixels array = ', len(pixels))
print("1) Minimum is: ", minimum)

minimum = 256
for  pix in pixels: 			# for loop more pythonic way using lists
	if pix < minimum:
		minimum = pix
print("2) Minimum is: ", minimum)	

minimum = 256
minimum = reduce( min, pixels) # The functional method using de REDUCE function.
	# The reduce function works with any two argument function like min
print('min is ?', min)
print("3) Min: ", minimum)

# For an RGB image, you'd want to ask which pixel is the least bright. 
# It's easy to do so by calling getBrightness() on the ImageProcessor 
# of an RGB image (which is a ColorProcessor). Or compute the minimum 
# for one of its color channels, which you get with the method ip.toFloat(0, None) 
# to get the red channel (1 is green, and 2 is blue).

# Introduction to some operations to perform on list or collections
# MAP, REDUCE, FILTER

# MAP Operation
# A map operation returns a list of lenght N with the results of a function applied to another list of lenght N as well
# The map operation will run much more faster than a for loop
# Example where want to get a list of all images open in Fiji.
# While this is a trivial example, suppose you were executing a complex operation 
# on every element of a list or an array. 
# If you were to redefine the map function to work in parallel, 
# suddenly any map operation in your program will run faster, 
# without you having to modify a single line of tested code!
from ij import WindowManager as WM
# 1) MAP operation - for loopp version
images = []
print('WM.getIDList() = ', WM.getIDList())
for id in WM.getIDList():
	print('id = ', id)
	images.append(WM.getImage(id))
# 2) MAP operation version with listt comprehension
images = [WM.getImage(id) for id in WM.getIDList()]
# 3) Real MAP operation
images = map(WM.getImage, WM.getIDList())


# FILTER operation
# A filter operation takes a list of lenght N, and return ashorter 
# list, with lenght 0>L<N which elements pass a test
# For example, suppose you want to find the largest image, by area, from the list of all opened images in Fiji.
imps = map(WM.getImage, WM.getIDList()) # Remember that imp (plural imps) is an instance of a ImagePlus object
print('ImagePlus=' , ImagePlus)
def match(imp):
	""" Returns true if the image title contains the word 'boats' """
	return imp.title.find("boats") > -1
# 1) Filter operation with a for loop
matching = []
for imp in imps:
	if match(imp):
		matching.append(imp)
# 2) Filter operation with a list comprehension
matching  = [imp for imp in imps if match(imp)]
# 3) Filter operation
matching = filter(match, imps)
print('matching is       : ', matching)
print('len of matching is: ', len(matching))

# REDUCE Operation
# A reduce operation takes a list of length N and returns a single value. 
# It applies function that takes two arguments to the first two elements of the list, 
# then to the result of that and the next element, etc. 
# Optionally an initial value may be provided, so that the cycle starts with that value and the first element of the list.
def area(imp):
	return imp.width*imp.height
# 1) Reduce operation with a for loop
largest      = None
largestArea = 0
for imp in imps:
	if largest is None:
		largest = imp
	else:
		a = area(imp)
		if a > largestArea:
			largest     = imp
			largestArea = a
# 2) with a reduce operation
def largestImage(imp1, imp2):
	return imp1 if area(imp1)>area(imp2) else imp2
largest = reduce(largestImage, imps)
print('Largest images is :' , largest)


# Subtract the minimum value to every pixel
imp     = IJ.getImage()
# Method1: subtract in place modifying the pixels array
ip      = imp.getProcessor().convertToFloat() # as a copy
pixels  = ip.getPixels ()
minimum = reduce(min, pixels)
for i in xrange(len(pixels)):
	pixels[i] -= minimum
imp2 = ImagePlus('_2', ip) # This is the way how you create a new image
imp2.show()
# Method2: subtract and store in a new array
ip      = imp.getProcessor().convertToFloat() # as a copy
pixels  = ip.getPixels ()
minimum = reduce(min, pixels)
pixels  = ip.getPixels ()
pixels3 = map(lambda x: x - minimum, pixels)
from ij.process import FloatProcessor
ip3  = FloatProcessor(ip.width, ip.height, pixels3, None)
imp3 = ImagePlus('_3', ip3)
imp3.show()
imp2.close()
imp3.close()

# Count pixels above a threshold (in this example the threshols is the mean)
# we are using the reduce function to count the pixels, it is like programming your own numpy.countNonZero()
imp    = IJ.getImage()
ip 	   = imp.getProcessor().convertToFloat()
pixels = ip.getPixels() 
mean = sum(pixels)/len(pixels)
n_pix_above = reduce (lambda count, a: count + 1 if a >mean else count, pixels, 0)
print "Mean value", mean
print "% pixels above mean: ", n_pix_above / float(len(pixels))*100


# Finding the coordinates of all pixels above a certain value
# and the mass center
above = filter (lambda i: pixels[i]> mean, xrange(len(pixels))) # Obtain the list of indexes of pixels whose value is above the mean 
print('Number of pixels above mean value: ', len(above))
width = imp.width
# METHOD 1 with a for loop 
# NOTE:  For this exaple this is the method with the best performance
xc = 0
yc = 0
for i in above:
	xc += i % width
	yc += i / width
xc = xc / len(above)
yc = yc / len(above)
print ("Mass center Method 1: xc, yc = ", xc, yc)
# METHOD 2: with sum and map
# NOTE: While, in this case, the method is less performant 
# due to repeated iteration of the list "above", 
# the code is shorter, easier to read, 
# and with far less opportunities for introducing errors.
# If the actual computation was far more expensive than the simple calculation 
# of the coordinates of a pixel given its index in the array of pixels, 
# this method would pay off for its clarity.
xc = sum(map(lambda i: i%width, above))/len(above)
yc = sum(map(lambda i: i/width, above))/len(above)
print ("Mass center Method 2: xc, yc = ", xc, yc)
# METHOD 3: Iterating the list "above" just once
# But the least performant at all
xc, yc = [d / len(above) for d in
		  reduce(lambda c, i:[c[0]+i%width, c[1]+i/width], above, [0,0])]
print ("Mass center Method 3: xc, yc = ", xc, yc)
# METHOD 4: Iterating the list "above" juste one 
# More clearly and performant
from functools import partial # Partial is used to call functions with a frozen argument, also known as a currying a funcion
def accum (width, c, i):
	""" Accumulate de sum of the X, Y coordinates of index i in the list c """
	c[0] += i%width
	c[1] += i/width
	return c
xc, yc = [d/len(above) for d in reduce(partial(accum, width), above, [0,0])]
print ("Mass center Method 4: xc, yc = ", xc, yc)

# IMAGEJ PLUGGINS
# Running ImageJ / Fiji plugins on an ImagePlus
from ij import IJ
from ij.plugin.filter import RankFilters # Rank Filters is a class
# Grab the image active
imp = IJ.getImage()
ip  = imp.getProcessor().convertToFloat() # as a copy
# Remove the noise by runing a median filter
# with a radius of 2
radius = 2
RankFilters().rank(ip, radius, RankFilters.MEDIAN) # A new instance of RankFilters has been createdby the ()
# Then the method "rank" is called
imp2 = ImagePlus(imp.title + "median filtered", ip)
imp2.show()

# Some lines to understand better the mechanism of "RankFilters()
print  ("RankFilters  is", RankFilters) # RankFilters is a "type"
print  ("imp.getProcessor is:" , imp.getProcessor) # getProcessir is a "method"
ip1   = imp.getProcessor().convertToFloat() # as a copy
ip2   = RankFilters().rank(ip1, radius, RankFilters.MEDIAN)
imp1  = ImagePlus(imp.title + "copy", ip1)
imp1.show()
# THIS LINE WILL NOT WORK, to conclude, RankFilters will modify the argument ip in place
# imp2  = ImagePlus(imp.title + "copy", ip2)
imp1.close()
imp2.close()
print('ok')

# Running a command on an Image
from ij import IJ
imp   = IJ.getImage()
IJ.run(imp, "Enhance Contrast...", "saturated=4  normalize")


