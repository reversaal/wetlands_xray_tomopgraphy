# -*- coding: utf-8 -*-
"""
Created on Thu Jun  6 18:21:52 2019

@author: german.martinez-carvajal
"""

import tifffile
import matplotlib.pyplot as plt
import numpy as np
from os import chdir
import pandas as pd

def sector_mask(shape,centre,radius,angle_range):
    """
    CODE FROM STACK OVER FLOW
    Return a boolean mask for a circular sector. The start/stop angles in  
    `angle_range` should be given in clockwise order.
    """

    x,y = np.ogrid[:shape[0],:shape[1]]
    cx,cy = centre
    tmin,tmax = np.deg2rad(angle_range)

    # ensure stop angle > start angle
    if tmax < tmin:
            tmax += 2*np.pi

    # convert cartesian --> polar coordinates
    r2 = (x-cx)*(x-cx) + (y-cy)*(y-cy)
    theta = np.arctan2(x-cx,y-cy) - tmin

    # wrap angles between 0 and 2*pi
    theta %= (2*np.pi)

    # circular mask
    circmask = r2 <= radius*radius

    # angular mask
    anglemask = theta <= (tmax-tmin)

    return circmask*anglemask

def ROI_circular(image, rw):
    print("adjusting regions of interest ...")

    # CREATING CIRCULAR MASK
    # The cylinder is supposed to be the biggest "circle" inscribed in a squared image
    z,x,y = np.shape(image)[0], np.shape(image)[1], np.shape(image)[2]
    center = int(x/2), int(y/2)
    radius = int((x/2))*rw
    #Selecting circular ROI (region of interest)
    mask_2D = sector_mask((x,y),center,radius,(0,360 ))       
    mask_3D = np.zeros(image.shape, dtype = np.bool)
    for j in range(z):
        mask_3D[j] = mask_2D
    image[np.logical_not(mask_3D)] =  0
    return image
    
def figure(zone, diameters_real,volume_fractions,cum_volume_fractions, path_save, file_name, bins):

        # Plotting
        fig, ax1 = plt.subplots()
        ax1.plot(diameters_real, volume_fractions,
                color= "blue",
                label = "Fraction    " ,
                lw = 1 )

        ax2 = ax1.twinx()  
        ax2.plot(diameters_real, cum_volume_fractions, 
                 color= "k",
                 lw=1,
                 label = "Cummulative Fraction",
                 linestyle = '-' )

        ax1.legend(loc = 'upper left'  , bbox_to_anchor =(0.0, 1.1))
        ax2.legend(loc = 'upper right' , bbox_to_anchor =(1.0, 1.1))
        ax1.set_xlabel('Diameter (mm)' )
        ax1.set_ylabel("Fraction (% v/v)")
        ax2.set_ylabel("Cumumative Fraction (% v/v)")
        ax1.set_xlim(0)
        ax1.set_ylim(0)
        ax2.set_ylim(0,1)
        ax1.grid(axis = 'both', which = 'both')
        ax1.minorticks_on()
        # Saving figure
        chdir(path_save)
        title  = 'Figure_PSD_from_{}_{}_bins_{}'.format(file_name.replace('.tif', ""), zone, bins)
        fig.set_size_inches(9, 7)
        fig.savefig(title + '.png', dpi = 300)
        print('finished')
        
def PSD_from_pore_size_map(zone, image, file_name, path_read, path_save, resolution, max_pore_size = None, number_of_bins = 100):
    
    print("image shape = ", image.shape)

    
    # getting rid of not important voxels  (i.e voxels that do not belong to the void phase)
    not_important_voxels = (image == 0)
    image[not_important_voxels] = np.nan

    # A line for checking everything is good
    total_pore_voxels = np.count_nonzero(image > 0)

    # computing data to create histograms (FOR THE WHOLE IMAGE)  
    print('Computing for ', zone)
    if max_pore_size == None:
        max_pore_size = np.nanmax(image)
        print("Max pore size (in pixels) has been set to ",  max_pore_size)
    else:
        print("Max pore size (in pixels) is ",  max_pore_size)
        
    hist, bin_edges = np.histogram(image, bins = number_of_bins, range = (0,max_pore_size))
    diameters_real = bin_edges*resolution # result in mm
    total = np.sum(hist)
    volume_fractions = np.append(0, hist/total)
    cum_volume_fractions = np.cumsum(volume_fractions)
    
    # checking     
    assert (total == total_pore_voxels)

    # saving data in a csv file
    df = pd.DataFrame()
    df['Diameter_mm'] = diameters_real
    df['voxel count'] = np.append(0, hist)
    df['Volume fraction'] = volume_fractions
    df['Cum volume fraction'] = cum_volume_fractions
    chdir(path_save)    
    df.to_csv('PSD_of_{}_{}_bins_{}.csv'.format(zone,file_name.replace('.tif', ""), number_of_bins), sep = ';')
    
    # creating figure    
    print('Creating figure...')    
    figure(zone, diameters_real,volume_fractions,cum_volume_fractions, path_save, file_name, number_of_bins)
    
   
    
def main():
    # closing existing figures
    plt.close('all')
    
    file_name = 'LocThk.tif' # the pore size mag image computed by the local thickness method
    path_read = '/home/german.martinez-carvajal/Desktop/These/PSD_Local_Thickness/test'
    path_save = '/home/german.martinez-carvajal/Desktop/These/PSD_Local_Thickness/test'
    print('Reading pore size map ... ', file_name)    
    chdir(path_read)
    image = tifffile.imread(file_name)
    rw = 1
    image = ROI_circular(image, rw)
    print(image.shape)
    
    resolution = 0.035  # mm/vox
    number_of_bins = 100
    max_pore_size = None # in vox
    limit_dep_layer = 6 # slice number where the deposit layer finishes and the gravel layer starts
    
    # Computing    
    zone = "Whole_Image"    
    PSD_from_pore_size_map(zone, image[:], file_name, path_read, path_save, resolution,max_pore_size, number_of_bins)
    zone = "Dep_Layer"
    PSD_from_pore_size_map(zone, image[0:limit_dep_layer], file_name, path_read, path_save, resolution, max_pore_size,  number_of_bins)
    zone = "Grv_Layer"    
    PSD_from_pore_size_map(zone, image[limit_dep_layer:], file_name, path_read, path_save, resolution,max_pore_size, number_of_bins)

main()
