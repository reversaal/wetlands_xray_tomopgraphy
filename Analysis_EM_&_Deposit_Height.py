########################################################
# Analysis of the relationship between height of deposit 
# layer and electric conductivity (EM)
########################################################

def Fusion_Frames (Frame_EM, Frame_Deposit):
    import pandas as pd
    import numpy  as np
    """ Returns a new frame where a column with the data of "Deposit Heights" belonging to Frame_Deposit 
        is added to Frame_EM at the corresponding coordinates (# of filter, X and Y).
        If data is missing, the value is filled with np.nan
        
        Output: Data_EM_DH (pandaDataFrame) "
    """
    # FUSIONING BOTH FRAMES
    # The number of points in both frames is different. 
    # Here I fuse the frames if the coordinates are the same
    # if not a np.nan is stored
    
    # Selecting the columns corresponding to the coordinates
    CoordinatesEM = Frame_EM     [["Filtre_#","x_[m]_by_hand","y_[m]_by_hand"]]
    CoordinatesDP = Frame_Deposit[["Filtre_#","x_[m]_by_hand","y_[m]_by_hand"]]
    # Initializing the new columns Heights and Comments
    Dep_Heights = np.zeros(len(Frame_EM)) # initialize Deposit Heights
    Dep_Heights.fill(np.nan)
    # In this loop I import the values of Height and comment if coordinates are the same
    # I need two for loops to study all possibilies    
    for index1 in CoordinatesEM.index:    
        for index2 in CoordinatesDP.index:
            Same_Coords = CoordinatesEM.ix[index1] == CoordinatesDP.ix[index2] # this is a series-type object
            Same_Coords = Same_Coords.all() #= True if the three (#Filter, X and Y) coordinates are equal
            if Same_Coords:
                Dep_Heights[index1] = Frame_Deposit.ix[index2,["h(cm)"  ]]
                CoordinatesDP       = CoordinatesDP.drop(index2)
                break # break the loop
    # Checking if all deposit depths were imported
    if len(CoordinatesDP) != 0:
        print('WARNING, fuision of data is not complete !')
        print('There are Deposit Heights data whose coordinates are not present in the EM_Data')
    else:
        print('Fusion of data OK!')
    Data_EM_DH            = Frame_EM.copy()
    Data_EM_DH["h(cm)"  ] = Dep_Heights # Deposit Heights
    return(Data_EM_DH)

def Figure_1 (Frame):
    import matplotlib.pyplot as plt
    """ Plots the a scatter of electric conductivity and Deposit Height
        taking all the points into account """
    Figure, Ax = plt.subplots()
    Ax.scatter   (Frame["h(cm)"  ], Frame["Cond_[mS/m]"])
    Ax.set_xlabel('Deposit Height (cm)')
    Ax.set_ylabel('Conductivity (mS/m)')
    Ax.set_title ('Scatter with all points')
    plt.show()

def Figure_2 (Frame):
    import matplotlib.pyplot as plt
    """ Plots the a scatter of electric conductivity and Deposit Height
        taking points that were not affected by the presence of a metalic
        object while performing the EM """
    # Deleting non consisting values
    Outliers1 = Frame["Inphase_[ppt]"] > 3  # 
    Outliers2 = Frame["x_[m]_by_hand"] == 0 # Parce qu'il sont proche d'un talud (surface non plate)
    Outliers3a= Frame["y_[m]_by_hand"] == 0
    Outliers3b= Frame["Filtre_#"]      == 1
    Outliers3 = Outliers3a & Outliers3b
    Outliers  = Outliers1 | Outliers2 | Outliers3 # Parce qu'il sont proche d'un talud (surface non plate)
    Frame_Good= Frame[~Outliers]    
    Figure, Ax = plt.subplots()
    Ax.scatter   (Frame_Good["h(cm)"  ], Frame_Good["Cond_[mS/m]"])
    Ax.set_xlabel('Deposit Height (cm)')
    Ax.set_ylabel('Conductivity (mS/m)')
    Ax.set_title ('Scatter without interference points')
    plt.show()
    return(Frame_Good)# the Frame without the outliers

def Figure_3 (Frame, Filter_num):
    import matplotlib.pyplot as plt
    from matplotlib import cm
    from mpl_toolkits.mplot3d import Axes3D
    Selection = (Frame["Filtre_#"] == Filter_num)
    Frame = Frame[Selection]
    Fig   = plt.figure() 
    Ax    = Axes3D(Fig)
    X     = Frame["x_[m]_by_hand"]
    Y     = Frame["y_[m]_by_hand"]
    Z     = Frame["Cond_[mS/m]"  ]
    X, Y, Z = create_2D_arrays(X,Y,Z)
    Ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.viridis)
    pass    
def create_2D_arrays(X,Y,Z):
    """Transforms lists of values X,Y Z, into 2D arrays, using interpolation
    to use them when you want to draw 3D plots or contours, 
    See: https://stackoverflow.com/questions/9008370/python-2d-contour-plot-from-3-lists-x-y-and-rho   """
    import scipy.interpolate
    import numpy as np
    # Set up a regular grid of interpolation points
    xi, yi = np.linspace(X.min(), X.max(), 100), np.linspace(Y.min(), Y.max(), 100)
    xi, yi = np.meshgrid(xi, yi)
    # Interpolate
    rbf = scipy.interpolate.Rbf(X, Y, Z, function='linear')
    zi = rbf(xi, yi)
    return (xi, yi, zi)
################################################################################
# 
################################################################################
# Reading File frame with EM data (Carte à Electromagnetisme Multifrequentiel d'un FPR Filtre Planté des Roseaux)
import pandas as pd
File_Path = '/home/german.martinez-carvajal/Desktop/2018/EM_Electromag_multifrequentiel/180329_Montromant_EM/donees_EM_correction_tacheo'
File_Name = File_Path + "/Data_Frame_EM_dots.odf.csv"
Frame_EM  = pd.read_csv(File_Name, sep = ',')
# Reading Data frame with Deposit Height Data (Data noted by hand in the same FPR )
File_Path = '/home/german.martinez-carvajal/Desktop/2018/EM_Electromag_multifrequentiel/180329_Montromant_EM/Hauteurs_depot'
File_Name = File_Path + "/Hauteur_depot.csv"
Frame_Deposit = pd.read_csv(File_Name, sep = ',')

# Creating the new frame and saving it
print("Fusing EM and Depotit data")  
Frame_EM_DH = Fusion_Frames (Frame_EM, Frame_Deposit)
from os import chdir
File_Path = '/home/german.martinez-carvajal/Desktop/2018/EM_Electromag_multifrequentiel/180329_Montromant_EM/Data_together'
chdir       (File_Path)
Frame_EM_DH.to_csv('Frame_EM_DH.csv')

# Plotting
import matplotlib.pyplot as plt
plt.close("all")
# Figure 1 - scatter with all points
Figure_1(Frame_EM_DH)
# Figure 2 - scatter without interferences
Frame_EM_DH_Good = Figure_2(Frame_EM_DH)
# Figure 3 - scatter without interferences
Figure_3(Frame_EM_DH_Good, Filter_num = 1)








