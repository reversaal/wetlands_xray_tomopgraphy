import tifffile
from os import chdir
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os.path

def raw_profile_original(image, labels):
    Phase0  = (image == labels[0])
    Phase1  = (image == labels[1]) # Air
    Phase2  = (image == labels[2]) # Fouling Material
    Phase3  = (image == labels[3]) # Gravel
    VolPerSlice  = np.sum(Phase1, axis = (1,2)) + np.sum(Phase2, axis = (1,2)) + np.sum(Phase3, axis = (1,2)) # in voxels
    VF1   = np.sum(Phase1, axis = (1,2))/VolPerSlice
    VF2   = np.sum(Phase2, axis = (1,2))/VolPerSlice
    VF3   = np.sum(Phase3, axis = (1,2))/VolPerSlice
    VFT  = VF1 + VF2 + VF3
    return (VF1, VF2, VF3, VFT)
    
def raw_profile_if_ram_limitation(Image, Labels):
    
    Vol_Frac_1, Vol_Frac_2, Vol_Frac_3, Vol_Frac_Total = [], [], [], []
    for z_Slice in range(len(Image)):
        # this loop computes the volume fractions for each slice and appends the result to the whols profile 
        
        Phase0  = (Image[z_Slice] == Labels[0] )
        Phase1  = (Image[z_Slice] == Labels[1])
        Phase2  = (Image[z_Slice] == Labels[2])
        Phase3  = (Image[z_Slice] == Labels[3])
        VolPerSlice  = np.sum(Phase1) + np.sum(Phase2) + np.sum(Phase3) # in voxels
        Vol_Frac_1.append(np.sum(Phase1/VolPerSlice))
        Vol_Frac_2.append(np.sum(Phase2/VolPerSlice))
        Vol_Frac_3.append(np.sum(Phase3/VolPerSlice))
        Vol_Frac_Total.append(Vol_Frac_1[-1]+Vol_Frac_2[-1]+Vol_Frac_3[-1])
    Vol_Frac_1 = np.array(Vol_Frac_1)
    Vol_Frac_2 = np.array(Vol_Frac_2)
    Vol_Frac_3 = np.array(Vol_Frac_3)
    Vol_Frac_Total = np.array(Vol_Frac_Total)
    return (Vol_Frac_1, Vol_Frac_2, Vol_Frac_3, Vol_Frac_Total)
    
    
def plot_raw_profile(path_save, sample_name, z, VF1, VF2, VF3, rw):
    plt.close("all")
    fig, ax = plt.subplots()

    ax.plot(VF1,z,color='b',lw=1,linestyle = '-' , label='Voids')
    ax.plot(VF2,z,color='r',lw=1,linestyle = '--', label ='FM')
    ax.plot(VF3,z,color='k',lw=1,linestyle = ':', label ='Grv')
    ax.set_xlabel('Volume Fraction')
    ax.set_ylabel('Depth (mm)')
    ax.set_xlim([0,1])
    ax.legend()
    if rw == None:
        title =  "Raw_VFP_of_{}.png".format(sample_name)
    else:
        title =  "Raw_VFP_of_{}_rw_{}.png".format(sample_name, rw)
    chdir(path_save)
    fig.savefig(title)
    return (fig, ax)

def plot_smooth_profile(path_save, sample_name, z, VF1, VF2, VF3, rw):
    """
    w_size = window_size in mm
    """
    plt.close("all")
    fig, ax = plt.subplots()

    ax.plot(VF1,z,color='b',lw=1,linestyle = '-' , label='Voids')
    ax.plot(VF2,z,color='r',lw=1,linestyle = '--', label ='FM')
    ax.plot(VF3,z,color='k',lw=1,linestyle = ':' , label ='Grv')
    ax.set_xlabel('Volume Fraction')
    ax.set_ylabel('Depth (mm)')
    ax.set_xlim([0,1])
    ax.legend()
    if rw == None:
        title =  "Smooth_VFP_of_{}.png".format(sample_name)
    else:
        title =  "Smooth_VFP_of_{}_rw_{}.png".format(sample_name, rw)
    chdir(path_save)
    fig.savefig(title)
    return (fig, ax)

def mov_av_window (A,n):
    extended = np.concatenate(([A[0]]*n, A, [A[-1]]*n))
    smooth   = []
    for i in range(n,len(A)+n):
        smooth.append(np.mean(extended [i-n:i+n+1]))
    return np.array(smooth)
    
def sector_mask(shape,centre,radius,angle_range):
    """
    CODE FROM STACK OVER FLOW
    Return a boolean mask for a circular sector. The start/stop angles in  
    `angle_range` should be given in clockwise order.
    """

    x,y = np.ogrid[:shape[0],:shape[1]]
    cx,cy = centre
    tmin,tmax = np.deg2rad(angle_range)

    # ensure stop angle > start angle
    if tmax < tmin:
            tmax += 2*np.pi

    # convert cartesian --> polar coordinates
    r2 = (x-cx)*(x-cx) + (y-cy)*(y-cy)
    theta = np.arctan2(x-cx,y-cy) - tmin

    # wrap angles between 0 and 2*pi
    theta %= (2*np.pi)

    # circular mask
    circmask = r2 <= radius*radius

    # angular mask
    anglemask = theta <= (tmax-tmin)

    return circmask*anglemask

def relabel_the_outside(image,labels, rw = None):
    """ if pixels outside the cylinder that contains the sample have the same value for air
        they will be relabeled with the value label[1]
        so, if you follow the conventions for label values
        label [0]= 1 = pixels outside the cylinder, 
        label [1]= 0 = air (i.e voids) pixels
        label [2]= 128 = fouling material
        label [3]= 255 = gravel
    """
    assert (np.count_nonzero(image == 1) == 0 ), 'You must reserve label = 1, for pixels outside the cylinder'
    print("relabeling from 0,128,255 to 0,1,128, 255")


    # CREATING CIRCULAR MASK
    # The cylinder is supposed to be the biggest "circle" inscribed in a squared image
    z,x,y = np.shape(image)[0], np.shape(image)[1], np.shape(image)[2]
    center = int(x/2), int(y/2)
    if rw == None:
        radius = int((x/2))
    else:
        radius = int((x/2))*rw
    #Selecting circular ROI (region of interest)
    mask_2D = sector_mask((x,y),center,radius,(0,360 ))       
    mask_3D = np.zeros(image.shape, dtype = np.bool)
    for j in range(z):
        mask_3D[j] = mask_2D
    image[np.logical_not(mask_3D)] =  labels[0] # outside the cilinder label = 1
    print("{} pixels have been attributed to the phase : outside the cylinder".format(np.count_nonzero(image == 1)))
    return image
    
def Compute_Volume_Fraction_Profile (path, path_save, image_name, sample_name, labels, neighbors, resolution, flag_TZ = False, lims = (None, None), flag_RAM_limitation = False, flag_relabel = False, rw = None):
    """ Input Labels = list with int numbers that represent the phases of the image, in this version there are 4 phases
        Label[0] = outside the cylinder
        Label[1] = voids
        Label[2] = fouling material
        Label[3] = gravel
    """
    ##################    
    # Importing files 
    ##################
    print('Reading Image :', path, " ", image_name)
    chdir(path)
    image = tifffile.imread(image_name)
    
    if flag_relabel == True:
        image = relabel_the_outside(image, labels, rw)
    
   
    ##################
    # Cheking there are only 4 phases in the image    
    ##################
    phase0  = np.count_nonzero((image==labels[0]))
    phase1  = np.count_nonzero((image==labels[1])) 
    phase2  = np.count_nonzero((image==labels[2]))     
    phase3  = np.count_nonzero((image==labels[3])) 
    test    = (np.product(image.shape) == (phase0+phase1+phase2+phase3))   
    if test == False:
        raise AssertionError("You must verify that there are EXACTLY {} labels for phases in your image".format(len(labels)))
    
    # computing Raw Profile
    ###################################
    if flag_RAM_limitation == False:
        VF1, VF2, VF3, VFT = raw_profile_original(image, labels)
    else:
        VF1, VF2, VF3, VFT = raw_profile_if_ram_limitation(image, labels)    
    
    ##################    
    # saving raw profile
    ##################
    z = -1*resolution*np.arange(len(image)) # result in mm
    raw_data = np.transpose([z, VF1, VF2, VF3, VFT])
    columns = ['z (mm)','Voids (%v/v)','FM (%v/v)','Gravel (%v/v)', 'Total']
    raw_data = pd.DataFrame(raw_data, columns = columns)
    if rw == None:
        csv_file_name = 'Raw_VFP_of_{}.csv'.format(sample_name)
    else:
        csv_file_name = 'Raw_VFP_of_{}_rw_{}.csv'.format(sample_name, rw)
        
    chdir (path_save)
    raw_data.to_csv(csv_file_name)
    
    ##################
    # plotting raw profile 
    ##################
    plot_raw_profile(path_save, sample_name, z, VF1, VF2, VF3, rw)
    
    ###################################
    # Compute smooth profiles
    ###################################
    S_VF1 = mov_av_window (VF1,neighbors)
    S_VF2 = mov_av_window (VF2,neighbors)
    S_VF3 = mov_av_window (VF3,neighbors)
    S_VFT = S_VF1 + S_VF2 + S_VF3
    
    ##################    
    # saving smooth profile
    ##################
    z = -1*resolution*np.arange(len(image)) # result in mm
    smooth_data = np.transpose([z, S_VF1, S_VF2, S_VF3, S_VFT])
    columns = ['z (mm)','Air (%v/v)','FM (%v/v)','Grv (%v/v)', 'Total']
    smooth_data = pd.DataFrame(smooth_data, columns = columns)
    w_size  =  neighbors*resolution
    
    if rw == None:
        csv_file_name = 'Smooth_VFP_of_{}.csv'.format(sample_name)
    else:
        csv_file_name = 'Smooth_VFP_of_{}_rw_{}.csv'.format(sample_name, rw)
    chdir(path_save)
    smooth_data.to_csv(csv_file_name)
    
    ##################
    # plotting smooth profile
    ##################
    fig, ax = plot_smooth_profile(path_save, sample_name, z, S_VF1, S_VF2, S_VF3, rw)
        
    ###################################
    # 3) SMOOTHED PROFILES  With Transition Region Between Deposit and gravel
    ###################################
    if flag_TZ == True:
        ax.fill([0,0,1,1],[lims[0],lims[1],lims[1],lims[0]],'r', alpha = 0.2, label = 'Transition zone')
        title =  "TZ_Smooth_VF_Profile_Raw_of_{}_ws_{}mm.png".format(clean_name(image_name),w_size )
        ax.legend()
        chdir(path_save)
        fig.savefig(title)
    
    print('Volume Fraction Profiles of {} were drawn correctly'.format(image_name))
###################################################################################################
# END of FUNCTION TO PLOT THE profiles
##################################################################################################

def check_paths_and_files(path_read, path_save, file):
    
    assert (os.path.isdir(path_read)) , "Folder: "    + path_read       + " IS NOT A FOLDER!"
    
    chdir (path_read)
    
    assert (os.path.exists(file)), "Scan_File: " + file + " DOES NOT EXIST!"
    
    assert (os.path.isdir(path_save))        , "Folder: "    + path_save       + " IS NOT A FOLDER!"
    
    
###################################################################################################
# MAIN 
###################################################################################################
def main():
    # Directory Reading
    path_read = '/home/german.martinez-carvajal/Desktop/These/Hashemi_segmentation/Tests/Test9_Rapide/Results/heavy_results'
    # Directory saving
    path_save = '/home/german.martinez-carvajal/Desktop/These/Volume_fraction_profiles/Test'
    # Image Name
    image_name ='Hash_Segm_of_Image_Filtered_PVE_50_Tol_0.75_NumVS_1.tif'
    # Sample Name
    sample_name = "Image_Filtered"
    # cheking
    check_paths_and_files(path_read, path_save, image_name)

    # Constant parameters    
    # Image resolution
    resolution        = 0.035 #mm per vox
    # Number of neighbors for moving average window
    neighbors         = 57 # in voxels 
    # Labels of the phases
    labels = [1,0,128,255] # 1 = outside the cylinder, 0 = voids, 128 = fouling material, 255 = gravel

    # Relabel
    flag_relabel = True    # set value of pixels outside the inscribed cylinder to int(1)
    
    # Draw Transition Zone ?
    flag_TZ = False
    lims = (-2*resolution, -5*resolution) # limits of the transition zone in mm
    
    # RAM limitation ?    
    flag_RAM_limitation = False
    
    # Compute only inside a circular ROI of size rw (radius of window) as a fraction 
    rw = 0.99
    
    # Computing
    Compute_Volume_Fraction_Profile (path_read, path_save, image_name, sample_name, labels, neighbors, resolution, flag_TZ = flag_TZ, lims = lims, flag_RAM_limitation = flag_RAM_limitation, flag_relabel = flag_relabel, rw = rw)
main()

print('Finished')
