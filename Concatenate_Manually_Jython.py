from ij import IJ
from ij import ImageStack
from ij import ImagePlus
from ij.io import FileSaver

def concatenate_scans(path,files,cs):
	""" input cs are connecting slices
	"""
	# Reading images
	print('Reading images')
	Images = list()
	for i in range(len(files)):
		Images.append(IJ.openImage(files[i]))

	# Geting stacks
	Stack_processors = list()
	for i in range(len(files)):
		Stack_processors.append(Images[i].getImageStack())

	# Creating the new stack
	print('Creating the new Stack for the concatenation')
	StackNew  = ImageStack(Images[0].width, Images[0].height)

	# Filling the new  stack
	for file_ in range(len(files)):
		for slice_ in xrange(cs[file_]+1, Images[file_].getNSlices()+1):
			Ip = Stack_processors[file_].getProcessor(slice_)
			StackNew.addSlice(Ip)
	ImpNew = ImagePlus("Concatenated", StackNew)


	# Saving
	print('Saving')
	fs = FileSaver(ImpNew)
	filepath = path + "\Concatenation.tif"
	fs.saveAsTiff(filepath)
	print(ImpNew)
	ImpNew.show()
	print('Finished Ok')


path         = "C:\Users\german.martinez-carv\Desktop\_2018_11_12_MON_1_ScanJ0"
File1 = path + "\_2018_11_12_MON_1_35mic_J0_scan1-1.tif"
File2 = path + "\_2018_11_12_MON_1_35mic_J0_scan2-1.tif"
File3 = path + "\_2018_11_12_MON_1_35mic_J0_scan3-1.tif"
File4 = path + "\_2018_11_12_MON_1_35mic_J0_scan4-1.tif"
cs = [0,288,294,295]
files = [File1,File2,File3,File4]
assert(len(cs)==len(files))
Saturation = 0.3 # in %
concatenate_scans (path, files, cs)

print('finished')
