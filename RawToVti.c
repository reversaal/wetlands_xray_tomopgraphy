/*****************************************************************************************
This program aims atconverting a raw file containing labels from a PSD to a .vti file that
could be visualized using paraview

*****************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
//Structures

void VTI_PSD();
void free3D_UC(unsigned char***, int, int);

//Global Variables
int Nx, Ny, Nz, size;
unsigned char ***image;     //Input original image
char run_name[256]; //name of the run
int main()
{
    FILE *fp1;

    char fname[256];
    int i, j, k;

    /****************************************************************************************************************
     DATA GIVEN BY USER
    ****************************************************************************************************************/
    // Input and output files
    printf("Enter the file name without '.raw' :\n");
    scanf("%s", &run_name);

    printf("Enter the size of the cubic ROI :\n");
    scanf("%i", &size);

    Nx =size;
    Ny =size;
    Nz =size;


    /****************************************************************************************************************/
    //preparing file names
    strcpy(fname,run_name);
    strncat(fname,".raw",4);
    printf("input raw file : %s\n",fname);


    printf("Allocating memory now ... \n");
    // Allocate memory for the input image data(interface)
    image=(unsigned char***)malloc(sizeof(unsigned char**)*Nx);
    for(i =0; i<Nx; i++)
    {
        image[i] =(unsigned char**)malloc(sizeof(unsigned char*)*Ny);
        for(j=0; j<Ny; j++)
        {
            image[i][j]=(unsigned char*)malloc(sizeof(unsigned char)*Nz);
        }
    }


    // read raw data file
    printf("reading input file ... \n");
    fp1=fopen(fname, "r+b");

    for(k=0; k<Nz; k++)
    {
        for(j=0; j<Ny; j++)
        {
            for(i=0; i<Nx; i++)
            {
                fread(&image[i][j][k],sizeof(unsigned char),1,fp1);
            }
        }
    }
    // close raw data file
    fclose (fp1);

  
//Saving VTI files
    printf("Writing VTI file... \n");
    VTI_PSD();

    /****************************************************************************************************************
    Cleaning the memory
    ****************************************************************************************************************/
    free3D_UC(image,Nx,Ny);

    return 0 ;
}

/***************************************************************************************************************
FUNCTIONS
****************************************************************************************************************/

//*******************************************************************
//This FUNCTION save the data from label label in a VTI file
//*******************************************************************
void VTI_PSD()
{
    FILE *outfile;
    char fname2[256];
    unsigned char*** ROI;
    int i,j,k;
    int sizeBlock;
    int imax,jmax,kmax,ri,rj,rk;
    unsigned char temp,min,max;
// Creating the output file name
    strcpy(fname2,run_name);
    strncat(fname2,".vti",4);
    
//Computing ROI corner and ROI size
    ri=0;
    rj=0;
    rk=0;

    imax=Nx-1;
    jmax=Ny-1;
    kmax=Nz-1;

    //Creating Array ROI
    // Allocate memory for the input image data(interface)
    ROI=(unsigned char***)malloc(sizeof(unsigned char**)*(Nx));
    for(i =0; i<Nx; i++)
    {
        ROI[i] =(unsigned char**)malloc(sizeof(unsigned char*)*(Ny));
        for(j=0; j<Ny; j++)
        {
            ROI[i][j]=(unsigned char*)malloc(sizeof(unsigned char)*(Nz));
        }
    }
//Initialisation of ROI at the value 64 that should not be used as a Phase value
    max=0;
    min=255;
    for (i=0; i<Nx; i++)
    {
        for (j=0; j<Ny; j++)
        {
            for (k=0; k<Nz; k++)
            {
                ROI[i][j][k]=image[i][j][k];
                if(ROI[i][j][k]<min)min=ROI[i][j][k];
		if(ROI[i][j][k]>max)max=ROI[i][j][k];
            }
        }
    }

    /*This function creates a VTI file for the meniscus lab */
    outfile = fopen(fname2,"w");
    //fprintf(outfile, "<?xml version=\"1.0\"?>\n");
    //fprintf(outfile, "<!-- From GeoDict -->\n");
    fprintf(outfile, "<VTKFile type=\"ImageData\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
    fprintf(outfile, "  <ImageData WholeExtent=\"%d %d %d %d %d %d\" Origin=\"%d %d %d\" Spacing=\"1 1 1\">\n",0,imax,0,jmax,0,kmax,ri,rj,rk);//ri-1,rj-1,rk-1);
    fprintf(outfile, "   <Piece Extent=\"%d %d %d %d %d %d\">\n",0,imax,0,jmax,0,kmax); //,ri,rj,rk,ri+di+2,rj+dj+2,rk+dk+2);
    fprintf(outfile, "    <PointData>\n");
// OUTPUT PSD class of type unsigned char
    // number of bytes in the block of a single scalar of type unsigned char
    sizeBlock=imax*jmax*kmax*sizeof(unsigned char);
    fprintf(outfile, "     <DataArray type=\"UInt8\" Name=\"PSD_class\" format=\"appended\"  RangeMin=\"%i\" RangeMax=\"%i\" offset=\"0\"/>\n",min,max);
    fprintf(outfile, "    </PointData>\n");
    fprintf(outfile, "    <CellData>\n");
    fprintf(outfile, "    </CellData>\n");
    fprintf(outfile, "   </Piece>\n");
    fprintf(outfile, "  </ImageData>\n");
// Put all the data in the appended section just before the end of the file cf : http://www.vtk.org/Wiki/VTK_XML_Formats
    fprintf(outfile, "  <AppendedData encoding=\"raw\">\n");
// First character is alway"_"
    fprintf(outfile, "_");
// Writing the phase field
    sizeBlock=Nx*Ny*Nz*sizeof(unsigned char);
    fwrite(&sizeBlock,sizeof(int),1,outfile);;
    for (k=0; k<Nz; k++)
    {
        for (j=0; j<Ny; j++)
        {
            for (i=0; i<Nx; i++)
            {
                temp = ROI[i][j][k];
                fwrite(&temp,sizeof(unsigned char),1,outfile);
            }
        }
    }
    fclose(outfile);
    free3D_UC(ROI,Nx,Ny);
}

void free3D_UC(unsigned char*** arr3D, int a, int b)
{
    int i,j;
    for(i=0; i<a; i++)
    {
        for(j=0; j<b; j++)
        {
            free(arr3D[i][j]);
        }
        free(arr3D[i]);
    }
    free(arr3D);
}

