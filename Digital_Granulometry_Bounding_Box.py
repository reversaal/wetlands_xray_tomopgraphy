#################################################
# Granulometry tomographies & FIJI objects counter
#################################################
# This granulometries are based on the selection 
# of the second bigest dimension of the grains (from its bounding box)

#################################################
# Granulometry tomographies & FIJI objects counter
#################################################
import pandas as pd
from   os import chdir
import numpy as np
import matplotlib.pyplot as plt
import tifffile

chdir  ('/home/german.martinez-carvajal/Documents/Tomographie/170404MON/02/Object Counter')
Data       = pd.read_csv("Statistics for gravel MON02.csv")
Resolution = 0.035 # mm/vox
Erosion_R  = 20 # Erosion radius in voxels
# Erosion radius in voxels (Before the object counter of FIJI an erosion step 
# was made to separate gravel grains)

Data       = Data.sort_values(by = ['Volume (pixel^3)'], ascending = False)

# Correcting volumes and radii of particles due to erosion 
Volumes_Er = Data['Volume (pixel^3)']*(Resolution**3) # Eroded volumes in mm3
Volumes_Er = np.array(Volumes_Er)

# Eroded Boundig Boxes dimensions
B_Heights   = np.array(Data['B-height'])*Resolution # in mm
B_Widths    = np.array(Data['B-width' ])*Resolution
B_Depths    = np.array(Data['B-depth' ])*Resolution

B_Box_eroded      = np.zeros((len(B_Heights),3))
B_Box_eroded[:,0] = B_Heights
B_Box_eroded[:,1] = B_Widths
B_Box_eroded[:,2] = B_Depths
B_Box_eroded      = np.sort(B_Box_eroded, axis = 1)

# Volume of the eroded bounding boxes
BB_Volumes_Er     = np.prod(B_Box_eroded, axis = 1)

# Ratio eroded volume / bounding box volume
Ratios     = Volumes_Er/BB_Volumes_Er 

# Volume of BB corrected by erosion
B_Box      = B_Box_eroded + (2*Erosion_R)*Resolution
BB_Volumes = np.prod(B_Box, axis = 1)

# Correction of particles volume
Volumes    = Ratios*BB_Volumes

# Caracteristic dimiension
B_Box      = np.sort(B_Box, axis = 1)
Dim_char   = B_Box[:,1]
Radii      = Dim_char


Tot_volume = np.sum(Volumes)           # in mm3
RadiusMax  = np.max(Radii)
RadiusMin  = np.min(Radii)

# Save radii into the pd data frame
Data['RadiiChar (mm)'] = Radii

# Number of points to draw the distributions
Points_num = 30

# Computing histogram (Radii vs Volume)
Radii_Classes    = np.linspace(0,RadiusMax,Points_num+1)
Volumes_Retained = list() # Soting the total volume of grains retained in each radii class
for i in range(Points_num):
    Bottom          = Radii >  Radii_Classes[i  ]  
    Top             = Radii <= Radii_Classes[i+1]
    Inside          = Bottom & Top
    Volume_Retained = np.sum(Volumes[Inside])
    Volumes_Retained.append (Volume_Retained)
# Computing volume cumulative frequency
Counter    = 0
Cumulative = list()
for Volume in Volumes_Retained:
    Counter    += Volume # in mm3
    Cumulative.append(Counter)
Cumulative = Cumulative/Tot_volume*100    

plt.figure(1)
plt.plot(Radii_Classes[1:], Cumulative, label = "lin_bins", marker ="*")
plt.xlabel('Radii (mm)')
plt.ylabel('Cumulative % (volume)')
plt.legend()


Labels = np.array (Data['#'])
## Cheking big particles
#
#Gravels = tifffile.imread('Objects map of gravel MON02.tif')
#Grain1     = (Gravels == Labels[0])
#Grain2     = (Gravels == Labels[1])
#Big_Gravel = Grain1 | Grain2
#Big_Gravel = np.array(Big_Gravel, dtype = np.uint8)*255
#tifffile.imsave('BigGravel.tif',Big_Gravel)
print('FINISHED')