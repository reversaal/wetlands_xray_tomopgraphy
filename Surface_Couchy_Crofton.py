import tifffile
from   os    import chdir 
import numpy  as np
import pandas as pd 
from   matplotlib import pyplot as plt
from   sys import getsizeof
from   scipy.ndimage import generic_filter as gfilter

def SurfaceCouchyCrofton(Image, Resolution, Phase1, Phase2):
    # Importing the image
    # Defining 1 and 2 as the label for the phases of interest
    Image[Image == Phase1] = 1
    Image[Image == Phase2] = 2
    Size_Image  = getsizeof(Image)/2**20 #  in MB
    print('Size_Image (megaBytes) = ',Size_Image)
    # An eveloping ROI box is needed
    Shape = np.array(Image.shape) + 2
    ROI   = np.zeros(Shape, dtype = np.uint8)
    # Filing ROI with Image Values
    ROI[1:-1,1:-1,1:-1] = Image
    Size_ROI            = getsizeof(ROI)/2**20 #  in M
    print('Size_ROI (megaBytes) = ',Size_ROI)
    del Image
    
    # Some counters are needed
    # Connectivity = root(1) neighbors
    Family1 = 0
    NeighborsF1 = np.zeros(6)
    # Connectivity = root(2) neighbors
    Family2 = 0
    NeighborsF2 = np.zeros(18)
    # Connectivity = root(3) neighbors
    Family3 = 0
    NeighborsF3 = np.zeros(8)
    
    # Saving coordinates of every element in phase 1
    print('Saving Coordinates')
    Coordinates = np.where(ROI == 1)
    Coordinates = np.array(Coordinates, dtype = np.uint8)
    Size_Coordinates = getsizeof(Coordinates)/2**20 #  in MB
    print('Size_Coordinates (megaBytes) = ',Size_Coordinates)
    
    
    # This is another way to save the coordinates when there are memory errors
    #Coordinates = np.empty((3,0), dtype = np.uint8)
    #for Row in range(ROI.shape[0]):
    #    Coordinates_Row2D    = np.where(ROI[Row] == 1)
    #    Z                    = np.ones ((1,len(Coordinates_Row2D[0])))*Row
    #    Coordinates_Row      = np.append(Z, Coordinates_Row2D, axis = 0)
    #    Coordinates_Row      = np.array (Coordinates_Row, dtype = np.uint8)
    #    Coordinates          = np.append(Coordinates,Coordinates_Row, axis = 1)

    # Saving coordinates into a panda frame
    Dict            = {'z':Coordinates[0], 'x':Coordinates[1],'y':Coordinates[2] } 
    FrameCoord       = pd.DataFrame(Dict)
    Size_FrameCoord  = getsizeof(FrameCoord) /2**20 #  in MB
    print('Size_FrameCoord (megaBytes) =',Size_FrameCoord)
    Max = len(FrameCoord)
    print('# Evaluations to perform = ', Max)    
    print(Max, 'Max')
    print(STOP)
        
    print('Computing contributions')
    for index in FrameCoord.index :
        z = FrameCoord.loc[index,'z']
        x = FrameCoord.loc[index,'x']
        y = FrameCoord.loc[index,'y']
        # Evaluating neighbors with connectivity = root(1)     
        NeighborsF1[0]  = (ROI[z+1, x  , y  ] == 2)
        NeighborsF1[1]  = (ROI[z  , x+1, y  ] == 2)
        NeighborsF1[2]  = (ROI[z  , x  , y+1] == 2)
        NeighborsF1[3]  = (ROI[z-1, x  , y  ] == 2)
        NeighborsF1[4]  = (ROI[z  , x-1, y  ] == 2)
        NeighborsF1[5]  = (ROI[z  , x  , y-1] == 2)
        Family1        += np.sum(NeighborsF1)
        # Evaluating neighbors with connectivity = root(2)    
        NeighborsF2[0]  = (ROI[z  , x+1, y+1] == 2)
        NeighborsF2[1]  = (ROI[z  , x-1, y+1] == 2)
        NeighborsF2[2]  = (ROI[z  , x+1, y-1] == 2)
        NeighborsF2[3]  = (ROI[z  , x-1, y-1] == 2)
        NeighborsF2[4]  = (ROI[z+1, x  , y-1] == 2)
        NeighborsF2[5]  = (ROI[z+1, x  , y+1] == 2)
        NeighborsF2[6]  = (ROI[z+1, x+1, y  ] == 2)
        NeighborsF2[7]  = (ROI[z+1, x-1, y  ] == 2)
        NeighborsF2[8]  = (ROI[z-1, x  , y-1] == 2)
        NeighborsF2[9]  = (ROI[z-1, x  , y+1] == 2)
        NeighborsF2[10] = (ROI[z-1, x+1, y  ] == 2)
        NeighborsF2[11] = (ROI[z-1, x-1, y  ] == 2)
        Family2        += np.sum(NeighborsF2)
        # Evaluating neighbors with connectivity = root(3)
        NeighborsF3[0]  = (ROI[z+1, x+1, y+1] == 2)
        NeighborsF3[1]  = (ROI[z+1, x-1, y+1] == 2)
        NeighborsF3[2]  = (ROI[z+1, x+1, y-1] == 2)
        NeighborsF3[3]  = (ROI[z+1, x-1, y-1] == 2)
        NeighborsF3[4]  = (ROI[z-1, x+1, y+1] == 2)
        NeighborsF3[5]  = (ROI[z-1, x-1, y+1] == 2)
        NeighborsF3[6]  = (ROI[z-1, x+1, y-1] == 2)
        NeighborsF3[7]  = (ROI[z-1, x-1, y-1] == 2)
        Family3        += np.sum(NeighborsF3)
        if index%1000 == 0:
            print(index)        
        if index%(Max/100) == 0:
            print(index/(Max/100)*100,'%')
    
    # This are the discretization weight associated to directions
    # taken from the Osher mucklish book
    VoronoiFactor1 = 0.045777895 
    VoronoiFactor2 = 0.0369806261       
    VoronoiFactor3 = 0.035195639
    
    # Surface of the cuting element
    Lambda_1 = np.sqrt(1)/(Resolution**2)
    Lambda_2 = np.sqrt(2)/(Resolution**2)
    Lambda_3 = np.sqrt(3)/(Resolution**2)
    
    # Computing family contributions
    CC1 = VoronoiFactor1/Lambda_1 * Family1
    CC2 = VoronoiFactor2/Lambda_2 * Family2
    CC3 = VoronoiFactor3/Lambda_3 * Family3
    
    # Computing the surface
    Surface = 4*(CC1 + CC2 + CC3)
    #print('function executed')
    return (Surface)
#################################################
#################################################

# MAIN
chdir      ('/home/german.martinez-carvajal/Documents/Tomographie/170404MON/02')
Image_Name = 'HashemiConcatenated(0-128-255)' + '.tif'
Image      = tifffile.imread(Image_Name)
Phase1     = 0   # Label for air
Phase2     = 128 # Label for Organic Matter
Resolution = 0.035 # mm/vox
Surface   = SurfaceCouchyCrofton(Image, Resolution, Phase1, Phase2)


print('FINISHED')