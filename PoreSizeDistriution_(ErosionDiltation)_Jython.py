# """ Pore size profile by erosion Dilation """

def Pore_Size_Distribution(Dir, FileName):
	""" Output: Saves a csv file with the volume cummulative distribution of pore sizes for an image """
	""" Inputs: Dir: the directory/folder of the image, and where the csv will be saved """
	""" FileName : Name of the file """
	""" File must be a segmented image, i.e. binary, values are only 0 or 255 """
	""" The pore phase has to be labeled 255 """

	from os import path, chdir
	assert (path.isdir(Dir)) , "Folder IS NOT A FOLDER: " + folder
	assert (path.exists(FileName)) , "FILE DOES NOT EXIST: " + FileName
	
	# Reading Segmented Image
	# Imp is the air-phase (this is the result of the Hashemi's segmentation, by Hashemi's method normally)
	from ij import IJ
	Imp_Air  = IJ.openImage(FileName)
	Imp_Air.show()
	
	def get_Num_Air_Pixels (Imp):
		# Computing the number of Air Pixels
		from ij.process import StackStatistics
		SS         = StackStatistics(Imp) 
		Histogram  = SS.getHistogram() # Histogram is an array of longs
		Histogram  = map(int, Histogram) # So here I change loongs to int numbers
		Num_Air_Pixels = Histogram[-1] # -1 is the 256 bin. 255 = white = Air
		return Num_Air_Pixels
	
	# COMPUTING THE PORE SIZE DISTRIBUTION
	
	# Initializing
	Initial = get_Num_Air_Pixels (Imp_Air)
	print('Initial number of Air voxels                      = ', Initial)    
	Max_radius  = max(Imp_Air.width, Imp_Air.height, Imp_Air.getNSlices()) # Max_radius will control the main loop
	Radii       = [0] # list of radii  of pores  (will grow within th loop)
	Cum_Volumes = [0] # list of volumes of pores belonging to a radii class (will grow in the loop)
	print('Max Radius:', Max_radius)
	
	from ij import ImagePlus
	Erosion = ImagePlus('Erosion', Imp_Air.getImageStack())
	Erosion.show()
	for Radius in range(1,Max_radius):
	    print('Step: Radius of erosion (in pixels) = ', Radius)
	    
	    # Computing erosions
	    IJ.run(Erosion, "Erode (3D)", "iso=255") # IJ.run("Erode 3D") is a change in place method (there is no need to reasign the result of the Erosion)
	    # Computing dilations  
	    Dilation = ImagePlus('Dilation', Erosion.getImageStack())
	    for repeat in range(Radius):
	        IJ.run(Dilation, "Dilate (3D)", "iso=255")
	
	    Remaining = get_Num_Air_Pixels (Dilation)
	    print('Remaining air voxels (after erosion-dilation) = ', Remaining)
	 
	    # Pore size distribution
	    # Radius (in vox)
	    Radii.append(Radius)
	    # Volume of pores smaller than radius (in vox)
	    Volume = Initial - Remaining
	    Cum_Volumes.append(Volume)
	    # Breaking if procedure completed
	    Dilation.close()
	    if Remaining == 0:
	        break
	
	# Saving
	chdir(Dir)
	File = open('Cumul_distribution.csv', mode = 'w')
	File.write('Radius (vox)' + ',' + 'Cumul Volume (vox)' + '\n')
	for i in range(len(Radii)):
		File.write(str(Radii[i]) + ',' + str(Cum_Volumes[i]) + '\n')
	File.close()
	print("Finished !")    



# MAIN
# Report start-time of the execution
import datetime
Start = datetime.datetime.today() 
# Directory and File Name
Dir  = '/home/german.martinez-carvajal/Desktop/2018/Pore_Size_Distribution/Test'
#Dir  = '/home/german.martinez-carvajal/Desktop/2017/Tomographie/170411COP/Segmentation(Hashemi)'
FileName = Dir + "/" + "Binary.tif"
#FileName = Dir + "/" + "Air.tif"
# Calling the function
Pore_Size_Distribution(Dir, FileName)
# Report end-time of the execution
End = datetime.datetime.today()
# Saving report
from os import chdir
chdir (Dir)
File = open('Time_Report.txt', mode = 'w')
File.write('Start time: ' + str(Start) + '\n')
File.write('End   time: ' + str(End  ) + '\n')
File.close()