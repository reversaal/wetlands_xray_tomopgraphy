Repository of the codes used in German Dario Martinez PhD thesis (2020) for the post-processing of X-ray computed tomography images.

German Dario Martinez Carvajal., 2020. Etude des filtres plantés de roseaux par la tomographie à rayons X. Université Claude Bernard, Lyon.
