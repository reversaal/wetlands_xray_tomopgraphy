import datetime
import numpy as np
import matplotlib.pyplot as plt 
import pandas as pd
import tifffile
from scipy.stats       import norm
from os import mkdir, chdir, path
from scipy.optimize    import curve_fit
import scipy.ndimage
from sys import getsizeof 
import psutil # To get some memory and CPU stats:

def interface_filling(B_Air,B_OrM,B_Grv, slices_VS):
    #########################################################################################
    ## INTERFACE FILLING
    #########################################################################################
    #print("Performing interface filling - this may take a while")
    #def PREDOMINANT_PHASE (box):
    #    count_phase_64  = np.count_nonzero((box==64))  #Air
    #    count_phase_128 = np.count_nonzero((box==128)) #OrM
    #    count_phase_192 = np.count_nonzero((box==192)) #Grv
    #    Maximum = max(count_phase_64,count_phase_128,count_phase_192)    
    #    if Maximum == count_phase_64:
    #        predominant_phase = 64
    #    elif Maximum == count_phase_128:
    #        predominant_phase = 128
    #    elif Maximum == count_phase_192:
    #        predominant_phase = 192
    #    return predominant_phase
    #Footprint = np.ones((3,3,3))
    #Image_Interface_Filled = scipy.ndimage.generic_filter(Grown_Image,PREDOMINANT_PHASE,footprint = Footprint)
    ## In this step we are adding the recently attributed interface voxels
    #Image_Final = Grown_Image*(-Interface) + Image_Interface_Filled*(Interface) + Image_Interface_Filled*(Grown_Image==0) # Grown Image + Interface filling + Not attributed Voxels during loop
    #Name = "Hashemi_Segmentation"
    #tifffile.imsave(Name + ".tif", Image_Final)
    #print("Finished")
    
    ########################################################################################
    # INTERFACE FILLING Version#2
    ########################################################################################
    print ("STAGE: Interface filling version 2")
     
    # Filling the interface by dilation (more steps if neccessary)
    Grown_Image = 0           + np.array(B_Air,dtype=np.uint8)*64                  
    Grown_Image = Grown_Image + np.array(B_OrM,dtype=np.uint8)*128
    Grown_Image = Grown_Image + np.array(B_Grv,dtype=np.uint8)*192
    
    # Tracking atributed Voxels
    Counter_GrowSteps_Interface = 0 
    Attributed_Voxels_Interface = list ()
    Air,OrM,Grv = np.count_nonzero(B_Air),np.count_nonzero(B_OrM),np.count_nonzero(B_Grv)
    Total = Air + OrM + Grv
    Attributed_Voxels_Interface.append ([Counter_GrowSteps_Interface,Air,OrM,Grv,Total])
    
    counter = 0
    while np.count_nonzero(Grown_Image==0) > 0:
        Counter_GrowSteps_Interface += 1
        print("step=",Counter_GrowSteps_Interface)
        B_new_Air = scipy.ndimage.morphology.binary_dilation(B_Air)     
        B_new_OrM = scipy.ndimage.morphology.binary_dilation(B_OrM)     
        B_new_Grv = scipy.ndimage.morphology.binary_dilation(B_Grv)     
        # Eliminating overwriting
        B_new_Air = B_new_Air & (~(B_OrM | B_Grv)) 
        B_new_OrM = B_new_OrM & (~(B_Air | B_Grv))
        B_new_Grv = B_new_Grv & (~(B_OrM | B_Air))
        # Assigning priorities for the last dilation
        B_new_OrM =  B_new_OrM & (~B_new_Air)
        B_new_Grv =  B_new_Grv & (~B_new_Air) & (~B_new_OrM)
        print(np.count_nonzero(B_new_Air),np.count_nonzero(B_new_OrM),np.count_nonzero(B_new_Grv))
        # Creating new segmentation
        Grown_Image = 0           + np.array(B_new_Air,dtype=np.uint8)*64
        Grown_Image = Grown_Image + np.array(B_new_OrM,dtype=np.uint8)*128
        Grown_Image = Grown_Image + np.array(B_new_Grv,dtype=np.uint8)*192
        print("Remaining Interface Voxels to fill = " , np.count_nonzero(Grown_Image==0))
        Messages.append("Step Interface filling = {} ".format(counter))
        Messages.append("Remaining Interface Voxels to fill = {}".format(np.count_nonzero(Grown_Image==0)))
        MessagesDF = pd.DataFrame({"Message":Messages})
        Title = 'Messages_' + Scan_File.replace(Dir + '/' , "")[:-4] + '.csv' 
        chdir(path_lr)    
        MessagesDF.to_csv(Title, encoding = "utf-8")
            
        counter += 1
        # Reasigning names for next loop
        B_Air = B_new_Air                                                               
        B_OrM = B_new_OrM                                                               
        B_Grv = B_new_Grv                                                               
        Name = "Grown_Image_IntFil" + str(Counter_GrowSteps_Interface)# +1 just beacuse Counter starts at 0 :)

        # Tracking atributed Voxels
        Air,OrM,Grv = np.count_nonzero(B_Air),np.count_nonzero(B_OrM),np.count_nonzero(B_Grv)
        Total = Air + OrM + Grv
        Attributed_Voxels_Interface.append ([Counter_GrowSteps_Interface,Air,OrM,Grv,Total])
        
    Messages.append("# of Interface filling Steps = " + str(Counter_GrowSteps_Interface))       
                     
    # Retrieving memory
    del B_Air, B_OrM, B_Grv, B_new_Air, B_new_OrM, B_new_Grv  
    
    Image_Final = Grown_Image                                 
    del Grown_Image                                           
       
    # Checking
    Remaining_Voxels_this_step = np.count_nonzero((Image_Final==0)) 
    if (Remaining_Voxels_this_step==0):
        print ("Succes - Interface filling OK")
        Messages.append("Succes - Interface filling OK")
    else:
        print ("Failure, Interface Filling NOT OK, remaining Voxels = ", Remaining_Voxels_this_step)
        Messages.append("Failure, Interface Filling NOT OK, remaining Voxels = " + str(Remaining_Voxels_this_step))
    
    # Changing values for each phase for 3d rendering purposes (Fiji viewer)
    Image_Final[(Image_Final==64 )] = 0    # Air
    Image_Final[(Image_Final==128)] = 128  # OrM 
    Image_Final[(Image_Final==192)] = 255  # Grv
    
    return(Image_Final)
    
    
def region_growing(Seed_Image, slices_VS, Save_Step):
    print ("STAGE: Region Growing")
    global stats_RAM 
    
    # Spliting the seed for each phase
    B_Air = (Seed_Image==64)
    stats_RAM = get_RAM_usage(stage = 'After Split seed phase Air', path = path_lr, frame_stats =stats_RAM)
                                                            
    B_OrM = (Seed_Image==128)
    stats_RAM = get_RAM_usage(stage = 'After Split seed phase OrM', path = path_lr, frame_stats =stats_RAM)
                                                          
    B_Grv = (Seed_Image==192)                                                           
    stats_RAM = get_RAM_usage(stage = 'After Split seed phase Grv', path = path_lr, frame_stats =stats_RAM)
    

    del Seed_Image                                                                      
    # Tracking atributed and not attributed Voxels
    Remaining_Voxels  = list() # Voxels that have not been attributed
    Air,OrM,Grv,Itf, Tot = list(), list(), list(), list(), list()
    Air.append(np.count_nonzero(B_Air))
    OrM.append(np.count_nonzero(B_OrM))
    Grv.append(np.count_nonzero(B_Grv))
    Itf.append(0) # Intercate
    Tot.append(Air[-1]+OrM[-1]+Grv[-1]+Itf[-1]) # Total voxels
    # For Loop
    Max_iterations = np.max(Image.shape)
    
    Messages.append("")
    for Counter in range(Max_iterations):
    
        # Performing the dilation
        Dilated_Air = scipy.ndimage.morphology.binary_dilation(B_Air)                   
        if Counter == 0:
            stats_RAM = get_RAM_usage(stage = 'Dilation Air', path = path_lr, frame_stats =stats_RAM)
        Dilated_OrM = scipy.ndimage.morphology.binary_dilation(B_OrM)
        if Counter == 0:                   
            stats_RAM = get_RAM_usage(stage = 'Dilation OrM', path = path_lr, frame_stats =stats_RAM)
        Dilated_Grv = scipy.ndimage.morphology.binary_dilation(B_Grv)                   
        if Counter == 0:
            stats_RAM = get_RAM_usage(stage = 'Dilation Grv', path = path_lr, frame_stats =stats_RAM)

        # Computing Neighboors
        N_Air = Dilated_Air & (~B_Air)
        if Counter == 0:
            stats_RAM = get_RAM_usage(stage = 'Computing Neighbors Air', path = path_lr, frame_stats =stats_RAM)
        N_OrM = Dilated_OrM & (~B_OrM)
        if Counter == 0:
            stats_RAM = get_RAM_usage(stage = 'Computing Neighbors OrM', path = path_lr, frame_stats =stats_RAM)
        N_Grv = Dilated_Grv & (~B_Grv)                                                  
        if Counter == 0:
            stats_RAM = get_RAM_usage(stage = 'Computing Neighbors Grv', path = path_lr, frame_stats =stats_RAM)

        # Selecting Neightbors = Eliminatig Neighbors that are included in the other phase's dilation
        M_Air = N_Air & (~(Dilated_OrM | Dilated_Grv))
        if Counter == 0:
            stats_RAM = get_RAM_usage(stage = 'Selecting Neighbors Air', path = path_lr, frame_stats =stats_RAM)
        M_OrM = N_OrM & (~(Dilated_Air | Dilated_Grv))
        if Counter == 0:
            stats_RAM = get_RAM_usage(stage = 'Selecting Neighbors OrM', path = path_lr, frame_stats =stats_RAM)
        M_Grv = N_Grv & (~(Dilated_OrM | Dilated_Air))                                  
        if Counter == 0:
            stats_RAM = get_RAM_usage(stage = 'Selecting Neighbors Grv', path = path_lr, frame_stats =stats_RAM)

        del Dilated_Air                                                                 
        del Dilated_OrM                                                                 
        del Dilated_Grv                                                                                                               
        # Including Tolerance Threshold
        M_Air, M_OrM, M_Grv = Include_Tolerance_Threshold(Image, M_Air, M_OrM, M_Grv, Parameters, slices_VS)
        # Computing the new phase
        B_new_Air = B_Air | M_Air                                                       
        B_new_OrM = B_OrM | M_OrM                                                       
        B_new_Grv = B_Grv | M_Grv                                                       
        if Counter == 0:
            stats_RAM = get_RAM_usage(stage = 'Computing new phase', path = path_lr, frame_stats =stats_RAM)
        del M_Air                                                                       
        del M_OrM                                                                       
        del M_Grv                                                                       
        del B_Air                                                                       
        del B_OrM                                                                       
        del B_Grv                                                                           
        # Interface voxels 
        Interface = ((N_Air | N_OrM) | N_Grv ) & (~((B_new_Air | B_new_OrM) | B_new_Grv )) 
        if Counter == 0:
            stats_RAM = get_RAM_usage(stage = 'Interface Voxels', path = path_lr, frame_stats =stats_RAM)
        del N_Air                                                                       
        del N_OrM                                                                       
        del N_Grv                                                                       
        # Defining the Grown Image
        Grown_Image = 0           + np.array(B_new_Air,dtype=np.uint8)*64                  
        Grown_Image = Grown_Image + np.array(B_new_OrM,dtype=np.uint8)*128
        Grown_Image = Grown_Image + np.array(B_new_Grv,dtype=np.uint8)*192
        Grown_Image = Grown_Image + np.array(Interface,dtype=np.uint8)*255
        if Counter == 0:
            stats_RAM = get_RAM_usage(stage = 'Defining Grown Image', path = path_lr, frame_stats =stats_RAM)

        # Saving Only every x steps
        if ((Counter % Save_Step == 0) & (Counter != 0)):
            Name = "Grown_Image_" + str(Counter)
            chdir(path_hr)
            tifffile.imsave(Name + ".tif", Grown_Image)
        # Reasigning names for next loop step
        B_Air = B_new_Air                                                               
        B_OrM = B_new_OrM                                                               
        B_Grv = B_new_Grv                                                               
        if Counter == 0:
            stats_RAM = get_RAM_usage(stage = 'Reasigning for next loop', path = path_lr, frame_stats =stats_RAM)

        # Tracking atributed and not attributed Voxels
        Remaining_Voxels_this_step = np.count_nonzero((Grown_Image==0))
        Remaining_Voxels.append(Remaining_Voxels_this_step)
        Air.append(np.count_nonzero(B_Air))
        OrM.append(np.count_nonzero(B_OrM))
        Grv.append(np.count_nonzero(B_Grv))
        Itf.append(np.count_nonzero(Interface)) # Intercate
        Tot.append(Air[-1]+OrM[-1]+Grv[-1]+Itf[-1]) # Total voxels
        del B_new_Air                                                                   
        del B_new_OrM                                                                   
        del B_new_Grv                                                                   
        del Interface                                                                   
        # Loop control - checking growth - and printing messages for each case
        print("Step =", str(Counter))
        
    
        if (np.logical_and(Counter>0,Remaining_Voxels[Counter]==Remaining_Voxels[Counter-1])):
            Message = "Loop Broken - Growing stopped, remaining uncertain voxels step(-i) = step(i) = " + str(Remaining_Voxels_this_step) +" voxels"
            print(Message)
            Messages.append(Message)
            Messages.append("")

            MessagesDF = pd.DataFrame({"Message":Messages})
            Title = 'Messages_' + Scan_File.replace(Dir + '/' , "")[:-4] + '.csv' 
            chdir(path_lr)    
            MessagesDF.to_csv(Title, encoding = "utf-8")
            break

        elif (Remaining_Voxels_this_step == 0):
            print ("Succes : Loop Broken - All Voxels were attributed durring Growing")
            Messages.append("Succes : Loop Broken at step:"  + str(Counter) + "- All Voxels were attributed durring Growing")
            Messages.append("")

            MessagesDF = pd.DataFrame({"Message":Messages})
            Title = 'Messages_' + Scan_File.replace(Dir + '/' , "")[:-4] + '.csv' 
            chdir(path_lr)    
            MessagesDF.to_csv(Title, encoding = "utf-8")
            break

        elif (Counter == Max_iterations-1):
            print ("Failure: Max_iterations reached")
            Messages.append("Failure: Max_iterations reached: "+ str(Max_iterations) )
            Messages.append("")

            MessagesDF = pd.DataFrame({"Message":Messages})
            Title = 'Messages_' + Scan_File.replace(Dir + '/' , "")[:-4] + '.csv' 
            chdir(path_lr)    
            MessagesDF.to_csv(Title, encoding = "utf-8")

        else:
            print("Growing loop going on")
            print("Number of remaining uncertain Voxels =", str(Remaining_Voxels_this_step))
            
            
            Messages.append('Loop Going on : step =  ' + str(Counter))
            Messages.append('Number of remaining uncertain Voxels = ' + str(Remaining_Voxels_this_step))
            MessagesDF = pd.DataFrame({"Message":Messages})
            Title = 'Messages_' + Scan_File.replace(Dir + '/' , "")[:-4] + '.csv' 
            chdir(path_lr)    
            MessagesDF.to_csv(Title, encoding = "utf-8")
            
        del Grown_Image                                                                

        if Counter == 0:
            stats_RAM = get_RAM_usage(stage = 'End of the growing step', path = path_lr, frame_stats =stats_RAM)

    
        # Tracking atributed and not attributed Voxels
        Attributed_Voxels_Growing = pd.DataFrame({"Air":Air,"OrM":OrM,"Grv":Grv,"Itf":Itf,"Tot":Tot})
        # Ploting
        plt.figure()
        # In fig() 
        plt.plot(Attributed_Voxels_Growing.index,Attributed_Voxels_Growing.Air,color="k",lw=1, label = "Air Voxels", linestyle = '-')
        plt.plot(Attributed_Voxels_Growing.index,Attributed_Voxels_Growing.OrM,color="k",lw=1, label = "OrM Voxels", linestyle = '--')
        plt.plot(Attributed_Voxels_Growing.index,Attributed_Voxels_Growing.Grv,color="k",lw=1, label = "Grv Voxels", linestyle = '-.')
        plt.plot(Attributed_Voxels_Growing.index,Attributed_Voxels_Growing.Itf,color="k",lw=1, label = "Int Voxels", linestyle = ':')
        plt.plot(Attributed_Voxels_Growing.index,Attributed_Voxels_Growing.Tot,color="k",lw=2, label = "Sum", linestyle = '-')
        plt.axhline(y = np.product(Image.shape), label = " Image Size", color = "red")
        plt.title('Growing')
        plt.xlabel('Step')
        plt.ylabel('# Attributed Voxels')
        plt.legend(fontsize = "small", loc = 'upper left')
        # Saving the figure
        chdir(path_lr)
        Title = 'Growing_of_' + Scan_File.replace(Dir + '/' , "")[:-4]
        plt.savefig(Title)
        plt.close('all')
    
    return (B_Air,B_OrM,B_Grv)
    
def sector_mask(shape,centre,radius,angle_range):
    """
    CODE FROM STACK OVER FLOW
    Return a boolean mask for a circular sector. The start/stop angles in  
    `angle_range` should be given in clockwise order.
    """

    x,y = np.ogrid[:shape[0],:shape[1]]
    cx,cy = centre
    tmin,tmax = np.deg2rad(angle_range)

    # ensure stop angle > start angle
    if tmax < tmin:
            tmax += 2*np.pi

    # convert cartesian --> polar coordinates
    r2 = (x-cx)*(x-cx) + (y-cy)*(y-cy)
    theta = np.arctan2(x-cx,y-cy) - tmin

    # wrap angles between 0 and 2*pi
    theta %= (2*np.pi)

    # circular mask
    circmask = r2 <= radius*radius

    # angular mask
    anglemask = theta <= (tmax-tmin)

    return circmask*anglemask
    
def get_RAM_usage(stage, path, frame_stats):
    """
    returns a dict with RAM usage stats at for a stage str(stage)

    input:
    stage = str, a word describing the moment you call this function
    path = pathway where a .txt file will be saved with the stats
    frame_stats = a panda frame where stats will be saved row per row each time this function is called
    
    output:
    frame_stats : panda frame with ram usage stats
    """
    r_u = psutil.virtual_memory() # RAM usage 
    keys = ['stage','total_GB', 'used_GB', 'percent']
    values = [stage ,r_u[0]/2**30, r_u[3]/2**30, r_u[2]/2**30 ]    
    stats = dict()
    for key, value in zip(keys, values):
        stats[key] = value
    # Saving
    chdir(path)
    frame_stats = frame_stats.append(stats, ignore_index = True)
    frame_stats.to_csv('stats_RAM_{}.csv'.format(scan_file.replace(".tif","")), sep = ';')
    return frame_stats

def Include_Tolerance_Threshold(Image, M_Air, M_OrM, M_Grv, Parameters, slices_VS):
    
    if slices_VS == None :
        L = [0,len(Image)]
    else:
        L = slices_VS.copy()
        L.append(len(Image))
    
    num_sec = 1 if slices_VS == None else len(slices_VS)    
    for i in range(num_sec):
        Image_Section = Image[L[i]:L[i+1] ]
        Cut_1  = Parameters.ix[i,'Cut_1' ]
        Cut_2a = Parameters.ix[i,'Cut_2a']
        Cut_2b = Parameters.ix[i,'Cut_2b']
        Cut_3  = Parameters.ix[i,'Cut_3' ]
        M_Air[L[i]:L[i+1]] = M_Air[L[i]:L[i+1]] &  (Image_Section < Cut_2b)                                               
        M_OrM[L[i]:L[i+1]] = M_OrM[L[i]:L[i+1]] & ((Image_Section > Cut_1 ) & (Image_Section < Cut_3))                            
        M_Grv[L[i]:L[i+1]] = M_Grv[L[i]:L[i+1]] &  (Image_Section > Cut_2a)
    return M_Air, M_OrM, M_Grv
    
def Compute_Thresholds(Tolerance, Parameters, Image_type, Histograms_without_PVE, N_bins, mode = 1, slices_VS = None):
    """
        In this function the thresholds (i.e the Cuts 1,2a,2b,3) 
        
        Mode 1 = Thresholds computed using the ppf (percent point function) and the tolerance (example: Tolerance 50% of a peaks area)
        Mode 2 = Thresholds computed as the inverse of the normalized gaussian at a tolerance height, (Ex Tolerance = 0.01, the threshold of one peak is set 
            to the gray value at which height of the adjacent peak is 0.01)
                
        If there is an error in the cuts assignment in any methods (i.e. mixing) the program will warn you and stop!
        
        Tolerance may be set to 0.01, which means 1% of the height of a gaussian peak
        
        Parameter is a panda frame (created inside the Hashemi's segmentation function that stores the parameters of the gaussian fitting:
        Mu and Standard Deviation (i.e. Sigma)"""
        
    if mode == 1 :
        Cut_1  = norm.ppf(        Tolerance)*Parameters['Sigma1']+ Parameters['Mu1'] # ppf = percent point function
        Cut_2a = norm.ppf(0.5-0.5*Tolerance)*Parameters['Sigma2']+ Parameters['Mu2']
        Cut_2b = norm.ppf(0.5+0.5*0.975)*Parameters['Sigma2']+ Parameters['Mu2']
        Cut_3  = norm.ppf(1  -    0.975)*Parameters['Sigma3']+ Parameters['Mu3']
    elif mode == 2:
        Cut_1  = Parameters['Mu2'] - np.sqrt(-2*(Parameters['Sigma2']**2)*np.log(Tolerance))
        Cut_2a = Parameters['Mu1'] + np.sqrt(-2*(Parameters['Sigma1']**2)*np.log(Tolerance))
        Cut_2b = Parameters['Mu3'] - np.sqrt(-2*(Parameters['Sigma3']**2)*np.log(Tolerance))
        Cut_3  = Parameters['Mu2'] + np.sqrt(-2*(Parameters['Sigma2']**2)*np.log(Tolerance))
        
    Parameters['Cut_1' ] = Cut_1
    Parameters['Cut_2a'] = Cut_2a
    Parameters['Cut_2b'] = Cut_2b 
    Parameters['Cut_3' ] = Cut_3
    
    # This is a special line #
    
    # Plot
    # In fig(2) Plot of the optimized trimodal model AND the cuts
    def gauss(x,mu,sigma,a):
        # a = height of the peak
        return a*np.exp(-(x-mu)**2/2/sigma**2)
    def trimodal(x,mu1,sigma1,a1,mu2,sigma2,a2,mu3,sigma3,a3):
        """ Sum of three gaussian distributions"""
        return gauss(x,mu1,sigma1,a1)+gauss(x,mu2,sigma2,a2)+gauss(x,mu3,sigma3,a3)
    
    repetitions = 1 if slices_VS == None else len(slices_VS)
    for i in range(repetitions):
        Histogram_without_PVE = Histograms_without_PVE[i]
        Mu1 = Parameters.ix[i,'Mu1']
        Mu2 = Parameters.ix[i,'Mu2']
        Mu3 = Parameters.ix[i,'Mu3']
        Sigma1 = Parameters.ix[i,'Sigma1']
        Sigma2 = Parameters.ix[i,'Sigma2']
        Sigma3 = Parameters.ix[i,'Sigma3']
        A1 = Parameters.ix[i,'A1']
        A2 = Parameters.ix[i,'A2']
        A3 = Parameters.ix[i,'A3']
        Cut_1  = Parameters.ix[i,'Cut_1' ]
        Cut_2a = Parameters.ix[i,'Cut_2a']
        Cut_2b = Parameters.ix[i,'Cut_2b']
        Cut_3  = Parameters.ix[i,'Cut_3' ]
        
        Params = [Mu1,Sigma1,A1,Mu2,Sigma2,A2,Mu3,Sigma3,A3 ]
        plt.figure()
        Bins = Create_Bins(Image_type, N_bins) 
        x  = Bins[1:]
        y1 = Histogram_without_PVE
        y2 = trimodal(Bins[1:], *Params)
        plt.plot        (x, y1, color="k",lw=1, label = "Histogram without PVE-voxels", linestyle = '--')
        plt.plot        (x, y2, color='k',lw=2, label='Model_Optimized')
        plt.fill_between(x, y1, where=  x < Cut_1                 , color= (145/256,   0/256, 210/256), alpha = 1  , label = "Threshold_Air")
        plt.fill_between(x, y1, where= (x > Cut_2a) & (x < Cut_2b), color= (240/255,  79/256,   0/256), alpha = 1, label = "Threshold_OrM")
        plt.fill_between(x, y1, where=  x > Cut_3                 , color= (256/256, 206/256,   0/256), alpha = 1  , label = "Threshold_Grv")
        plt.plot(Bins[1:]  ,gauss   (Bins[1:], *Params[0:3]),color='k',lw=1, )
        plt.plot(Bins[1:]  ,gauss   (Bins[1:], *Params[3:6]),color='k',lw=1, )
        plt.plot(Bins[1:]  ,gauss   (Bins[1:], *Params[6:9]),color='k',lw=1, )
        plt.xlabel('Gray value')
        plt.ylabel('Frequence')
        plt.legend(fontsize = "small")
        # Saving the figure
        chdir(path_lr)
        Title = 'Thresholds_of_{}_Sec_{}_PVE_{}_Tol_{}.png'.format(Scan_File.replace(Dir + '/' , "")[:-4],i+1,PVE_percentile,Tolerance)
        plt.savefig(Title, dpi = 300)

        # Saving messages
        global Messages
        Messages.append("")
        Messages.append("Thresholds for section " + str(i) + "are")
        Messages.append("Cut_1, Cut_2a, Cut_2b, Cut_3: "+str([int(Cut_1), int(Cut_2a), int(Cut_2b), int(Cut_3)]))
        if  (Cut_1 > 0) & (Cut_1 < Cut_2a) & (Cut_2a < Cut_2b) & (Cut_2b < Cut_3):
            Messages.append("Thresholds for section " + str(i) + "are OK")
        else:
            Messages.append("Thresholds for section " + str(i) + "are NOT OK")
            print ("Thresholds for section " + str(i+1) + " are NOT OK", Cut_1, Cut_2a, Cut_2b, Cut_3)
            raise ValueError 
    
    return (Parameters)
    
def Fitting_Three_Gaussians (Image_type, Histograms_without_PVE, N_bins, Mu, slices_VS = None):
    """ Makes a plot with the fitting of three gaussian bells (for a 3_phase multiphase segmentation
    
    Inputs:
    NumVS: Number of vertical sections See function Segmentation_Hashemi
    
    Returns: 
    Parameters: a panda frame with the value of Mu, Sigma and Height of each of the three Gaussian Bells
    each line of <parameters> corresponds to each of the Num_VS """
   
    # Bins 
    Bins = Create_Bins(Image_type, N_bins)

    def gauss(x,mu,sigma,a):
        # a = height of the peak
        return a*np.exp(-(x-mu)**2/2/sigma**2)
    def trimodal(x,mu1,sigma1,a1,mu2,sigma2,a2,mu3,sigma3,a3):
        """ Sum of three gaussian distributions"""
        return gauss(x,mu1,sigma1,a1)+gauss(x,mu2,sigma2,a2)+gauss(x,mu3,sigma3,a3)
    
    # Creating lists (L) that will sotre all optimized parameters
    Sections =  [1] if slices_VS == None else np.arange(1, len(slices_VS)+1)
    Mu1_L   , Mu2_L   , Mu3_L    = [], [] , []
    Sigma1_L, Sigma2_L, Sigma3_L = [], [] , []
    A1_L    , A2_L    , A3_L     = [], [] , []
        
    for VS in range (len(Sections)):

        # Mu initialisation
        Mu1_ini   , Mu2_ini   , Mu3_ini    = Mu_init(Mu, Image_type, VS, len(Sections))
        # Initialisation of sigma
        Sigma1_ini, Sigma2_ini, Sigma3_ini = Sigma_init(Image_type)
        # Initialisation of the height of the peaks; 
        A1_A2_A3_ini = Peak_Height_init (Histograms_without_PVE, Image_type, Mu1_ini, Mu2_ini, Mu3_ini, N_bins )
        # Separating heights in different names
        A1_ini, A2_ini, A3_ini = A1_A2_A3_ini [VS,0], A1_A2_A3_ini [VS,1], A1_A2_A3_ini [VS,2]

        # All initial guess in one name
        Params_ini    = (Mu1_ini,Sigma1_ini,A1_ini,Mu2_ini,Sigma2_ini,A2_ini,Mu3_ini,Sigma3_ini,A3_ini)

        # Ploting
        # New Figure
        # First Plot of the raw histogram
        plt.figure()
        Histogram_without_PVE = Histograms_without_PVE[VS] # index "i" is the vertical subsection
        plt.plot        (Bins[1:],Histogram_without_PVE  , color="k"   , lw=1, )
        plt.fill_between(Bins[1:],Histogram_without_PVE  , color='gray', alpha = 0.2 ,            label='Histogram without PVE voxels' )
        # In new figure - Plot of the trimodal model with initial guess
        plt.plot(Bins[1:],trimodal(Bins[1:],*Params_ini) , color='k'   ,lw=0.5, linestyle = '--', label='Model_initial_guess')
        Title = 'Gaussian_fitting_of_{}_Sec_{}_PVE_{}.png'.format(Scan_File.replace(Dir + '/' , "")[:-4],VS+1,PVE_percentile)
        plt.xlabel('Gray value')
        plt.ylabel('Frequence')
        plt.legend(fontsize = "small")
        # Saving the figure
        chdir(path_lr)
        plt.savefig(Title, dpi = 300)
        
        # Computation of the best parameters of the gaussian function
        global Params
        Params,Cov    = curve_fit(trimodal,Bins[1:], Histogram_without_PVE, Params_ini)
        Params[1::3]  = np.abs(Params[1::3]) # absolute value of sigma
        Mu1,Sigma1,A1 = Params[0:3]
        Mu2,Sigma2,A2 = Params[3:6]
        Mu3,Sigma3,A3 = Params[6:9]
        Mu1_L.append (Mu1)
        Mu2_L.append (Mu2)
        Mu3_L.append (Mu3)
        Sigma1_L.append(Sigma1)
        Sigma2_L.append(Sigma2)
        Sigma3_L.append(Sigma3)
        A1_L.append(A1)
        A2_L.append(A2)
        A3_L.append(A3)
        
        # In new figure - Plot of the optimized trimodal model 
        plt.plot(Bins[1:],trimodal(Bins[1:],*Params     ), color='k'   ,lw=2  , linestyle = '-' , label='Model_Optimized')
        plt.plot(Bins[1:],gauss   (Bins[1:],*Params[0:3]), color= (145/256,   0/256, 210/256)   ,lw=0.75, )
        plt.plot(Bins[1:],gauss   (Bins[1:],*Params[3:6]), color= (240/255,  79/256,   0/256)   ,lw=0.75, )
        plt.plot(Bins[1:],gauss   (Bins[1:],*Params[6:9]), color= (256/256, 206/256,   0/256)   ,lw=0.75, )
        
        Title = 'Gaussian_fitting_of_{}_Sec_{}_PVE_{}.png'.format(Scan_File.replace(Dir + '/' , "")[:-4],VS+1,PVE_percentile)
        plt.xlabel('Gray value')
        plt.ylabel('Frequence')
        plt.legend(fontsize = "small")
        # Saving the figure
        chdir(path_lr)
        plt.savefig(Title, dpi = 300)
        # Saving messages
        global Messages
        Messages.append("")
        Messages.append("Parameters for subsection : " + str (VS+1) + " are")
        Messages.append("Mu1,Sigma1,A1: " +str([int(Mu1),int(Sigma1),int(A1)]))
        Messages.append("Mu2,Sigma2,A2: " +str([int(Mu2),int(Sigma2),int(A2)]))
        Messages.append("Mu3,Sigma3,A3: " +str([int(Mu3),int(Sigma3),int(A3)]))
        
        # Checking if the means are well sorted    
        if (Mu1<Mu2) & (Mu2<Mu3) & (Mu1>0) & (Mu2>0) & (Mu3>0) :    
            Messages.append ("Mu OK : Mu1 <  Mu2 < Mu3 ")
        else:
            Messages.append ("Mu for section {} are NOT OK".format(VS+1))
            print ("Mu for section {} are NOT OK".format(VS+1), Mu1, Mu2, Mu3)           
            raise ValueError
            
        # Checking sigmas are not very high 
        if Image_type == np.uint8:
            limit = 2**8/3
        if Image_type == np.uint16:        
            limit = 2**16/3
        if  (abs(Sigma1)<limit) & (abs(Sigma2)<limit) & (abs(Sigma3)<limit) :    
            Messages.append ("Sigmas are OK: Sigma1, Sigma2, Sigma3 ")
        else:
            Messages.append ("Sigmas are VERY HIGH, Sigma1, Sigma2, Sigma3 ")
        
        # Checking peak heights all positive 
        if (A1>0) & (A2>0) & (A3>0) :    
            Messages.append ("Peak Heights are OK: A1, A2, A3 > 0")
        else:
            Messages.append ("Peak Heights are NOT OK: A1, A2, A3 < 0")
            print ("Peak Heights are NOT OK", A1, A2, A3) 
            raise ValueError
    
    # Creating output Parameters 
    Parameters = pd.DataFrame({'Section': Sections})
    Parameters['Mu1'] = Mu1_L
    Parameters['Mu2'] = Mu2_L
    Parameters['Mu3'] = Mu3_L
    Parameters['Sigma1'] = Sigma1_L
    Parameters['Sigma2'] = Sigma2_L
    Parameters['Sigma3'] = Sigma3_L
    Parameters['A1'] = A1_L
    Parameters['A2'] = A2_L
    Parameters['A3'] = A3_L
    chdir(path_lr)
    Parameters.to_csv('Parameters_Gaussians_of_{}_PVE_{}_VS_{}.csv'.format(scan_file.replace(".tif",""),PVE_percentile, len(Sections)), sep = ';')
    return (Parameters)
        
def Create_Bins(Image_type, N_bins):
    if Image_type == np.uint8:
        Bins = np.linspace(0,2**8 ,N_bins) # 256   Gray values for 8 -bit image
    if Image_type == np.uint16:        
        Bins = np.linspace(0,2**16,N_bins) # 65536 Gray values for 16-bit image
    return (Bins)
    
def Initial_Histograms(Image,PVE, Image_type, N_bins, center = None, radius = None, slices_VS = None):
    """ Plots a figure with the histogram of the Image "raw" and without the PVE voxels
        for each section dependin on the number Num_VS
        Num_VS = Number of vertial sections """

    Bins = Create_Bins(Image_type, N_bins) 
    if slices_VS == None :
        Limits = [0,len(Image)]
    else:
        Limits = slices_VS.copy()
        Limits.append(len(Image))
    
    Histograms              = [] # A list that will store an histogram for each vertival section
    Histograms_without_PVE  = [] # Similar but with histograms without PVE voxels

    for i in range(len(Limits)-1):

        Image_Section = Image[ Limits[i]:Limits[i+1] ]
       
        # CREATING CIRCULAR MASK       
        
        # Center
        # Images 1 and 2 (3D) should have the same dimensions x*y (h may be different)
        z,y,x = np.shape(Image_Section)[0], np.shape(Image_Section)[1], np.shape(Image_Section)[2]
        if center == None:
            center = int(y/2), int(x/2)
        if radius == None:
            radius = int(x/2)
        
         
        #Selecting circular ROI (region of interest)
        mask_2D = sector_mask((y,x),center,radius,(0,360 ))       
        mask_3D = np.zeros(Image_Section.shape, dtype = np.bool)
        for j in range(z):
            mask_3D[j] = mask_2D
        
        
        Histogram     = np.histogram(Image_Section[mask_3D], bins=Bins)
        Histogram     = np.array(Histogram[0])
        Histogram[ 0] = 1  # Some make up to hide extreme peaks due to saturated pixels (very important not to delete this lines if the region out of the cylinder is black (value = 0))
        Histogram[-1] = 1  # Some make up to hide extreme peaks due to saturated pixels
        Histograms.append(Histogram)
        
        PVE_Section       = PVE[ Limits[i]:Limits[i+1] ]
        Not_PVE_section   = (PVE_Section==0) # Borders = PVE = 255 ; Not Borders = Not_PVE = 0         
        Image_without_PVE = Image_Section[np.logical_and(Not_PVE_section, mask_3D)]                                                  
        Histogram_without_PVE = np.histogram(Image_without_PVE, bins=Bins)
        Histogram_without_PVE = np.array    (Histogram_without_PVE[0]) # index 0 is the frequency vector, index 1 is the bins vector
        Histogram_without_PVE[ 0] = 1  # Some make up to hide extreme peaks due to saturated pixels
        Histogram_without_PVE[-1] = 1  # Some make up to hide extreme peaks due to saturated pixels

        Histograms_without_PVE.append(Histogram_without_PVE)

        plt.figure()
        plt.plot        (Bins[1:  ],Histogram,                  color="b",      lw=1  , label = "Original Histogram")
        plt.plot        (Bins[1:  ],Histogram_without_PVE,      color="k",      lw=1  , label = "Histogram without PVE-voxels")
        plt.fill_between(Bins[1:  ],Histogram_without_PVE, color = 'gray' , alpha = 0.2  )
        plt.xlabel('Gray value')
        plt.ylabel('Frequence')
        plt.legend(fontsize = "small")
        
        chdir(path_lr)
        Title = "Histograms_of_{}_Sec_{}_PVE_{}.png".format(Scan_File.replace(Dir + '/' , "")[:-4],i+1,PVE_percentile)
        plt.savefig(Title, dpi = 300)
    
    del mask_3D    
    return (Histograms, Histograms_without_PVE)
    
def Mu_init(Mu, Image_type, VS, Num_VS):

    """ Initialization of mean values for gaussian fitting inputs:
    Mu & Num_VS: See doc of function Segmentation_Hashemi
    Image_Type: either np.uint8 (8-bit) or np.uint16(16-bit). For mor information see Scan_File in the doc of Segmentation_Hashemi
    """

    if Mu == None : # then initialize by experience
        if   Image_type == np.uint8:
            Mu1_ini = 40  # Air
            Mu2_ini = 80  # OrM Organic Matter
            Mu3_ini = 160 # Gravel
        elif Image_type == np.uint16:
            Mu1_ini = 7000  # Air
            Mu2_ini = 21000 # OrM Organic Matter
            Mu3_ini = 45000 # Gravel
    else: # Takes user's initialization
        assert (type (Mu) == list) , 'Input Mu must be a list'
        assert (len  (Mu) == Num_VS   ) , 'Input Mu must be a list of integers with shape Num_VS, 3'
        Mu1_ini = Mu[VS][0]  # Air
        Mu2_ini = Mu[VS][1]  # OrM Organic Matter
        Mu3_ini = Mu[VS][2]  # Gravel
    return (Mu1_ini, Mu2_ini, Mu3_ini)

def Sigma_init(Image_type):
    # Initialisation of sigma
    if Image_type == np.uint8:
        Sigma1_ini = 2**8/50 # very small vallue
        Sigma2_ini = 2**8/50
        Sigma3_ini = 2**8/50
    if Image_type == np.uint16:        
        Sigma1_ini = 2**16/50 # very small vallue
        Sigma2_ini = 2**16/50
        Sigma3_ini = 2**16/50
    return (Sigma1_ini, Sigma2_ini, Sigma3_ini)

def Peak_Height_init (Histograms_without_PVE, Image_type, Mu1_ini, Mu2_ini, Mu3_ini, N_bins):
    """  Initialisation of the height "A" of the peaks, 
         looks for the maximum in the originial histogram based on intervales 
         generated using Mu initialization """
    
    Bins   = Create_Bins(Image_type, N_bins)
    index1 = np.searchsorted(Bins, (Mu1_ini + Mu2_ini)/2 )
    index2 = np.searchsorted(Bins, (Mu2_ini + Mu3_ini)/2 )
    Array_init_A1_A2_A3 = []
    for i in range(len(Histograms_without_PVE)):
        Histogram_without_PVE = Histograms_without_PVE [i]
        A1_ini = np.max(Histogram_without_PVE[0     :index1])
        A2_ini = np.max(Histogram_without_PVE[index1:index2])
        A3_ini = np.max(Histogram_without_PVE[index2:      ])
        Array_init_A1_A2_A3.append([A1_ini, A2_ini, A3_ini])
    Array_init_A1_A2_A3 = np.array(Array_init_A1_A2_A3)
    return (Array_init_A1_A2_A3) 
  
def Create_Seed (Image, Not_PVE, Parameters, slices_VS ):
    """ Takes Image PVE voxels and Initials thresholds to create
        a the seed    """
    
    if slices_VS == None :
        L = [0,len(Image)]
    else:
        L = slices_VS.copy()
        L.append(len(Image))
        
    Seed_Image = np.zeros(Image.shape, dtype = np.uint8)
    
    num_sec = 1 if slices_VS == None else len(slices_VS)
    for i in range(num_sec):
        Image_Section   = Image  [L[i]:L[i+1]]
        Not_PVE_Section = Not_PVE[L[i]:L[i+1]]
        Cut_1  = Parameters.ix[i,'Cut_1' ]
        Cut_2a = Parameters.ix[i,'Cut_2a']
        Cut_2b = Parameters.ix[i,'Cut_2b']
        Cut_3  = Parameters.ix[i,'Cut_3' ]
        ThresholdAir =                np.array(Image_Section<Cut_1 ,dtype=bool)  
        ThresholdOrM =                np.array(Image_Section>Cut_2a,dtype=bool)
        ThresholdOrM = ThresholdOrM & np.array(Image_Section<Cut_2b,dtype=bool)  
        ThresholdGrv =                np.array(Image_Section>Cut_3 ,dtype=bool)  
        ThresholdAir = ThresholdAir & Not_PVE_Section                                                    
        ThresholdOrM = ThresholdOrM & Not_PVE_Section                                                    
        ThresholdGrv = ThresholdGrv & Not_PVE_Section                                                    
        # One gray level for each phase (0, 64,128,192: 0 means voxel is uncertain)
        Seed_Image[L[i]:L[i+1]] = Seed_Image[L[i]:L[i+1]] + np.array(ThresholdAir,dtype=np.uint8)*64  
        Seed_Image[L[i]:L[i+1]] = Seed_Image[L[i]:L[i+1]] + np.array(ThresholdOrM,dtype=np.uint8)*128
        Seed_Image[L[i]:L[i+1]] = Seed_Image[L[i]:L[i+1]] + np.array(ThresholdGrv,dtype=np.uint8)*192
        del ThresholdAir, ThresholdOrM, ThresholdGrv                                   

    return(Seed_Image)
    
def Segmentation_Hashemi (Dir, Scan_File, Edges_File, PVE_percentiles = [60], Mu = None, slices_VS = None, Tolerances = [0.50], Save_Step = 1000, Mode_Threshold = 1, flag = False, center = None, radius = None):
    """ This function creates a 3-phase-segmentation from an image issue of 3D tomography
    Those 3 phases are often: Air, Organic Matter (OM) and Gravel
    Each phase is modeled as a gaussian distribution with height, mean and standard deviation
    
    The algorithm is based on: A tomographic imagery segmentation methodology for three-phase geomaterials based on simultaneous region growing
    October 2013Acta Geotechnica 9(5) Hashemi et al
    
    The funcion takes inputs
    Dir       : The directory where the files Scan_File and PVE_File are read and where results are written
    Scan_File : .tif  3D-image in 8-bit or 16-bit. It is a previously filtered 3D Image (Use 3D median filter in ImageJ)
    Edges_File  : .tif  3D-image in 8-bit (same size as Scan_File). It is a 32-bit 3D image,                 
                To obtain the a Edges Image: In ImageJ use the command: Process-Find Edges (Warning, this is a 2D operation)
    PVE_Percentile: This value will define the threshold of the PVE voxels. The PVE image is the set of voxels whose value is lower than this percentil in the Edges Image            
    Mu        : a list with three integer values that will be the initialisation of means 
                when fitting the gaussians of the gray value histogram of ScanFile, if Mu == None, they
                will be initialised by some values depending on our experience"
                Mu must have a size 3,Num_VS, ie a value for 3-phase segmentation for each Vertical Section
    Num_VS    : int, Number of vertical sections, it indicates in how many parts the image should be splitted to execute the Gaussian Fitting and the generation
                of the seed image (before region growing) to take into account the vertical variations in gray contrast
                if Num_VS = 1 (default) then the Gaussian Fitting and seed creation will be based on the whole stack histogram of gray values of Scan_File
                PLEASE be aware that the region growing phase must be executed on the whole image because it is a neighborhood based image treatment. 
                Therefore, there is NO spliting of the region growing using Num_VS.
    Mode_Threshold : see Compute_Thresholds function documentation
    Flag      : bool, default falue = True, if False, the computations are stopped just before region growing, (useul if you want to check if seed creation was well done)
    
    Outputs
    5 figures with histograms to understand how the seed-image was built and a track of the growing phase
    -A .csv file with all the fitted parameters of the gaussian fitting (mu, std, peak height vs vertical section)
    -The seed Image in .tif format (following Hashemi's method)
    -An image for every <Save_Step>-th growing step in .tif format
    -An image for interface pixels attribution in .tif frmat
    -THE MOST IMPORTANT: The final segmentation (WHOSE labels are 0 for air, 128 for OM and 255 for Gravel) in .tif format
    -Some messages about the performance of the segmentation in a .csv file
    -A csv. file with information of the physical memory usage (RAM usage)
    """
    
    #########################################################################################
    # CHECKING FILES
    ########################################################################################    
    # Reading Directory
    chdir(Dir)
    # Verifying if all files exist
    assert (path.isdir(Dir))        , "Folder: "    + Dir       + " IS NOT A FOLDER!"
    assert (path.exists(Scan_File)) , "Scan_File: " + Scan_File + " DOES NOT EXIST!"
    assert (path.exists(Edges_File)), "Edges_File: "  + Edges_File  + " DOES NOT EXIST!"
    assert Scan_File[-4:]   == ".tif" , "Scan_File: " + Scan_File + " MUST BE A TIFF IMAGE (.tif)!"
    assert Edges_File [-4:] == ".tif" , "PVE_File: "  + Edges_File  + " MUST BE A TIFF IMAGE (.tif)!"
    
    #########################################################################################
    # CREATING SAVING PATHS
    #########################################################################################    
    global path_r # resluts
    path_r = Dir+"/Results"
    if not(path.exists(path_r)):
        mkdir(path_r)

    global path_hr # heavy results
    path_hr = Dir+"/Results/heavy_results"
    if not(path.exists(path_hr)):
        mkdir(path_hr)
    
    global path_lr # light results
    path_lr = Dir+"/Results/light_results"
    if not(path.exists(path_lr)):
        mkdir(path_lr)
    
    global stats_RAM
    # Creating DataFrame that will contain RAM usage stats
    stats_RAM = pd.DataFrame(columns = ['stage','total_GB', 'used_GB', 'percent'])
    # Getting Ram usage stats        
    stats_RAM = get_RAM_usage('Beggining', path = path_lr, frame_stats = stats_RAM)

    #########################################################################################
    # A list of messages will be recorded to check if the execution of the function was ok
    ########################################################################################
    global Messages    
    Messages = list()
    Message  = "Sample to be segmented: "+ Scan_File
    print("Sample to be segmented: "+ Scan_File)
    Messages.append(Message)
   
    #########################################################################################
    # *IMPORTING FILES 
    ########################################################################################    
    print ("STAGE: Importing files")
    # Importing files and checking some computational requirements
    global Image, PVE, Image_Size, PVE_Size  # Image is the scan to be segmented
    chdir(Dir)    
    Image      = tifffile.imread(Scan_File)
    stats_RAM  = get_RAM_usage(stage = 'After reading Scan_File', path = path_lr, frame_stats =stats_RAM)    
    
    chdir(Dir)
    Edges        = tifffile.imread(Edges_File)   # Partial Volume Effect Voxels
    stats_RAM = get_RAM_usage(stage = 'After reading Edges_File', path = path_lr, frame_stats =stats_RAM)    
  
    # Checking if the Image type
    Image_type = Image.dtype
    check      = (Image_type == np.uint8) or (Image_type == np.uint16)    
    assert (check) , 'Scan_File MUST be 8-bit oR 16-bit !'

    # Checking image is not too big
    #Image_Size = getsizeof    (Image)/10**6 # Result in MB
    #Edges_Size = getsizeof    (Edges)/10**6   # Result in MB
    global PVE_percentile
    for PVE_percentile in PVE_percentiles:
        plt.close("all")
        #########################################################################################
        # *DEFINING PVE VOXELS FROM EDGES IMAGE 
        ########################################################################################
        
        #Selecting circular ROI (region of interest)
        # Center
        # Images 1 and 2 (3D) should have the same dimensions x*y (h may be different)
        z,y,x =np.shape(Edges)[0], np.shape(Edges)[1],np.shape(Edges)[2]
        
        if center == None:
                center = int(y/2), int(x/2)
        if radius == None:
                radius = int(x/2)   
        mask_2D = sector_mask((y,x),center,radius,(0,360))    
        mask_3D = np.zeros(Edges.shape, dtype = np.bool)
        for i in range(z):
            mask_3D[i] = mask_2D
            
        Cut_PVE = np.percentile(Edges[mask_3D], PVE_percentile)
        PVE     = (Edges > Cut_PVE )

        
        #########################################################################################
        # *CHANGING TO SAVING DIRECTORY
        ########################################################################################
        chdir(Dir+"/Results")
        
        #########################################################################################
        # *INITIAl HISTOGRAMS 
        ########################################################################################
        print ("STAGE: Ploting Initial Histograms")
        print ("PVE = ", PVE_percentile)    
        N_bins = 256
        Histograms, Histograms_without_PVE = Initial_Histograms(Image, PVE, Image_type, N_bins, center = center, radius = radius, slices_VS =slices_VS )  
     
        ########################################################################################
        # *FITTING GAUSSIANS - 
        # Based on Histograms without PVE
        ########################################################################################
        print ("STAGE: Fitting Gaussians")
        global Parameters
        Parameters = Fitting_Three_Gaussians (Image_type, Histograms_without_PVE, N_bins, Mu = Mu, slices_VS = slices_VS )
        
        MessagesDF = pd.DataFrame({"Message":Messages})
        Title = 'Messages_' + Scan_File.replace(Dir + '/' , "")[:-4] + '.csv' 
        chdir(path_lr)    
        MessagesDF.to_csv(Title, encoding = "utf-8")
        
        stats_RAM = get_RAM_usage(stage = 'After Seed Creation', path = path_lr, frame_stats =stats_RAM)
        
    
        #######################################################################################
        # *FINDING INITIAL THRESHOLDING - normal version
        ########################################################################################
        print ("STAGE: Finding initial thresholding")
        for Tolerance in Tolerances:
            plt.close('all')
            print ("Tolerance = ", Tolerance) 
            Parameters = Compute_Thresholds(Tolerance, Parameters, Image_type, Histograms_without_PVE, N_bins, mode = Mode_Threshold, slices_VS = slices_VS)

            MessagesDF = pd.DataFrame({"Message":Messages})
            Title = 'Messages_' + Scan_File.replace(Dir + '/' , "")[:-4] + '.csv' 
            chdir(path_lr)    
            MessagesDF.to_csv(Title, encoding = "utf-8")
            stats_RAM = get_RAM_usage(stage = 'After Seed Creation', path = path_lr, frame_stats =stats_RAM)
    
    del Edges, mask_3D
    ########################################################################################
    # *CREATE SEED
    ########################################################################################
    # create the seed only if you are working with a single value of percentil
    num_sec = 1 if slices_VS == None else len(slices_VS)    
    if (len(PVE_percentiles) == 1) and len(Tolerances) == 1:
        print ("STAGE: Creating seed with initial thresholding")
        Not_PVE  = (PVE==0)
        Seed_Image = Create_Seed (Image, Not_PVE, Parameters, slices_VS )
        Title = "Seed_{}_NumVS_{}_PVE_{}_Tol_{}.tif".format(scan_file.replace(".tif",""),num_sec,PVE_percentile,Tolerance)
        chdir(path_hr)
        tifffile.imsave(Title, Seed_Image)
        del Not_PVE
    
        MessagesDF = pd.DataFrame({"Message":Messages})
        Title = 'Messages_' + Scan_File.replace(Dir + '/' , "")[:-4] + '.csv' 
        chdir(path_lr)    
        MessagesDF.to_csv(Title, encoding = "utf-8")
        stats_RAM = get_RAM_usage(stage = 'After Seed Creation', path = path_lr, frame_stats =stats_RAM)
    
    ########################################################################################
    # *REGION GROWING (for notation of the variables see Hashemi's article)
    ########################################################################################
    if (len(PVE_percentiles) > 1) or (len(Tolerances) > 1):
        flag = False # Turning the flag to compute region growing off automatically 
    
    if flag == True:
        B_Air,B_OrM,B_Grv = region_growing(Seed_Image, slices_VS, Save_Step)
    
    ########################################################################################
    # INTERFACE FILLING 
    ########################################################################################
    if flag == True:
        Image_Final = interface_filling(B_Air,B_OrM,B_Grv, slices_VS)
        # Saving
        Name = "Hash_Segm_of_{}_PVE_{}_Tol_{}_NumVS_{}.tif".format( Scan_File.replace(Dir + '/' , "")[:-4], PVE_percentile, Tolerance, num_sec)
        chdir(path_hr)
        tifffile.imsave(Name, Image_Final)
        del Image_Final
        del Image                                                                       
    
 
    ########################################################################################
    # 10) Saving Messages & Close graphs
    ######################################################################################
    plt.close("all")
    chdir(path_lr)
    MessagesDF = pd.DataFrame({"Message":Messages})
    Title = 'Messages_' + Scan_File.replace(Dir + '/' , "")[:-4] + '.csv' 
    MessagesDF.to_csv(Title, encoding = "utf-8")    
    print ("FINISHED")

def decorateur_timeit(a_function):
    def wrapper (*args, **kwargs):
        start = start = datetime.datetime.now()
        res = a_function(*args, **kwargs)
        end = datetime.datetime.now()
        chdir(Dir)
        file = open(Dir + "/Results/light_results/time_report_{}.txt".format(scan_file.replace(".tif","")), 'w')
        file.write(" Start:\n" )
        file.write(start.strftime('%c'))
        file.write("\n End: \n" )
        file.write(end.strftime('%c'))
        file.write("\n Duration: \n")
        file.write(str(end-start))
        file.close()
    return wrapper

def main():
    global start , Dir, Scan_File, scan_file, PVE_percentile, Tolerance
    Dir       = "/home/german.martinez-carvajal/Desktop/These/Hashemi_segmentation/Tests/Test11_Different_sections"
    ## The two next files are located in the same directory (Dir)
    scan_file = "Image_Filt_heterogeneous.tif"
    edges_file =  "Edges_img_heterogeneous.tif"   
    Scan_File   = Dir + "/" + scan_file # This is the scan previously filtrated by a median filter
    Edges_File  = Dir + "/" + edges_file      # This is the file Partial Volume Effect file
    slices_VS = [0,3,6]
    Mu = [[17001/3.5,31001/3.5,55001/3.5], [17001*2/3.5,25000,31000], [17001,31001,55001]]
    Save_Step = 1000 # int very high if you dont want to save any growing step
    Tolerances = [0.25] # must be a list
    PVE_percentiles = [50] # must be a list # the lower the PVE percentile, the more uncertain pixels will be taken out of the seed
    flag = True # False = stop right before region growing, True = continue...
    center = None # 300,300 # in pixels
    radius = None # 30 # in pixels
    # computing    
    f = decorateur_timeit(Segmentation_Hashemi)
    f(Dir, Scan_File, Edges_File, PVE_percentiles, Mu, slices_VS, Tolerances, Save_Step, Mode_Threshold = 1, flag = flag, center = center, radius = radius)
#main()

def main2():
    global start , Dir, Scan_File, scan_file, PVE_percentile, Tolerance
    Dir       = "/home/german.martinez-carvajal/Bureau/These/Hashemi_segmentation/Tests/Test9_Rapide"
    ## The two next files are located in the same directory (Dir)
    scan_file = "Image_Filtered.tif"
    edges_file =  "Image_Edges.tif"   
    Scan_File   = Dir + "/" + scan_file # This is the scan previously filtrated by a median filter
    Edges_File  = Dir + "/" + edges_file      # This is the file Partial Volume Effect file
    slices_VS = None
    Mu = [[17001,31001,55001]]
    Save_Step = 1000 # int very high if you dont want to save any growing step
    Tolerances = [0.75] # must be a list
    PVE_percentiles = [50] # must be a list # the lower the PVE percentile, the more uncertain pixels will be taken out of the seed
    flag = True # False = stop right before region growing, True = continue...
    center = None # 300,300 # in pixels
    radius = None # 30 # in pixels
    # computing    
    f = decorateur_timeit(Segmentation_Hashemi)
    f(Dir, Scan_File, Edges_File, PVE_percentiles, Mu, slices_VS, Tolerances, Save_Step, Mode_Threshold = 1, flag = flag, center = center, radius = radius)
main2()
