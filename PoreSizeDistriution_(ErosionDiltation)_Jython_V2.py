def Pore_Size_Cum_Distribution(Dir, FileName, Radii_File):
	""" Output: Saves a csv file with the volume cummulative distribution of pore sizes for an image """
	""" Inputs: Dir: the directory/folder of the image, and where the csv will be saved """
	""" FileName : Name of the file """
	""" File must be a segmented image, i.e. binary, values are only 0 or 255 in 8-bit"""
	""" The pore phase has to be labeled 255 """

	from os import path, chdir
	assert (path.isdir(Dir)) , "Folder IS NOT A FOLDER: " + folder
	assert (path.exists(FileName)) , "FILE DOES NOT EXIST: " + FileName
	
	# Reading Segmented Image
	# Imp the Image plus object for the air-phase (this is the result of the Hashemi's segmentation)
	from ij import IJ
	Imp_Air  = IJ.openImage(FileName)
	Imp_Air.show()

	# Defining a function to comput the number of air pixels of an image
	def get_Num_Air_Pixels (Imp):
		# Computing the number of Air Pixels
		from ij.process import StackStatistics
		SS         = StackStatistics(Imp) 
		Histogram  = SS.getHistogram() # Histogram is an array of longs
		Histogram  = map(int, Histogram) # So here I change loongs to int numbers
		Num_Air_Pixels = Histogram[-1] # -1 is the 256 bin. 255 = white = Air
		return Num_Air_Pixels

	# Total initial number of air pixels
	Initial = get_Num_Air_Pixels (Imp_Air)
	print('Initial number of Air voxels                      = ', Initial)

	# Importing the a list of radii for erosion_dilation
	Radii = open(Radii_File, mode = 'r')
	R = []
	for r in Radii:
		R.append(int(r))
	print('Radii : ', R)
	Radii = R
	
	# Max_dimension will control the number of iterations of the main loop    
	Max_dimension  = max(Imp_Air.width, Imp_Air.height, Imp_Air.getNSlices()) 
	print('Max_dimension : ', Max_dimension)	

	# Initializing
	Cum_Volumes = [0] # list of volumes of pores belonging to a radii class (will grow in the loop)

	# Creating an Image plus object to store the erosions
	from ij import ImagePlus
	Erosion = ImagePlus('Erosion', Imp_Air.getImageStack()) # at this step the NO erosion is performed yet
	Erosion.show()

	# Main loop that performs erosion and dilations
	for i in range(1,Max_dimension): 
	    if i < len(Radii):
	    	Radius = Radii[i]
	    	print('Step: Radius of erosion (in pixels) = ', Radius)
	    	# Computing erosions
	    	Length_of_erosion_loop = Radii[i]-Radii[i-1]
	    	print('Lenght of erosion loop ', Length_of_erosion_loop)
	    	for repeat in range(Length_of_erosion_loop):
		    	IJ.run(Erosion, "Erode (3D)", "iso=255") # IJ.run("Erode 3D") is a change in place method (there is no need to reasign the result of the Erosion)
	    	# Computing dilations
	    	Dilation = ImagePlus('Dilation', Erosion.getImageStack())
	    	print('Lenght of dilation loop ', Radius)
	    	for repeat in range(Radius):
		        IJ.run(Dilation, "Dilate (3D)", "iso=255")
	        Remaining = get_Num_Air_Pixels (Dilation)
	        print('Remaining air voxels (after erosion-dilation) = ', Remaining)
	    else :
	    	Real_max_radius = Radii[-1]
	    	while Remaining != 0:
	    		IJ.run(Erosion, "Erode (3D)", "iso=255") # IJ.run("Erode 3D") is a change in place method (there is no need to reasign the result of the Erosion)
	    		Remaining = get_Num_Air_Pixels (Erosion)
	    		Real_max_radius += 1
	    	print('Real max radius (for the erosion, dilation method): ' , Real_max_radius)
	    	
	    		 
	    # Computing cummultive Pore size distribution
	    # Volume of pores smaller than radius (in vox)
	    Volume = Initial - Remaining
	    Cum_Volumes.append(Volume)
	    # Breaking if procedure completed
	    Dilation.close()
	    if Remaining == 0:
	    	print('Erosion-Dilation loop broke correctly')
	        break
	# End of the main loop

	#Closing Image Plus objects
	Erosion.close()
	Imp_Air.close()
	
	# Appending the real max  radius
	Radii.append(Real_max_radius)
	print('Radii: ', Radii)
	print('Cum_Volumes: ', Cum_Volumes)
	# Saving
	chdir(Dir)
	File = open('Cumul_distribution_Jython.csv', mode = 'w')
	File.write('Radius (vox)' + ';' + 'Cumul Volume (vox)' + '\n')
	for i in range(len(Radii)):
		File.write(str(Radii[i]) + ';' + str(Cum_Volumes[i]) + '\n')
	File.close()
	print("Finished !")    

# MAIN

# Report start-time of the execution
import datetime
Start = datetime.datetime.today()

# Directory and File Name (for a test sample)
Dir  = '/home/german.martinez-carvajal/Desktop/2018/Pore_Size_Distribution/Test'
FileName = Dir + "/" + "Air.tif"
Radii_File = Dir + "/" + "Radii_list_in_voxels.csv"

# Directory and file Name (for a real sample)
#Dir  = '/home/german.martinez-carvajal/Desktop/2018/Pore_Size_Distribution/COP01/Low_Resolution_70MIC'
#FileName = Dir + "/" + "Air.tif"

# Calling the function
Pore_Size_Cum_Distribution(Dir, FileName, Radii_File)

# Report end-time of the execution
End = datetime.datetime.today()

# Saving report of time consumed
from os import chdir
chdir (Dir)
File = open('Time_Report_Jython.txt', mode = 'w')
File.write('Start time: ' + str(Start) + '\n')
File.write('End   time: ' + str(End  ) + '\n')
File.close()

# FINISHED