# Fiji scripting pythonic-way part 2

# Creating and manipulate stacks
from ij import IJ, ImagePlus, ImageStack
imp   = IJ.openImage("http://imagej.nih.gov/ij/images/flybrain.zip")  
stack = imp.getImageStack()
imp.show()
print "number of slices:", imp.getNSlices()
# A list of green slices
greens = []
# Iterate each slice in the stack
for i in xrange(1, imp.getNSlices()+1):
	# get the ColorProcessor Slice at index i
	cp = stack.getProcessor(i)
	# get its green channel as a FloatProcessor
	fp = cp.toFloat(1, None)
	# ... and store it in a list
	greens.append(fp)

# Create a new stack with only the green channel
stack2 = ImageStack(imp.width, imp.height)
for fp in greens:
	stack2.addSlice(None,fp)

# Create a new  image with the stack of green channel slices
imp2 = ImagePlus("Green channel", stack2)
# Set a green look-up table:
IJ.run(imp2, "Green", "")
imp2.show()
imp.close()
imp2.close()

from ij import CompositeImage
imp   = IJ.openImage("http://imagej.nih.gov/ij/images/flybrain.zip")
stack = imp.getImageStack()
# New stack
stack2 = ImageStack(imp.width, imp.height)
# Convert each color slice in the stack to two 32- bit FloatProcessor slices
for i in xrange(1, imp.getNSlices()+1):
	# Get the ColorProcessor slice at index i
	cp = stack.getProcessor(i)
	# Extract the red and green channels as FloatProcessor
	red   = cp.toFloat(0, None)
	green = cp.toFloat(1, None)
	stack2.addSlice(None, red)
	stack2.addSlice(None, green) # Until now, the stack has twice the number of slices of the original stack
	# Add both to the new stack
# Create a new ImagePlus with the new stack
imp2 = ImagePlus('Composite', stack2)
imp2.setCalibration(imp.getCalibration().copy()) # Calibration = microns/pixel
# Tell the ImagePlus to represent the slices in its stack
# in hyperstack form, and open it as a "CompositeImage"
nChannels = 2 
nSlices   = stack.getSize()
nFrames   = 1 # only 1-time point
imp2.setDimensions(nChannels, nSlices, nFrames)
comp      = CompositeImage(imp2, CompositeImage.COLOR)
comp.show() 
comp.close()
# Creating Images and regions of interest (ROIs)
# Creating an image from scracth
from ij.process import FloatProcessor
from array import zeros
from random import random
width = 1024
height = 1024
pixels = zeros ('f', width*height)
for i in xrange(len(pixels)):
	pixels[i] = random()
fp  = FloatProcessor(width, height, pixels, None)

print('ok')
# Fill a region of interest (ROI° with a given value
from ij.gui import Roi, PolygonRoi
roi = Roi(400,200,400,300)
fp.setRoi(roi)
fp.setValue(2.0)
fp.fill()
Imp = ImagePlus("White noise", fp)
Imp.show()
# Fill a polygonal region of interest  
# with a value of -3  
xs = [234, 174, 162, 102, 120, 123, 153, 177, 171,  
      60, 0, 18, 63, 132, 84, 129, 69, 174, 150,  
      183, 207, 198, 303, 231, 258, 234, 276, 327,  
      378, 312, 228, 225, 246, 282, 261, 252]  
ys = [48, 0, 60, 18, 78, 156, 201, 213, 270, 279,  
      336, 405, 345, 348, 483, 615, 654, 639, 495,  
      444, 480, 648, 651, 609, 456, 327, 330, 432,  
      408, 273, 273, 204, 189, 126, 57, 6]
proi = PolygonRoi(xs,ys, len(xs), Roi.POLYGON)
fp.setRoi(proi)
fp.setValue(-3)
fp.fill(proi.getMask())
Imp =ImagePlus("With a man", fp)
Imp.show()
