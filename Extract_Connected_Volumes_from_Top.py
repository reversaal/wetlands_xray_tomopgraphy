from os import chdir, getcwd
import pandas as pd
import numpy as np
import tifffile

def Extract_Volumes(Dir, Stats_File_Name, Obj_map_File_Name, Start_Depth = 1):
    """ Extracts the volumes of a segmented phase whose boundig box is between
    start depth (0 by defalut) and finishes at the last slice of the image"""
    
    # Directory 
    chdir(Dir)
    
    # Reading Fiji 3D objects counter statistics
    global Stats
    Stats     = pd.read_csv(Stats_File_Name)
    Columns   = list(Stats.columns)

    # Reading Image of labeled volumes
    print('Reading Image', Obj_map_File_Name)
    Air_Objects = tifffile.imread(Obj_map_File_Name)
    
    # Slices boudaries where the vertical connectivity will be evaluated
    # Start_depth 
    global Final_Depth
    Final_Depth = len(Air_Objects) #max(Stats.BZ)
    
    # Computing end of vertical box
    Stats["BZ-end"]  = Stats.BZ + Stats["B-depth"]
    
    # Extracting connected pathways  from Start to Final Depths
    global Connected
    Connected   = Stats[(Stats.BZ <= Start_Depth) & (Stats["BZ-end"] >= Final_Depth)]
    Name        = 'Connected_' + Stats_File_Name 
    Connected.to_csv(Name)

    
    # Extracting objects that satisfy the "connection" contition
    print('Extracting objects')
    ConnectedImage = np.zeros(Air_Objects.shape, dtype = np.uint8)
    for label in Connected.Label:
        print('Object: ', label, 'is connected from top to bottom' )
        Object         = (Air_Objects == label)
        ConnectedImage = ConnectedImage | Object
    if len(Connected) >0:
        ConnectedImage *= 255
        # Saving the image
        print("Saving the image in : ", getcwd())
        File_Name = "Connected_" + Obj_map_File_Name
        tifffile.imsave(File_Name, ConnectedImage)
        del(Object)
        del(Air_Objects)
        del(ConnectedImage)

## Main 1
#Dir = "/home/german.martinez-carvajal/Documents/Tomographie/170411COP/Object Counter"
#Stats_File_Name   = "Statistics for Air" + ".csv"
#Obj_map_File_Name = "Objects map of Air" + ".tif"
#Extract_Volumes(Dir, Stats_File_Name, Obj_map_File_Name, Start_Depth = 400)

# Main 2
Dir = '/home/german.martinez-carvajal/Documents/Tomographie/170411COP/Segmentation(Hashemi)/Boxes/ObjectMapsStats'
List_Stats_files = []
List_Stats_files.append('Statistics for Box50.csv')
List_Stats_files.append('Statistics for Box100.csv')
List_Stats_files.append('Statistics for Box150.csv')
List_Stats_files.append('Statistics for Box200.csv')
List_Stats_files.append('Statistics for Box250.csv')
List_Obj_maps   = []
List_Obj_maps   .append('Objects_map_of_Box50.tif')
List_Obj_maps   .append('Objects_map_of_Box100.tif')
List_Obj_maps   .append('Objects_map_of_Box150.tif')
List_Obj_maps   .append('Objects_map_of_Box200.tif')
List_Obj_maps   .append('Objects_map_of_Box250.tif')

for i in range(len(List_Stats_files)):
    Stats_File_Name     = List_Stats_files[i]
    Obj_map_File_Name   = List_Obj_maps   [i]
    Start_Depth = 1 # In imageJ zslice number starts at 1, (not 0)
    Extract_Volumes(Dir, Stats_File_Name, Obj_map_File_Name, Start_Depth = Start_Depth)
# Finished
print("Finished!")