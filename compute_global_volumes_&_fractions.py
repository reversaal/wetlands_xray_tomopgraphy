# -*- coding: utf-8 -*-
"""
Created on Wed May 22 13:08:22 2019

@author: german.martinez-carvajal
"""

import tifffile
import numpy as np
from os import chdir
import pandas as pd 


def sector_mask(shape,centre,radius,angle_range):
    """
    CODE FROM STACK OVER FLOW
    Return a boolean mask for a circular sector. The start/stop angles in  
    `angle_range` should be given in clockwise order.
    """

    x,y = np.ogrid[:shape[0],:shape[1]]
    cx,cy = centre
    tmin,tmax = np.deg2rad(angle_range)

    # ensure stop angle > start angle
    if tmax < tmin:
            tmax += 2*np.pi

    # convert cartesian --> polar coordinates
    r2 = (x-cx)*(x-cx) + (y-cy)*(y-cy)
    theta = np.arctan2(x-cx,y-cy) - tmin

    # wrap angles between 0 and 2*pi
    theta %= (2*np.pi)

    # circular mask
    circmask = r2 <= radius*radius

    # angular mask
    anglemask = theta <= (tmax-tmin)

    return circmask*anglemask

def relabel_the_outside(image,labels, rw):
    """ if pixels outside the cylinder that contains the sample have the same value for air
        they will be relabeled with the value label[1]
        so, if you follow the conventions for label values
        label [0]= 1 = pixels outside the cylinder, 
        label [1]= 0 = air (i.e voids) pixels
        label [2]= 128 = fouling material
        label [3]= 255 = gravel
    """
    assert (np.count_nonzero(image == 1) == 0 ), 'You must reserve label = 1, for pixels outside the cylinder'
    print("relabeling from 0,128,255 to 0,1,128, 255")

    # CREATING CIRCULAR MASK
    # The cylinder is supposed to be the biggest "circle" inscribed in a squared image
    z,x,y = np.shape(image)[0], np.shape(image)[1], np.shape(image)[2]
    center = int(x/2), int(y/2)
    radius = int((x/2))*rw
    #Selecting circular ROI (region of interest)
    mask_2D = sector_mask((x,y),center,radius,(0,360 ))       
    mask_3D = np.zeros(image.shape, dtype = np.bool)
    for j in range(z):
        mask_3D[j] = mask_2D
    image[np.logical_not(mask_3D)] =  labels[0] # outside the cilinder label = 1
    print("{} pixels have been attributed to tha phase : outside the cylinder".format(np.count_nonzero(image == 1)))
    return image
    
def compute_global_fractions_volumes_depths(r, image, sample_name, start_dl, end_dl, end_gl, path_save, rw):
    """ r = resolution in mm/vox 
        start_dl = int, starting slice of the deposit layer
        end_dl = int, ending slice of the deposit layer, start of the gravel layer
        end_gl = int, ending of the gravel layer # should be the ending slice of the image
        """
    print('computing...')
    zones = ['Total', 'Deposit + Gravel Layer','Deposit Layer', 'Gravel Layer']
    phases = ['Outside', 'Voids', 'Fouling Material' , 'Gravel']
    labels = [1,0,128,255] # each phase has its label 

    df = pd.DataFrame()
    df["Zone"] = zones
    
    depths= []
    depths.append(len(image)*r)
    depths.append((len(image)-start_dl)*r)
    depths.append((end_dl-start_dl)*r)
    depths.append((len(image)-end_dl)*r)    
    df["depth (mm)"]= depths        

    # total volumes in vox (it is necessary to subtract voxels corresponding to the phase outside the cylinder)
    total_volume = np.product(image.shape) - np.count_nonzero(image == 1)
    volume_dep_and_gravel_layer = np.product(image[start_dl:].shape) - np.count_nonzero(image[start_dl:] == 1)
    volume_dep_layer = np.product(image[start_dl:end_dl].shape) - np.count_nonzero(image[start_dl:end_dl] == 1)
    volume_grv_layer = np.product(image[end_dl:end_gl].shape)- np.count_nonzero(image[end_dl:end_gl] == 1)
    
    # loop over all the phases ('Outside', 'Voids', 'Fouling Material' , 'Gravel')
    for i in range(4):
        phase = phases[i]
        label = labels[i]
        fractions = []
        volumes = []

        
        if phase == 'Outside':
            # volume fraction in the whole sample
            fractions.append(np.count_nonzero(image == label)/np.product(image.shape))
            # volume fraction in the deposit + gravel layer
            fractions.append(np.count_nonzero(image[start_dl:] == label)/np.product(image[start_dl:].shape))
            # volume fraction in the deposit layer
            fractions.append(np.count_nonzero(image[start_dl:end_dl] == label)/np.product(image[start_dl:end_dl].shape))
            # volume fraction in the gravel layer
            fractions.append(np.count_nonzero(image[end_dl:end_gl] == label)/np.product(image[end_dl:end_gl].shape))
        else:
            # volume fraction in the whole sample
            fractions.append(np.count_nonzero(image == label)/total_volume)
            # volume fraction in the deposit + gravel layer
            fractions.append(np.count_nonzero(image[start_dl:] == label)/volume_dep_and_gravel_layer)
            # volume fraction in the deposit layer
            fractions.append(np.count_nonzero(image[start_dl:end_dl] == label)/volume_dep_layer)
            # volume fraction in the gravel layer
            fractions.append(np.count_nonzero(image[end_dl:end_gl] == label)/volume_grv_layer)
        
        # volume of each phase in the whole sample # IN cm3
        volumes.append(np.count_nonzero(image == label)*((r**3))/1000)
        # volume of each phase in the deposit + gravel layer # IN cm3
        volumes.append(np.count_nonzero(image[start_dl:] == label)*((r**3))/1000)
        # volume  of each phase the deposit layer
        volumes.append(np.count_nonzero(image[start_dl:end_dl] == label)*((r**3))/1000)
        # volume of  each phase the gravel layer
        volumes.append(np.count_nonzero(image[end_dl:end_gl] == label)*((r**3))/1000)


        
        df["volume fraction of "+ phase] =  fractions
        df["volume (cm3) of " + phase  ] =  volumes
    

    chdir(path_save)
    print("Fractions for sample {}\n".format(sample_name), df)

    df.to_csv('Global_Volume_Fractions_of_sample_{}_rw_{}.csv'.format(sample_name, rw), sep = ';' )
    
    return (df)
    
    
    

def main():
    
    path_read = '/home/german.martinez-carvajal/Bureau/These/Hashemi_segmentation/Tests/Test9_Rapide/Results/heavy_results'
    chdir(path_read) # just to check it exists
    path_save = '/home/german.martinez-carvajal/Bureau/These/Total_volumes_&_Global_fractions/test'
    chdir(path_save) # just to chech it exists
    file_name = 'Hash_Segm_of_Image_Filtered_PVE_50_Tol_0.75_NumVS_1.tif'
    sample_name = "Test"
    slice_start_deposit_layer = 1
    slice_end_deposit_layer = 5
    r = 0.035 # mm/vox
    rw = 1
    # Computing global fractions
    chdir(path_read)
    print('Reading...', path_read, file_name)
    image = tifffile.imread(file_name)
    print('Relabeling the outside')
    labels = [1,0,128,255] # 1 = outside the cylinder, 0 = voids, 128 = fouling material, 255 = gravel
    image = relabel_the_outside(image, labels, rw)
    compute_global_fractions_volumes_depths(r, image, sample_name, slice_start_deposit_layer, slice_end_deposit_layer, len(image), path_save, rw)

main()
print('ok global fractions')
