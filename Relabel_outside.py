# -*- coding: utf-8 -*-
"""
Created on Sun Jun 16 14:36:08 2019

@author: german.martinez-carvajal
"""
import tifffile
import numpy as np
from os import chdir
def sector_mask(shape,centre,radius,angle_range):
    """
    CODE FROM STACK OVER FLOW
    Return a boolean mask for a circular sector. The start/stop angles in  
    `angle_range` should be given in clockwise order.
    """

    x,y = np.ogrid[:shape[0],:shape[1]]
    cx,cy = centre
    tmin,tmax = np.deg2rad(angle_range)

    # ensure stop angle > start angle
    if tmax < tmin:
            tmax += 2*np.pi

    # convert cartesian --> polar coordinates
    r2 = (x-cx)*(x-cx) + (y-cy)*(y-cy)
    theta = np.arctan2(x-cx,y-cy) - tmin

    # wrap angles between 0 and 2*pi
    theta %= (2*np.pi)

    # circular mask
    circmask = r2 <= radius*radius

    # angular mask
    anglemask = theta <= (tmax-tmin)

    return circmask*anglemask
    
def relabel_the_outside(image,labels):
    """ if pixels outside the cylinder that contains the sample have the same value for air
        they will be relabeled with the value label[1]
        so, if you follow the conventions for label values
        label [0]= 1 = pixels outside the cylinder, 
        label [1]= 0 = air (i.e voids) pixels
        label [2]= 128 = fouling material
        label [3]= 255 = gravel
    """
    assert (np.count_nonzero(image == 1) == 0 ), 'You must reserve label = 1, for pixels outside the cylinder'
    print("relabeling from 0,128,255 to 0,1,1298255")

    # CREATING CIRCULAR MASK
    # The cylinder is supposed to be the biggest "circle" inscribed in a squared image
    z,x,y = np.shape(image)[0], np.shape(image)[1], np.shape(image)[2]
    center = int(x/2), int(y/2)
    radius = int((x/2))
    #Selecting circular ROI (region of interest)
    mask_2D = sector_mask((x,y),center,radius,(0,360 ))       
    mask_3D = np.zeros(image.shape, dtype = np.bool)
    for j in range(z):
        mask_3D[j] = mask_2D
    image[np.logical_not(mask_3D)] =  labels[0] # outside the cilinder label = 1
    print("{} pixels have been attributed to tha phase : outside the cylinder".format(np.count_nonzero(image == 1)))
    return image

paths = []
paths.append("/home/german.martinez-carvajal/Desktop/These/Hashemi_segmentation/Tomos_2018/Manip_juillet/MON1A_20180723/heavy_results")
paths.append("/home/german.martinez-carvajal/Desktop/These/Hashemi_segmentation/Tomos_2018/Manip_juillet/MON1B_20180723/heavy_results")
paths.append("/home/german.martinez-carvajal/Desktop/These/Hashemi_segmentation/Tomos_2018/Manip_juillet/MON2A_20180723/heavy_results")
paths.append("/home/german.martinez-carvajal/Desktop/These/Hashemi_segmentation/Tomos_2018/Manip_juillet/MON2B_20180723/heavy_results")
paths.append("/home/german.martinez-carvajal/Desktop/These/Hashemi_segmentation/Tomos_2018/Manip_juillet/MON3A_20180723/heavy_results")
paths.append("/home/german.martinez-carvajal/Desktop/These/Hashemi_segmentation/Tomos_2018/Manip_juillet/MON3B_20180723/heavy_results")

files = []
files.append("Hashemi_Segmentation_of_MON1A_20180723_ROI_Filtered_aniso_Num_VS_1.tif")
files.append("Hashemi_Segmentation_of_MON1B_20180723_ROI_Filtered_aniso_Num_VS_1.tif")
files.append("Hashemi_Segmentation_of_MON2A_20180723_ROI_Filtered_aniso_Num_VS_1.tif")
files.append("Hash_Segm_of_MON2B_20180723_ROI_Filtered_aniso_PVE_75_Tol_0.95_NumVS_1.tif")
files.append("Hash_Segm_of_MON3A_20180723_ROI_Filtered_aniso_PVE_75_Tol_0.95_NumVS_1.tif")
files.append("Hash_Segm_of_MON3B_20180723_ROI_Filtered_aniso_PVE_75_Tol_0.95_NumVS_1.tif")

for i in range(4,5):
    print(i)
    chdir(paths[i])
    image = tifffile.imread(files[i])
    labels = [1, 0, 128, 255]
    print('relabeling')    
    image[0:2000] = relabel_the_outside(image[0:2000], labels)
    image[2000:] = relabel_the_outside(image[2000:], labels)
    tifffile.imsave(files[i].replace('.tif', 'relabeled.tif'), image)
    