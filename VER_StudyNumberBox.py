# -*- coding: utf-8 -*-
"""
Created on Sun Apr 28 10:39:40 2019

@author: german.martinez-carvajal
"""

import matplotlib.pyplot as plt
from os import chdir
import tifffile
import numpy as np
import pandas as pd
from   numpy.random import random_integers as rd_int
from matplotlib.patches import Rectangle

def Make_Simetric_Image_2D (Box):
    Box = np.array(Box)
    Test =    (Box.shape[0] == Box.shape[1])
    if not (Test == True):
        raise AssertionError("2D array should have equal dimensions")
    SymBox = np.zeros(np.array(Box.shape)*3, dtype = Box.dtype)
    Y_len = Box.shape[0]
    X_len = Box.shape[1]
    for i in range(3):
        for j in range(3):
            S_distance = (i-1)**2 + (j-1)**2
            if   S_distance == 0:
                SymBox[Y_len*j:Y_len*(j+1), X_len*i:X_len*(i+1)] = Box
            elif (S_distance == 1 & j == 1):
                SymBox[Y_len*j:Y_len*(j+1), X_len*i:X_len*(i+1)] = np.fliplr(Box)
            elif (S_distance == 1 & i == 1):
                SymBox[Y_len*j:Y_len*(j+1), X_len*i:X_len*(i+1)] = np.flipud(Box)
            elif (S_distance == 2):
                SymBox[Y_len*j:Y_len*(j+1), X_len*i:X_len*(i+1)] = np.flipud(np.fliplr(Box))
    return SymBox

def check_centers(Radii, Centers_X, Centers_Y, slice_2D, n_box, path_save, mode_edges ):
# Graph to chek if aleatory centers are well distributed 
    print('Checking centers, creating graph...')
    for i in range(Radii.size):
        plt.close('all')
        x = Centers_X[i] - Radii[i]
        y = Centers_Y[i] - Radii[i] 
        dx = [Radii[i]*2+1]*len(Centers_X[i])
        fig, ax = plt.subplots()
        ax.set_aspect('equal')
        for x, y, h in zip(x, y, dx):
            ax.add_artist(Rectangle(xy=(x, y), color = 'black', alpha = 0.1, width=h, height=h))      # Gives a square of area h*h
        
        ax.add_artist(Rectangle(xy=(slice_2D.shape[0], slice_2D.shape[1]), fill = False, edgecolor = 'r', width=slice_2D.shape[0], height=slice_2D.shape[1]))
        if  mode_edges == 'Inside':
            shape = 1
        else:
            shape = 3
        ax.set_xlim((0,slice_2D.shape[0]*shape))
        ax.set_ylim((0,slice_2D.shape[1]*shape))
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        chdir(path_save)
        fig.savefig('Check_centers_Nbox_{}_radius_{}_Edges_Mode_{}.png'.format(n_box, Radii[i], mode_edges))
    plt.close("all")
        
def isquare(A):
    print('Exctracting inscribed square...')
    z, x, y = A.shape
    # diameter of the sphere    
    d = x
    # center
    cx, cy = int(x/2), int(y/2)
    # lenght and half lenght of inscribed square
    l = (d/np.sqrt(2))
    l_2 = int(l/2)
    S = A[:,cx-l_2:cx+l_2,cy-l_2:cy+l_2]
    return S 
    
def analysis_for_single_slice (slice_2D, dir_saving, num_slice, resolution, mode_edges, n_box, n_radii, type_graph, flag_check_centers):
            
    ################################################################################0
    #VER analysis with aleatory growing boxes 2D(JUST ONE Z - slice)
    ################################################################################ 
    # slice_2D = 2D image that will pass through VER analysis in this funtion
    # slice_2D is (normally) a segmented image with only three phases labelled 0, 128 and 255   
    # dsave = the directory where you want to save the resulting figures
    # num_slice (int) is nunmber the slice in the sample that you want to analyze
    # num_slice will be part of the name of the figure when it is saved as png
    # N_box is the number of aleatory boxes
    # Check centers: Flag to see (or not) a graph showing the distribution of the centers of the aleatory boxes
    # Save_graphs: Flag to save (or not) the figures resulting from this function 
    # Radii number = the number of points in your VER graph --> Radii = np.linspace(1,Radiux_Max,Points_number)
    # Mode = Indicates how the sampling is pande if the radius of the aleatory box is larger than distance between its center and a border of the image
    #  Inside means that centers are always inside enough not to have indexes out of range

    Test =    (len(slice_2D.shape) == 2)
    if not (Test == True):
        raise AssertionError("Array must be 2D")
        
    Test =    (slice_2D.shape[0] == slice_2D.shape[1])
    if not (Test == True):
        raise AssertionError("2D array must have equal dimensions")
        
    # Center of the image (y,x coordinates)
    Center           = (int(0.5*slice_2D.shape[0]),int(slice_2D.shape[1]*0.5))
    
    # List of radii of the aleatory boxes
    Points_number    = n_radii # Points number in the graph (same as number of radii)
    Radius_Max       = int(0.5*(0.5*np.min(slice_2D.shape)))
    Radius_Min       = int (Radius_Max/10)
    Radii            = np.linspace(Radius_Min,Radius_Max,Points_number,dtype = np.int)
     
    # Number of aleatory boxes
    Number_of_Boxes  = n_box
    
    # Creating centers of the boxes (aleatory)
    Centers_X  = list()
    Centers_Y  = list()
    X_len, Y_len = slice_2D.shape[1],slice_2D.shape[0]
    if  mode_edges == "Inside":
        # taking care of radii to avoid indexing problems
        for Radius in Radii:
            np.random.seed(0)
            Centers_X.append(rd_int(Radius, X_len-Radius, size = Number_of_Boxes))
            np.random.seed(1)
            Centers_Y.append(rd_int(Radius, Y_len-Radius, size = Number_of_Boxes))
        # Previous version to create the centers without np.seed
        #Centers_X    = [rd_int(Radius, X_len-Radius, size = Number_of_Boxes) for Radius in Radii]
        #Centers_Y    = [rd_int(Radius, Y_len-Radius, size = Number_of_Boxes) for Radius in Radii]
    elif (mode_edges == 'Reflect') or (mode_edges == 'Periodic')  :
        for Radius in Radii:
            np.random.seed(0)
            Centers_X.append(rd_int(X_len, 2*X_len, size = Number_of_Boxes))
            np.random.seed(1)
            Centers_Y.append(rd_int(Y_len, 2*Y_len, size = Number_of_Boxes))
    else:
        raise AssertionError("Mode {} not recognized".format(mode_edges)) 
    
    if flag_check_centers == True:
        check_centers(Radii, Centers_X, Centers_Y, slice_2D, n_box, dir_saving, mode_edges)

            
    # Creating limits of the boxes (based on the aleatory centers)
    X_left      = [Centers_X[i] - Radii[i] for i in range(Radii.size)] # Creating boxes boundaries
    X_right     = [Centers_X[i] + Radii[i] for i in range(Radii.size)]
    Y_left      = [Centers_Y[i] - Radii[i] for i in range(Radii.size)]
    Y_right     = [Centers_Y[i] + Radii[i] for i in range(Radii.size)]
    
    # Sampling the boxes
    if   mode_edges == 'Inside':
        # Sampling the of boxes 
        Boxes2D = [[slice_2D[Y_left[i][j]:Y_right[i][j], X_left[i][j]:X_right[i][j]] for j in range(Number_of_Boxes) ] for i in range (Radii.size)] 
    elif mode_edges == 'Periodic':
        Bigger = np.zeros(np.array(slice_2D.shape)*3)
        for i in range(3):
            for j in range(3):
                Bigger[Y_len*j:Y_len*(j+1), X_len*i:X_len*(i+1)] = slice_2D
        # Sampling the of boxes 
        Boxes2D = [[Bigger[X_left[i][j]:X_right[i][j], Y_left[i][j]:Y_right[i][j]] for j in range(Number_of_Boxes) ] for i in range (Radii.size)] 
    elif mode_edges == 'Reflect':
        Bigger = Make_Simetric_Image_2D (slice_2D)
         # Sampling the of boxes 
        Boxes2D = [[Bigger[X_left[i][j]:X_right[i][j], Y_left[i][j]:Y_right[i][j]] for j in range(Number_of_Boxes) ] for i in range (Radii.size)] 

  
   # Computing volume fractions
  
    VF        = [[Compute_Volume_Fractions(Box) for Box in Boxes2D [i]] for i in range (Radii.size)]
    VF        = np.array(VF)

   
    # Computing means and standard deviations

    Means     = VF.mean(axis =1) # axis 1 means : for each the box radius
    Std       = VF.std (axis =1)
    
    Means_Air = Means[:,0]
    Means_OrM = Means[:,1]
    Means_Grv = Means[:,2]
    
    Std_Air   = Std[:,0]
    Std_OrM   = Std[:,1]
    Std_Grv   = Std[:,2]

    # Computing relative standard deviations
    Std_Air_R = Std_Air/Means_Air
    Std_OrM_R = Std_OrM/Means_OrM
    Std_Grv_R = Std_Grv/Means_Grv
    
    # Computing percentiles (25,50,and 75)
    P25 = np.percentile(VF, 25, axis = 1)   
    P50 = np.percentile(VF, 50, axis = 1)
    P75 = np.percentile(VF, 75, axis = 1)

    # Computing µ +/- std
    Mu_plus_Std =   Means + Std
    Mu_minus_Std =   Means - Std
    
    # Computing relative lenghts of the boxe    
    Box_lenghts       = Radii*2+1
    Box_lenghts_Rel   = Box_lenghts/np.min(slice_2D.shape)
    
    # saving data:
    columns = ['Box_lenghts', 'Box_lenghts_Rel'] #1
    columns.extend(['std_Voids', 'std_FM', 'std_Grv' ]) #2
    columns.extend(['r_std_Voids', 'r_std_FM', 'r_std_Grv']) #3
    columns.extend(['mu_Voids', 'mu_FM', 'mu_Grv']) #4
    columns.extend(['mu_plus_Voids', 'mu_plus_FM', 'mu_plus_Grv']) #5
    columns.extend(['mu_minus_Voids', 'mu_minus_FM', 'mu_minus_Grv']) #6
    columns.extend(['P25_Voids', 'P25_FM', 'P25_Grv']) #7
    columns.extend(['P50_Voids', 'P50_FM', 'P50_Grv']) #8
    columns.extend(['P75_Voids', 'P75_FM', 'P75_Grv']) #9
    data = [Box_lenghts, Box_lenghts_Rel] #1
    data.extend([Std_Air, Std_OrM, Std_Grv]) #2
    data.extend([Std_Air_R, Std_OrM_R, Std_Grv_R]) #3
    data.extend([Means_Air,Means_OrM, Means_Grv]) #4
    data.extend([Mu_plus_Std[:,0], Mu_plus_Std[:,1], Mu_plus_Std[:,2]]) #5
    data.extend([Mu_minus_Std[:,0], Mu_minus_Std[:,1], Mu_minus_Std[:,2]]) #6
    data.extend([P25[:,0], P25[:,1], P25[:,2]]) #7
    data.extend([P50[:,0], P50[:,1], P50[:,2]]) #8
    data.extend([P75[:,0], P75[:,1], P75[:,2]]) #9
    
    data = np.transpose(data)
    
    data_results = pd.DataFrame(data, columns = columns)
    chdir(dir_saving)
    mode_edges, n_box, n_radii
    data_results.to_csv("VER_analysis_num_slice_{}_EdgesMode_{}_nbox_{}.csv".format(num_slice, mode_edges, n_box), sep = ';')
    
    
    # creating graphs
    if type_graph == 'r_std' or type_graph == 'all' :
        graph_r_std_vs_box_size(Box_lenghts_Rel, Std_Air_R, Std_OrM_R, Std_Grv_R, slice_2D, dir_saving, num_slice, n_box, mode_edges, resolution)
    if type_graph == 'std' or type_graph == 'all' :
        graph_std_vs_box_size(Box_lenghts_Rel, Std_Air, Std_OrM, Std_Grv, slice_2D, dir_saving, num_slice, n_box, mode_edges, resolution)
    if type_graph == 'percentiles' or type_graph == 'all' :
        graph_percentiles_vs_box_size(Box_lenghts_Rel, P25, P75, P50, slice_2D, dir_saving, num_slice, n_box, mode_edges, resolution )
    if type_graph == 'mu_and_std' or type_graph == 'all' :
        graph_mu_and_std_vs_box_size(Box_lenghts_Rel, Mu_minus_Std, Mu_plus_Std, Means, slice_2D, dir_saving, num_slice, n_box, mode_edges, resolution)
    
    return(Box_lenghts_Rel, Means_Air)

def Compute_Volume_Fractions (box):
    Air_Voxels  = np.count_nonzero((box==0 ))  #Air
    OrM_Voxels  = np.count_nonzero((box==128)) #OrM
    Grv_Voxels  = np.count_nonzero((box==255)) #Grv
    Test =  ( np.product(box.shape) == ( Air_Voxels + OrM_Voxels + Grv_Voxels ) )   
    if not (Test == True):
        raise AssertionError("You must verify that all phase-labels in Box are 0, or, 128, or 255")
    Volume      =  Air_Voxels + OrM_Voxels + Grv_Voxels
    VF_Air, VF_OrM, VF_Grv = Air_Voxels/Volume, OrM_Voxels/Volume, Grv_Voxels/Volume
    return (VF_Air, VF_OrM, VF_Grv)
    
def study_of_n_box(n_points, n_box_max, path_input, file_name, path_save, slice_index, n_radii, resolution, mode_edges, flag_isquare):
    # Reading segmented image
    chdir(path_input)
    segmentation = tifffile.imread (file_name)
    # Taking inscribed sqaure if necessary    
    if flag_isquare == True:
        segmentation = isquare(segmentation)
    # extracting the slice analyzed
    slice2D = segmentation[slice_index]
    chdir(path_save)
    tifffile.imsave('slice_{}.tif'.format(slice_index),slice2D)
    # Study of the number of aleatory voxels
    Frame_Results   = pd.DataFrame()
    Num_Boxes_Max     = n_box_max
    Num_Boxes         = np.logspace(1,np.log10(Num_Boxes_Max),n_points, dtype = np.int)
    print("Number of aleatory boxes  = ", Num_Boxes )
    Radii_number      = 4
    # Computing glocal fractions
    Air_G, OrM_G, Grv_G = Compute_Volume_Fractions (slice2D)
    
    
    flag_check_centers = True
    type_graph = None
    
    # Performing the study for each value of N_box
    for n_box in Num_Boxes:
        print("Study # aleatory boxes N_box = ", n_box)
                # assembling parameters        
        params = []
        params.append(slice2D)
        params.append(path_save)
        params.append(slice_index)
        params.append(resolution)
        params.append(mode_edges)
        params.append(n_box)
        params.append(n_radii)
        params.append(type_graph)
        params.append(flag_check_centers)
        Box_lenghts_Rel, Means_Air = analysis_for_single_slice(*params)

        Frame_Results["Box_Rel_Len"]   =  Box_lenghts_Rel
        Frame_Results["Box_Rel_Len"]   =  ["Mu_of_Voids_VF_for_Rel_Box_Len_{}".format(round(rel_len, 2)) for rel_len in Box_lenghts_Rel]
        Frame_Results["Mu_VF_Air_nbox_{}".format(n_box)]   =  Means_Air
        
    
    Frame_Results = Frame_Results.set_index("Box_Rel_Len")
    Frame_Results = Frame_Results.T
    Frame_Results['n_box'] = Num_Boxes
    for rel_len in Box_lenghts_Rel:
        column_name = "Error_of_Mu_of_Voids_VF_for_Rel_Box_Len_{}".format(round(rel_len, 2))
        Frame_Results[column_name] = 100*(1/Air_G)*(Frame_Results["Mu_of_Voids_VF_for_Rel_Box_Len_{}".format(round(rel_len, 2))]-Air_G)
    Frame_Results.to_csv('Data_Study_n_box_EdgesMode_{}.csv'.format(mode_edges) ,sep = ';')
        
    # first plot
    plot_error_vs_nbox(Frame_Results, Box_lenghts_Rel, path_save, slice2D, resolution, mode_edges, n_points)
    
    # second plot
    plot_mu_vs_nbox(Frame_Results, Box_lenghts_Rel, path_save, slice2D, resolution, mode_edges, n_points, Air_G)
    
    print('Finished VER study of number of aleatory sampled boxes ')

def plot_mu_vs_nbox(Frame_Results, Box_lenghts_Rel, path_save, slice2D, resolution, mode_edges, n_points, Air_G):
    plt.close("all")
    # Plotting Error vs Number of boxes for each radius
    fig, ax = plt.subplots()
    num_sides = 1
    
    for rel_len in Box_lenghts_Rel: # Plotting (relative standard deviations)
        label = "Relative Box's Lenght (%) = {}".format(round(rel_len, 2))
        column_name = "Mu_of_Voids_VF_for_Rel_Box_Len_{}".format(round(rel_len, 2))
        marker =  (num_sides,1,0)
        ax.plot(Frame_Results['n_box'], Frame_Results[column_name], label = label, marker = marker)
        ax.minorticks_on()
        ax.grid(which = 'major', color = 'gray')
        ax.grid(which = 'minor', color = 'gray', linestyle='--')
        num_sides += 1
    ax.axhline(y = Air_G,lw=2, linestyle = '-', color = 'k', label = "Real Volume Fraction")    
    ax.set_xscale('log')
    annotation = "(Region's lenght (mm) = {})".format((int(len(slice2D)*resolution)))
    title = "Analysis_of_number_of_aleatory_boxes_EdgesMode_{}_TypeGraph_Mu_Npoints_{}".format(mode_edges, n_points)
    plt.xlabel('Number of aleatory boxes - ' + annotation)
    ylabel = "Mean of Voids Volume Fractions (%)" 
    plt.ylabel(ylabel)
    plt.legend(fontsize = "small", loc = 'best')
    fig.set_size_inches(10,10)
    chdir(path_save)
    fig.savefig(title + '.png', dpi = 300 )
    plt.close("all")
    
def plot_error_vs_nbox(Frame_Results, Box_lenghts_Rel, path_save, slice2D, resolution, mode_edges, n_points):
    plt.close("all")
    # Plotting Error vs Number of boxes for each radius
    fig, ax = plt.subplots()
    num_sides = 1
    
    for rel_len in Box_lenghts_Rel: # Plotting (relative standard deviations)
        label = "Relative Box's Lenght (%) = {}".format(round(rel_len, 2))
        column_name = "Error_of_Mu_of_Voids_VF_for_Rel_Box_Len_{}".format(round(rel_len, 2))
        marker =  (num_sides,1,0)
        ax.plot(Frame_Results['n_box'], Frame_Results[column_name], label = label, marker = marker)
        ax.minorticks_on()
        ax.grid(which = 'major', color = 'gray')
        ax.grid(which = 'minor', color = 'gray', linestyle='--')
        num_sides += 1
    ax.axhline(y = 0, lw=2, label = "", linestyle = '-', color = 'k')
    ax.set_xscale('log')
    annotation = "(Region's lenght (mm) = {})".format((int(len(slice2D)*resolution)))
    title = "Analysis_of_number_of_aleatory_boxes_EdgesMode_{}_TypeGraph_RelativeError_Npoints_{}".format(mode_edges, n_points)
    plt.xlabel('Number of aleatory boxes - ' + annotation)
    ylabel = "Error of Mean of Voids Volume Fractions (%)" 
    plt.ylabel(ylabel)
    plt.legend(fontsize = "small", loc = 'best')
    fig.set_size_inches(10,10)
    chdir(path_save)
    fig.savefig(title + '.png', dpi = 300, )
    plt.close("all")

def check_paths_and_files(path_input, path_save, image_name):
    assert (os.path.isdir(path_input)) , "Folder: "    + path_input       + " IS NOT A FOLDER!"
    
    chdir (path_input)
    assert (os.path.exists(image_name)), "Scan_File: " + image_name + " DOES NOT EXIST!"
    
    assert (os.path.isdir(path_save)), "Folder: " + path_save + " IS NOT A FOLDER!"
    
####################################################################
# MAIN
####################################################################   
def main():
    plt.close('all')
    global image_Name
    # Directories and file nale
    dir_reading = "/home/german.martinez-carvajal/Desktop/These/Hashemi_segmentation/Tests/Test9_Rapide/Results/heavy_results"
    dir_saving  = "/home/german.martinez-carvajal/Desktop/These/VER/Test/Study_n_box/Periodic"
    image_Name = 'Hash_Segm_of_Image_Filtered_PVE_50_Tol_0.5_NumVS_1.tif'
    check_paths_and_files(dir_reading, dir_saving, image_Name)
    
    # take the biggest square inscribed the circle (cylinder) ??
    flag_isquare = True
    # Resolution
    resolution = 0.035 # mm/voxel
    # slice to be analyzed
    slice_index = 3
    # number of radii
    n_radii = 3
    # mode for samplig near edges
    mode_edges = 'Periodic' # onde of these ["Inside", "Reflect", "Periodic"]
    # number of points for the axis (number of aleatory boxes)
    n_points = 10
    # max number of  aleatory boxes
    n_box_max = 10000
    # assembling parameters for the function study_of_nbox   
    params = []
    params.append(n_points)
    params.append(n_box_max)
    params.append(dir_reading)
    params.append(image_Name)
    params.append(dir_saving)
    params.append(slice_index)
    params.append(n_radii)
    params.append(resolution)
    params.append(mode_edges)
    params.append(flag_isquare)
    # call funtion
    study_of_n_box(*params)
    #(path_input, file_name, path_save, slice_index, resolution, mode_edges, flag_isquare)::
    print("FINISHED")
main()