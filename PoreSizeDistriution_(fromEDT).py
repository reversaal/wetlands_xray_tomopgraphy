import scipy.ndimage
import tifffile
from os import chdir
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt 

# Directory Reading
print('Reading EDT')
Dir = "/home/german.martinez-carvajal/Documents/Tomographie/170411COP/EDT"
chdir(Dir)

# Reading Segmented Image
File_Name = "EDT(Air-Hashemi)" + ".tif"
EDT = tifffile.imread(File_Name)

# Distance map contains radius values
EDT = EDT*2 # Diameter

# Resolution
Res = 0.035 #mm/vox

# Creating histogram
print('Creating histrogram')
# Bins of diameter
Bins      = np.arange(0,max(EDT.shape),2)
Histogram = np.histogram(EDT[EDT>0],Bins)

# Computing volume fractions
print('Computing volume fractions')
Total_volume = np.count_nonzero(EDT>0)
Diameters    = Bins * Res # in mm  
Vol_Frac     = np.append([0],Histogram[0])/Total_volume
Vol_Frac_Cum = np.cumsum(Vol_Frac)


# Plotting
plt.close('all')
plt.figure()
plt.plot(Diameters, Vol_Frac_Cum, color="k",lw=1, label = "EDM", linestyle = '-', marker = '*')
title  = "Pore_Size_Distribution_fromEDM"
plt.xlabel('Diameter (mm)' )
plt.xlim(0)
ylabel = "Cummulative volume fraction (%)"
plt.ylabel(ylabel)
plt.ylim(0)
plt.legend(fontsize = "small", loc = 'upper left')

# Data frame
Result = pd.DataFrame({'Diameter(mm)':Diameters,'Vol_Cum_Frac': Vol_Frac_Cum } )

# Saving
Dir    = '/home/german.martinez-carvajal/Documents/Tomographie/170411COP/Pore_Size_Distribution'
chdir(Dir)
plt.savefig(title +'.png')
File_Name
Result.to_csv(title +'.csv')


print("Finished Pore Size Distribution from EDT!")    
    
    
    