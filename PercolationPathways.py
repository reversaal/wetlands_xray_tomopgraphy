############################################
# SKELETON AND MINIMA WITHIN THE SKELETON
############################################

from __future__ import division
import tifffile
from os import chdir
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import scipy.ndimage.filters as filters
import scipy.ndimage.morphology as morphology
import scipy.ndimage



# Import the SKELETON
chdir("/home/german.martinez-carvajal/Desktop/Lien vers 170411COP/Concatenated/ResultsAv25/2) Segementation Result/300.300.1000")
Name = "Skeleton"
Skeleton_initial = tifffile.imread(Name+".tif")

# Import the EucledianDistanceTransform (same as Euclidean Distance Map)
Name = "EDT.tif"
EDT_initial = tifffile.imread(Name)

# Facultative line : reduce the image to deacrease time 
Skeleton_initial = Skeleton_initial[400:600,160:260,100:200]
EDT_initial = EDT_initial[400:600,160:260,100:200]

# Important lines to draw pathways
Border =  1
Skeleton = Skeleton_initial[Border:-Border,Border:-Border,Border:-Border]
EDT = EDT_initial[Border:-Border,Border:-Border,Border:-Border]

Resolution = 0.035 # milimeters/voxel


############################################
# 1) PLOT SKELETON 
############################################

# Plot
plt.close("all") # close existing figures
z,y,x = Skeleton.nonzero() # Saving the x,y,z coordinates of every voxel of the skeleton
Fig1 = plt.figure(1)
ax = Fig1.add_subplot(111, projection='3d')
ax.scatter(x, y, -z, c= 'red', s = 0.1)
ax.axes.set_xlim(0,Skeleton.shape[2])
ax.axes.set_ylim(0,Skeleton.shape[1])
ax.axes.set_zlim(-Skeleton.shape[0],0)
ax.set_xlabel('X voxel ('+str(Skeleton.shape[2]*Resolution)+"mm)")
ax.set_ylabel('Y voxel ('+str(Skeleton.shape[1]*Resolution)+"mm)")
ax.set_zlabel('Z voxel ('+str(Skeleton.shape[0]*Resolution)+"mm)")
plt.savefig("SkeletonPYTHON.png")
print("Finished1")

##############################################################################################################
# 2a) Pathways with size restrictions
##############################################################################################################

Size_restriction =  1# particle size in voxels

if Border >= Size_restriction:    
    Size_restriction_mm = Size_restriction*Resolution # In milimeters    
    # Save the EclideanDistanceMap's values in the skeleton
    Mask = (Skeleton==255)
    Mask_background = (Skeleton==0)    
    SketEDT = EDT*Mask
    Pathways = (SketEDT >= Size_restriction)
    Pathways = Pathways*Mask
    #Plot
    z,y,x = Pathways.nonzero() # Saving the x,y,z coordinates of every voxel of the skeleton
    Fig2 = plt.figure(2)
    ax = Fig2.add_subplot(111, projection='3d')
    ax.scatter(x, y, -z, zdir='z', c= 'red', s = 0.1)
    ax.axes.set_xlim(0,Skeleton.shape[2])
    ax.axes.set_ylim(0,Skeleton.shape[1])
    ax.axes.set_zlim(-Skeleton.shape[0],0)
    ax.set_xlabel('X voxel ('+str(Skeleton.shape[2]*Resolution)+"mm)")
    ax.set_ylabel('Y voxel ('+str(Skeleton.shape[1]*Resolution)+"mm)")
    ax.set_zlabel('Z voxel ('+str(Skeleton.shape[0]*Resolution)+"mm)")
    plt.savefig("Pathway < : "+str(Size_restriction_mm)+"mm.png")
else:
    print("Borders < Size Restriction! Increase Borders")

print("Finished2")

##############################################################################################################
# 2b) Skeletons with size restrictions EVOLVED
# If the pahtway is not conected to the borders of the image then the braches of the skeleton are deleted
##############################################################################################################
if Border >= Size_restriction:
    A=Pathways*1    # *1 because the generic filter does not take bool inputs
    Neighborhood = np.ones((3,3,3))
    Mask = (A==1) 
    B = scipy.ndimage.generic_filter(A,np.sum,footprint = Neighborhood, origin =(0,0,0))
    B = (B-1)*Mask
    A[B==0]=0    # Eliminate individual isolated voxels
    print("Are there any disconected paths",np.any(B==1), np.sum(B==1))    
    while np.any(B==1):    
        Mask = (A==1)
        B = scipy.ndimage.generic_filter(A,np.sum,footprint = Neighborhood, origin =(0,0,0))
        B = (B-1)*Mask
        A[B<=1]=0        
        print("Are there disconected paths?:",np.any(B==1), np.sum(B==1))
    
    z,y,x = A.nonzero() # Saving the x,y,z coordinates of every voxel of the skeleton
    Fig3 = plt.figure(3)
    ax = Fig3.add_subplot(111, projection='3d')
    ax.scatter(x, y, -z, zdir='z', c= 'red', s = 0.1)
    ax.axes.set_xlim(0,Skeleton.shape[2])
    ax.axes.set_ylim(0,Skeleton.shape[1])
    ax.axes.set_zlim(-Skeleton.shape[0],0)
    ax.set_xlabel('X voxel ('+str(Skeleton.shape[2]*Resolution)+"mm)")
    ax.set_ylabel('Y voxel ('+str(Skeleton.shape[1]*Resolution)+"mm)")
    ax.set_zlabel('Z voxel ('+str(Skeleton.shape[0]*Resolution)+"mm)")
    Title= "Connected Pathways < : "+str(Size_restriction_mm)+"mm"
    plt.savefig(Title+".png")
    #Save the image as tiff
    Image = np.array(A,dtype=np.uint8)*255
    tifffile.imsave(Title+".tif",Image)
#end of the "if" structure


#############################################
## 4) Plot Minima of the EDT
#############################################
#
#maximum = SketEDT.shape[0]*SketEDT.shape[1]*SketEDT.shape[2]
#maximum = np.max(EDT)
#minimum = np.min(EDT[mask_not_background])
#SketEDT[mask_background]= maximum
#
## define a connected neighborhood
## http://www.scipy.org/doc/api_docs/SciPy.ndimage.morphology.html#generate_binary_structure
#neighborhood = morphology.generate_binary_structure(len(SketEDT.shape),2)
#MinFilterSize = 75
#neighborhood = np.ones([MinFilterSize,MinFilterSize,MinFilterSize])
#   
## apply the local minimum filter; all locations of minimum value 
## in their neighborhood are set to 1
## http://www.scipy.org/doc/api_docs/SciPy.ndimage.filters.html#minimum_filter
#local_min = ((filters.minimum_filter(SketEDT, footprint=neighborhood)==SketEDT))
#
## local_min is a mask that contains the peaks we are 
## looking for, but also the background.
## In order to isolate the peaks we must remove the background from the mask.
#local_min = local_min * mask_not_background
#   
##Locate the minima
#minZ, minY, minX  = np.where(local_min)
#MinPositions = minX, minY, minZ
#  
###Plot
#fig = plt.figure(4)
#ax = fig.add_subplot(111, projection='3d')
#ax.scatter(minX, minY, -minZ, zdir='minZ', s=10)
#plt.savefig("Throats of"+Name+str(MinFilterSize)+"vox cube "+"Borders: "+str(border)+"vox.png")
#ax.set_xlabel('X axis')
#ax.set_ylabel('Y axis')
#ax.set_zlabel('Z axis')


#############################################
## 5) Plot Minima andskeleton together 
#############################################
#
#fig = plt.figure(5)
#ax = fig.add_subplot(111, projection='3d')
#ax.scatter(x, y, -z, zdir='z', c= 'red', s = 1)
#sizes = EDT[np.where(local_min)]
#sizes_sorted = np.sort(sizes)
#ax.scatter(minX, minY, -minZ, zdir='minZ', s=sizes*10)
#ax.set_xlabel('X axis')
#ax.set_ylabel('Y axis')
#ax.set_zlabel('Z axis')




## OTHER PIECE OF CODE    
#import mayavi.mlab as mlab
#x, y, z, value = np.random.random((4, 40)) 
#mlab.points3d(x, y, z, value)

#Name = "k)SmallSection75-150-300.tif"
#AirPhase = tifffile.imread(Name)
#AirPhase = (AirPhase==255) # transfoming to bolean
#ParticleSize = 2
#Neighborhood = np.ones([ParticleSize,ParticleSize,ParticleSize])
#AirPhaseEroded = morphology.binary_erosion(AirPhase, structure = Neighborhood)
#AirPhaseDilated = morphology.binary_dilation(AirPhaseEroded, structure = Neighborhood)
#AirPhaseDeleted = (AirPhase != AirPhaseDilated)
#z,y,x = AirPhaseDeleted.nonzero()
#fig = plt.figure(4)
#ax = fig.add_subplot(111, projection='3d')
#ax.scatter(x, y, -z, c= 'red', s = 0.1)
#ax.set_xlabel('X axis')
#ax.set_ylabel('Y axis')
#ax.set_zlabel('Z axis')
#print (np.alltrue(AirPhaseEroded==AirPhase))
#print (np.alltrue(AirPhaseDilated==AirPhase))


