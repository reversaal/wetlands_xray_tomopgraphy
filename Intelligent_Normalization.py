from   os import chdir, getcwd
import tifffile
import scipy
import numpy as np
import skimage.exposure
import imageio
import matplotlib.pyplot as plt

def plot(hist_Orig,hist_Bett):
    print('Plotting')    
    plt.close('all')
    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()
    ax1.plot(hist_Orig[1][1:], hist_Orig[0], lw = 0.5, label = 'Original, nbins = {}'.format(len(hist_Orig[0])), color = "red")
    ax2.plot(hist_Bett[1][1:], hist_Bett[0], lw = 0.5, label = 'Enhanced, nbins = {}'.format(len(hist_Bett[0])), color = "blue")
    ax1.set_xlabel('Gray Value')
    ax1.legend(loc = 'upper left')
    ax2.legend(loc = 'upper right')
    ax1.set_xlabel('Gray Value')
    ax1.set_ylabel('Frequency (Original)')
    ax2.set_ylabel('Frequency (Enhanced) ')
    figure_Name =  'Hist_of_{}_Norm_perc_{}.png'.format(file_name[:-4],p)
    fig.set_size_inches(10,5) 
    fig.savefig(figure_Name, dpi = 300)


def sector_mask(shape,centre,radius,angle_range):
    """
    CODE FROM STACK OVER FLOW
    Return a boolean mask for a circular sector. The start/stop angles in  
    `angle_range` should be given in clockwise order.
    """

    x,y = np.ogrid[:shape[0],:shape[1]]
    cx,cy = centre
    tmin,tmax = np.deg2rad(angle_range)

    # ensure stop angle > start angle
    if tmax < tmin:
            tmax += 2*np.pi

    # convert cartesian --> polar coordinates
    r2 = (x-cx)*(x-cx) + (y-cy)*(y-cy)
    theta = np.arctan2(x-cx,y-cy) - tmin

    # wrap angles between 0 and 2*pi
    theta %= (2*np.pi)

    # circular mask
    circmask = r2 <= radius*radius

    # angular mask
    anglemask = theta <= (tmax-tmin)

    return circmask*anglemask

def create_3D_cylindrical_mask(image, mask_2D, z):
    mask_3D = np.zeros(image.shape, dtype = np.bool)
    for j in range(z):
            mask_3D[j] = mask_2D
    return mask_3D

def Intelligent_Normalization (path, file_name, p, center, radius, mode ):
    """
    Normalize the CT images 
     (because the Enhance Contrast - Normalize function in imageJ has a strage non systematic behavior)
    Inputs: 
    path = Directory of the image 
    p =  Tolerance percentile (between 0-1)
    mode = 0 (Normalize de whole image)
    mode = 1 (normalize  little by little (if RAM limitation))
    BE CAREFUL: in both modes the normalization is based on limit values computed from the stack histograms
    """

    # working directory
    chdir(path)

    # Reading Image
    print ('Reading Image...')
    image = tifffile.imread(file_name)
    
    # Circular Mask
    z,x,y =np.shape(image)[0], np.shape(image)[1],np.shape(image)[2]
    mask_2D = sector_mask((x,y),center,radius,(0,360))       
    mask_3D = create_3D_cylindrical_mask(image, mask_2D, z)
    
    # Defining limits of the normalization
    global v_min, v_max  
    v_min, v_max = np.percentile(image[mask_3D], (p, 100 - p))
    print("Limits of the normalization for percentile {} are \n{} and {} ".format(p, v_min, v_max))
    
    # Stretching
    print('Stretching...')
    if mode == 0:
        # stretching
        stretched = skimage.exposure.rescale_intensity(image, in_range = (v_min, v_max))
    elif mode == 1:
        stretched = np.zeros(image.shape, dtype = image.dtype)
        for z_slice in range(len(image)):
            # print progression            
            progression = z_slice/len(image)*100
            if  z_slice % int(len(image)/100) == 0:
                print("Enhancement progression = {}%".format(int(progression)))
            # stretching
            stretched[z_slice] = skimage.exposure.rescale_intensity(image[z_slice], in_range = (v_min, v_max))
    else :
        raise ValueError ('input : mode = {} not possible'.format(mode))
            
        
    
    # Select number of bins
    if   image.dtype == np.uint8 :
        bins = 2**8
    elif image.dtype == np.uint16:
        bins = int((2**8)*(2**0))
    else:
        raise ValueError('Image type not yet considered')
    
    
    # Computing histograms    
    print('Computing Histograms...')  
    hist_Orig = np.histogram(image[mask_3D]    , bins = bins)
    hist_Bett = np.histogram(stretched[mask_3D], bins = bins)
    
    # Plotting histograms
    plot(hist_Orig,hist_Bett)
    
    # Save normalized image
    print('Saving final result')
    final_name = '{}_Norm_perc_{}.tif'.format(file_name[:-4], p) 
    tifffile.imsave(final_name, stretched)
    
def main():
    global p, file_name
    # Directory 
    path       = '/home/german.martinez-carvajal/Desktop/These/Normalization_Enhance_Contrast/Test_Lourd'
    file_name  = 'Scan2.tif' 
    # Tolerance in (percentiles) (between 0-1)
    p       = 0.01
    # Data for circular mask
    corner = (0,0,182,119)
    radius = (int(1443/2))
    center = (int(corner[0]+radius),int(int(corner[1]+radius)))
    # mode
    mode = 1
    # Executing function
    Intelligent_Normalization(path, file_name, p, center, radius, mode)
    print('Finished!')
main()