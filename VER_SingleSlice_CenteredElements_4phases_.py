# Code to commpute VER of a 2D Image based on volume fractions 
# Volume fraction = pixels of phase X / total pixels
# Three phases maximum, otherwise modify the code

# In this version, we use centered square boxes growing from the center of the image
# to compute the VER

import tifffile
import os
from   os import chdir
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def sector_mask(shape,centre,radius,angle_range):
    """
    CODE FROM STACK OVER FLOW
    Return a boolean mask for a circular sector. The start/stop angles in  
    `angle_range` should be given in clockwise order.
    """

    x,y = np.ogrid[:shape[0],:shape[1]]
    cx,cy = centre
    tmin,tmax = np.deg2rad(angle_range)

    # ensure stop angle > start angle
    if tmax < tmin:
            tmax += 2*np.pi

    # convert cartesian --> polar coordinates
    r2 = (x-cx)*(x-cx) + (y-cy)*(y-cy)
    theta = np.arctan2(x-cx,y-cy) - tmin

    # wrap angles between 0 and 2*pi
    theta %= (2*np.pi)

    # circular mask
    circmask = r2 <= radius*radius

    # angular mask
    anglemask = theta <= (tmax-tmin)

    return circmask*anglemask

def Compute_Volume_Fractions (box):
    Out_Voxels  = np.count_nonzero((box==1 ))
    Air_Voxels  = np.count_nonzero((box==0 ))  #Air
    OrM_Voxels  = np.count_nonzero((box==128)) #OrM
    Grv_Voxels  = np.count_nonzero((box==255)) #Grv
    Reed_Voxels  = np.count_nonzero((box==64 )) # Reeds

    Test =  (np.product(box.shape) == ( Out_Voxels + Air_Voxels + OrM_Voxels + Grv_Voxels + Reed_Voxels) )   

    if not (Test == True):
        raise AssertionError("You must verify that all phase-labels are taken into account")
    
    Volume      =  Air_Voxels + OrM_Voxels + Grv_Voxels + Reed_Voxels
    VF_Air, VF_Reed, VF_OrM, VF_Grv = Air_Voxels/Volume, Reed_Voxels/Volume, OrM_Voxels/Volume, Grv_Voxels/Volume
    return (VF_Air, VF_Reed, VF_OrM, VF_Grv)

def relabel_the_outside_2D(image,labels):
    """ if pixels outside the cylinder that contains the sample have the same value for air
        they will be relabeled with the value label[1]
        so, if you follow the conventions for label values
        label [0]= 1 = pixels outside the cylinder, 
        label [1]= 0 = air (i.e voids) pixels
        label [2]= 128 = fouling material
        label [3]= 255 = gravel
    """
    assert (np.count_nonzero(image == labels[0]) == 0 ), 'You must reserve label[0] = 1, for pixels outside the cylinder'
    print("relabeling from 0,128,255 to 1,0,128, 255")

    # CREATING CIRCULAR MASK
    # The cylinder is supposed to be the biggest "circle" inscribed in a squared image
    x,y = np.shape(image)[0], np.shape(image)[1]
    center = int(x/2), int(y/2)
    radius = int(x/2)
    #Selecting circular ROI (region of interest)
    mask_2D = sector_mask((x,y),center,radius,(0,360 ))       
    image[np.logical_not(mask_2D)] =  labels[0] # outside the cilinder label = 1
    print("{} pixels have been attributed to the phase : outside the cylinder".format(np.count_nonzero(image == 1)))
    return image
    
def VER_single_slice(path_input, path_saving, image_name, z_slice, type_sample_element, flag_isquare, resolution, labels):
    
    # Reading image
    
    #Reading segmented image
    chdir(path_input)
    segmentation = tifffile.imread (image_name)
    
    sementation = relabel_the_outside_2D(segmentation,labels)

    
    # Taking inscribed sqaure if necessary    
    if flag_isquare == True and type_sample_element == 'square':
        segmentation = isquare(segmentation)
        
    # Selecting the slice        
    slice2D = segmentation
    chdir(path_saving)
    tifffile.imsave('slice_{}_{}.tif'.format(z_slice, type_sample_element),slice2D)
    
    
    # CB = cenered boxes
    ################################################################################
    #  VER analysis with growing centered boxes (JUST ONE Z - slice)
    ################################################################################    
    Test =    (slice2D.shape[0] == slice2D.shape[1])
    if not (Test == True):
        raise AssertionError("2D array must have equal dimensions")
    
    # Center of the image (y,x coordinates)
    Center           = (int(0.5*slice2D.shape[0]),int(slice2D.shape[1]*0.5))
    
    # List of boxes radii
    Points_number    = 30 # Points number in the graph (same as number of radii)
    Radiux_Max       = int(0.5*np.min(slice2D.shape))
    Radii            = np.linspace(1,Radiux_Max,Points_number,dtype = np.int)
    
    
    # creating sampling boxes depending on thir shape
    if type_sample_element == 'square':
        # Sampling the boxes
        Boxes2D          = [slice2D [Center[0]-Radius:Center[0]+Radius, Center[1]-Radius:Center[1]+Radius] for Radius in Radii]
        # Adding a las element to be sure that the last box is the whole 2D image 
        Radii            = np.append(Radii,0.5*np.min(slice2D.shape))
        Boxes2D.append(slice2D) 
    elif type_sample_element == 'circle':
        global circle_masks
        circle_masks = [sector_mask(slice2D.shape, Center, radius, (0,360)) for radius in Radii ]
        circle_masks.append(sector_mask(slice2D.shape, Center, Radiux_Max, (0,360)))
        chdir(path_saving)
        tifffile.imsave('masks.tif',np.array(circle_masks, dtype = np.uint8)*255)        
        Boxes2D = [slice2D[circle_mask] for circle_mask in circle_masks]
        Radii            = np.append(Radii,0.5*np.min(slice2D.shape))
    else:
        raise AssertionError("type_sample_element: {} not considered".format(type_sample_element))
    
    
    # Computing volume fractions in the boxes
    Volume_Fractions = [Compute_Volume_Fractions(Box) for Box in Boxes2D]
    VFS = ["VF_Voids", "VF_Reeds", "VF_FM","VF_Grv"]
    Volume_Fractions = pd.DataFrame(Volume_Fractions, columns = VFS)
    Volume_Fractions["Radii"] = Radii
    
    # Computing volume fractions (base excenta de juncos)
    for VF in VFS:
        key = VF + '*'
        Volume_Fractions[key] = Volume_Fractions[VF]/(1-Volume_Fractions['VF_Reeds'])
    
    # Computing relative lengths of the boxes
    Box_Lengths      = Radii*2
    Volume_Fractions["Box_Lengths"]     = Box_Lengths
    Volume_Fractions["Rel_Box_Lengths"] = Box_Lengths/np.min(slice2D.shape)
    
    # Saving data
    chdir(path_saving)
    Volume_Fractions.to_csv('data_VER_4phases_centered_{}_slice_{}.csv'.format(type_sample_element, z_slice), sep = ';')
        
    # creatig figure
    figure(type_sample_element, Volume_Fractions, z_slice, slice2D, resolution, path_saving) 
    print('finished!')
    
    
def figure(type_sample_element, Volume_Fractions, z_slice, slice2D, resolution, path_saving ):
    plt.close('all')
    plt.figure()
    #linestyles
    from collections import OrderedDict
    ls = OrderedDict(
    [('solid',               (0, ())),
     ('loosely dotted',      (0, (1, 5))),
     ('dotted',              (0, (1, 3))),
     ('densely dotted',      (0, (1, 1))),])
     
    plt.plot(Volume_Fractions.Rel_Box_Lengths,Volume_Fractions.VF_Voids,color="b",lw=1, label = "Voids", linestyle = ls['solid'])
    plt.plot(Volume_Fractions.Rel_Box_Lengths,Volume_Fractions.VF_Reeds,color="g",lw=1, label = "Reeds", linestyle = ls['densely dotted'])
    plt.plot(Volume_Fractions.Rel_Box_Lengths,Volume_Fractions.VF_FM,color="r",lw=1, label = "FM", linestyle = ls['dotted'])
    plt.plot(Volume_Fractions.Rel_Box_Lengths,Volume_Fractions.VF_Grv,color="k",lw=1, label = "Grv", linestyle = ls['loosely dotted'])
    title = "VER_Centered_{}_Slice_{}".format(type_sample_element, z_slice,)
    if type_sample_element == 'square':
        annotation = "(L_max = {}mm )".format(int(len(slice2D)*resolution))
        plt.xlabel('Relative box length ' + annotation)
    elif type_sample_element == 'circle':
        annotation = "(d_max = {}mm)".format(int(len(slice2D)*resolution))
        plt.xlabel('Relative diameter ' + annotation)
    plt.xlim(0,1)
    plt.ylim(0,1)
    plt.ylabel('Volume Fraction')
    plt.legend(fontsize = "small", loc = 'best')
    chdir(path_saving)
    plt.savefig(title+'.png')


def check_paths_and_files(path_input, path_save, image_name):
    assert (os.path.isdir(path_input)) , "Folder: "    + path_input       + " IS NOT A FOLDER!"
    
    chdir (path_input)
    assert (os.path.exists(image_name)), "Scan_File: " + image_name + " DOES NOT EXIST!"
    
    assert (os.path.isdir(path_save)), "Folder: " + path_save + " IS NOT A FOLDER!"

def isquare(A):
    print('Exctracting inscribed square...')
    x, y = A.shape
    # diameter of the sphere    
    d = x
    # center
    cx, cy = int(x/2), int(y/2)
    # length and half length of inscribed square
    l = (d/np.sqrt(2))
    l_2 = int(l/2)
    S = A[cx-l_2:cx+l_2,cy-l_2:cy+l_2]
    return S
    
def main():
    # Closing preexisting figures
    plt.close('all')
    # Paths
    path_input = "/home/german.martinez-carvajal/Desktop/These/VER_4phases/MON1A_20180723/centered"
    path_saving  = "/home/german.martinez-carvajal/Desktop/These/VER_4phases/MON1A_20180723/centered"
    # Other parameters
    resolution = 0.035 # mm/vovel
    type_sample_element = 'circle' # 'square' or 'circle'
    flag_isquare = False
    labels = [1,0,64,128,255]    
    # Names of the files    
    files = ['slice_675_circle_4phases.tif']
    files.append('slice_3700_circle_4phases.tif')
        
    z_slices = [675]
    z_slices.append(3700)   
    # take the biggest square inscribed the circle (cylinder) ??

    # check if files exist
    for file in files:
        check_paths_and_files(path_input, path_saving, file)

    for i in range(len(files)):
        file = files[i]
        z_slice = z_slices[i]
        # Call function CB : centered boxes
        VER_single_slice(path_input, path_saving, file, z_slice, type_sample_element, flag_isquare, resolution, labels)
        print('main routine for file {} finished'.format(file))
main()