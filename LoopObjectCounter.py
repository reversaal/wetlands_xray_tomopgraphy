""" Perform the object counter plugin over """
import os 
from ij import IJ
from ij.io import FileSaver
from os import path
folder      = "/home/german.martinez-carvajal/Documents/Tomographie/170411COP/Segmentation(Hashemi)/BoxesofAir/Boxes"
assert (path.isdir(folder)) , "Folder IS NOT A FOLDER: " + folder
folder_save = "/home/german.martinez-carvajal/Documents/Tomographie/170411COP/Segmentation(Hashemi)/BoxesofAir/ObjectsMaps&Stats"
assert (path.isdir(folder_save)) , "Folder Save IS NOT A FOLDER: " + folder_save
for filename in os.listdir(folder):
	print (filename)
	Imp = IJ.openImage(os.path.join(folder,filename))
	# To run the 3D OC options, copy the function call using the macro recorder
	IJ.run("3D OC Options", 
			"volume surface nb_of_obj._voxels nb_of_surf._voxels centroid mean_distance_to_surface std_dev_distance_to_surface median_distance_to_surface centre_of_mass bounding_box close_original_images_while_processing_(saves_memory) dots_size=5 font_size=10 store_results_within_a_table_named_after_the_image_(macro_friendly) redirect_to=none")
	Argument  = 'threshold=128' + ' '
	Argument += 'min.=0 max.=' + str(Imp.height*Imp.width*Imp.getNSlices()) + ' '
	Argument += 'objects'    + ' ' 
	Argument += 'statistics' + ' '
	Argument += 'summary' 
	print(Argument)
	IJ.run(Imp, "3D Objects Counter", Argument)
	# Saving the statistics file
	filepath = folder_save + '/' + 'Statistics_for' + Imp.getTitle()[:-4] +'.csv'
	print(filepath)
	IJ.saveAs("Results", filepath)
	# Saving the objects map
	Imp = IJ.getImage()
	fs = FileSaver(Imp)	
	filepath = folder_save + '/' + 'Objects_map_of_' + filename
	fs.saveAsTiff(filepath)
	Imp.close()
print('ok')
