
path_reading = "/scratch/german.martinez-carvajal/Manip_Juillet/MON1B_20180723_aniso/Results/heavy_results"
path_saving = "/scratch/german.martinez-carvajal/Manip_Juillet/PSD_LcThk/MON1B"
file_name = "Hashemi_Segmentation_of_MON1B_20180723_ROI_Filtered_aniso_Num_VS_1_section_0"

print("Reading...");
print(path_reading + '/' + file_name + '.tif');

open(path_reading + '/' + file_name + '.tif');
makeOval(0, 0, 1378, 1378);
run("Invert", "stack");
setBackgroundColor(0, 0, 0);
run("Clear Outside", "stack");

print("Computing...");
run("Local Thickness (complete process)", "threshold=250");

print("Saving...");
saveAs("Tiff",path_saving + '/' + "LocThkMap_" + file_name + ".tif"); 
close();
close();
