# -*- coding: utf-8 -*-
"""
Created on Thu Jun  6 15:17:40 2019

@author: german.martinez-carvajal
"""
from os import chdir
import tifffile
import numpy as np
import math

def cut_image_with_covering_zones(image_name, sample_name, path_read, path_save, section_size, covering_size, starting_slice  = 0):
    # reading image
    print('Reading image ', image_name, '...')
    chdir(path_read)
    image = tifffile.imread(image_name)
    length = len(image)
    num_sections = math.ceil((length-starting_slice)/section_size)
    # indexes to slice the image
    start_indexes = np.arange(starting_slice, length, section_size)-covering_size
    start_indexes[0] = starting_slice
    end_indexes = np.arange(starting_slice + section_size, length, section_size)+covering_size
    end_indexes= np.append(end_indexes, length)
    print('Indexes')
    print(list(zip(start_indexes, end_indexes)))
    assert(len(start_indexes) == len(end_indexes))
    assert(len(start_indexes) == num_sections)
    
    # extracting and saving the sections
    for i in range(num_sections):
        start = start_indexes[i]
        end = end_indexes[i]
        section = image[start: end]
        output_name = sample_name + '_section_{}.tif'.format(i)
        chdir(path_save)
        tifffile.imsave(output_name, section)

def main():
    image_name = 'Image_Filtered.tif'
    sample_name = 'MONxx'
    path_read = '/home/german.martinez-carvajal/Bureau/These/Hashemi_segmentation/Tests/Test9_Rapide'
    path_save = path_read
    section_size = 2
    covering_size = 1
    starting_slice = 3
    cut_image_with_covering_zones(image_name, sample_name, path_read, path_save, section_size, covering_size, starting_slice)
main()