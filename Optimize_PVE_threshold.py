import tifffile
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal, scipy.optimize 
#from scipy.signal import find_peaks
def smooth_by_polyfit(x, y, deg = 6):
    """
    deg = let's say that deg = 6 for a three peak gaussian
    """
    # comûputing coefficients; highest power first
    p = np.polyfit(x, y, deg)
    y_smooth = np.polyval(p, x)
    return y_smooth
    
def compute_histograms(image, edges, pve_percentile, Num_VS, N_bins):
    """ Computes the histogram of the Image "raw" and without the PVE voxels
        for each section dependin on the number Num_VS
        Num_VS = Number of vertial sections """
    
    cut_pve = np.percentile(edges, pve_percentile)
    pve     = (edges > cut_pve)
    Image_type = image.dtype
    Bins = Create_Bins(Image_type, N_bins)
    Limits = np.linspace(0, len(image), Num_VS+1, dtype = int)# indexes
    
    Histograms              = [] # A list that will store an histogram for each vertival section
    Histograms_without_PVE  = [] # Similar but with histograms without PVE voxels

    for i in range(Num_VS):

        Image_Section = image[ Limits[i]:Limits[i+1] ]
        Histogram     = np.histogram(Image_Section, bins=Bins)
        Histogram     = np.array(Histogram[0])
        Histogram[ 0] = 1  # Some make up to hide extreme peaks due to saturated pixels (very important not to delete this lines if the region out of the cylinder is black (value = 0))
        Histogram[-1] = 1  # Some make up to hide extreme peaks due to saturated pixels
        Histograms.append(Histogram)
        
        PVE_Section       = pve  [ Limits[i]:Limits[i+1] ]
        Not_PVE_section   = (PVE_Section==0) # Borders = PVE = 255 ; Not Borders = Not_PVE = 0         
        Image_without_PVE = Image_Section[Not_PVE_section]                                                  
        Histogram_without_PVE = np.histogram(Image_without_PVE, bins=Bins)
        Histogram_without_PVE = np.array    (Histogram_without_PVE[0]) # index 0 is the frequency vector, index 1 is the bins vector
        Histogram_without_PVE[ 0] = 1  # Some make up to hide extreme peaks due to saturated pixels
        Histogram_without_PVE[-1] = 1  # Some make up to hide extreme peaks due to saturated pixels

        Histograms_without_PVE.append(Histogram_without_PVE)
    return (Histograms, Histograms_without_PVE)

def Create_Bins(Image_type, N_bins):
    if Image_type == np.uint8:
        Bins = np.linspace(0,2**8 ,N_bins) # 256   Gray values for 8 -bit image
    if Image_type == np.uint16:        
        Bins = np.linspace(0,2**16,N_bins) # 65536 Gray values for 16-bit image
    return (Bins)

def moving_average(a, n) :
    """
    input
    a = array like
    n = neighbors to the right and left
    this means the average window has a size of 2*n+1
    
    output a_smoothed
    """
    a_ext = np.concatenate(([a[0]]*n,a,[a[-1]]*n ))
    a_smooth = np.zeros(len(a))
    for i in range(n,len(a)+n):
       a_smooth[i-n] = np.mean(a_ext[i-n:i+n+1])
    return a_smooth   



def plot_hist_smooth(x,y):
    
        plt.figure()
        plt.plot        (x[1:  ],y, color="k",lw=1 , label = 'smooth histogram')
        plt.xlabel('Gray value')
        plt.ylabel('Frequency')
        plt.legend(fontsize = "small", loc = 'best')
        Title = "smooth_histogram_of_" + image_file[:-4]
        plt.savefig(path+'/'+Title+'.png')

def create_smooth_histogram(H, n):
    """
    n = neighbors for moving average
    """
    # Creating smooth histogram
    hist_smooth = moving_average(H,n)
    return hist_smooth

def compute_rel_dep(image,edges, pve_percentile, n):
    """
    input n = number of neighbors for the smoothing
    """
    # Creating histograms without pve voxels
    N_bins = 256
    histograms, histograms_without_PVE = compute_histograms (image, edges, pve_percentile, Num_VS = 1, N_bins = N_bins)
    
    # Smoothing
    # Creating smooth histogram without PVE and plotting it
    hist_PVE_smooth = create_smooth_histogram(histograms_without_PVE[0], n)
    #hist_PVE_smooth = smooth_by_polyfit(Create_Bins(image_type, N_bins=256)[1:], histograms_without_PVE[0])
    # Creating original smoothed histogram and plotting it
    hist_smooth = create_smooth_histogram(histograms[0], n)
    #hist_smooth = smooth_by_polyfit(Create_Bins(image_type, N_bins=256)[1:], histograms[0])
    
    # finding maximums
    distance = (15000/2**16)*N_bins # warning, for a 16int type image
    indices_max, _ = scipy.signal.find_peaks(hist_smooth, distance = distance) # computing position of the peaks (maximum heights)
#    assert len(indices_max) == 3, 'More than three peaks found! Consider changing smothing'
    #print(indices_max)
    #print(bins[indices_max])
    print('Peaks for PVE {}% are {} at:'.format(pve_percentile, len(indices_max)) , bins[indices_max])
    
    # finding minimums
    distance = (15000/2**16)*N_bins
    indices_min, _ = scipy.signal.find_peaks(-hist_PVE_smooth, distance = distance) # computing position of the valleys (minimum heights)
#    assert len(indices_min) == 2, 'More than two minimums found! Consider changing smothing'
    #print(indices_min)
    #print(bins[indices_min])
    print('Valleys for PVE {}% are {}:'.format(pve_percentile, len(indices_min)) , bins[indices_min])
    
    
    # Relative depression 
    # The minimum between the two first peaks divided by the height of the first* peak without
    # * =  the peak of the raw histogram, i.e. without performing a mask with the pve voxels
    rel_dep =  hist_PVE_smooth[indices_min][0]/hist_smooth[indices_max][0]
    print('relative depression = ',rel_dep)
    return(rel_dep)

def check_image_type(image):
    # Checking the Image type
    image_type = image.dtype
    check      = (image_type == np.uint8) or (image_type == np.uint16)
    assert (check) , 'Scan_File MUST be 8-bit oR 16-bit !'
    return(image_type)
    
def plot_histograms(image_file, Bins, Histogram, Histogram_without_PVE, Histogram_smooth, Histogram_without_PVE_smooth, pve_percentile):
    plt.figure()
    plt.plot        (Bins[1:  ],Histogram,                color="b",      lw=1  , label = "Original Histogram")
    plt.plot        (Bins[1:  ],Histogram_smooth,         color="b",     linestyle = ":",  lw=1  , label = "smoothed")
    plt.plot        (Bins[1:  ],Histogram_without_PVE,    color="k",      lw=1  , label = "Histogram without PVE-voxels " + str(pve_percentile) + "%")
    plt.plot        (Bins[1:  ],Histogram_without_PVE_smooth, color="k",     linestyle = ":",  lw=1  , label = "smoothed")
        
    plt.fill_between(Bins[1:  ],Histogram_without_PVE, color = 'gray' , alpha = 0.2  )
    plt.xlabel('Gray value')
    plt.ylabel('Frequence')
    plt.legend(fontsize = "small", loc = 'best')
    Title = "Histograms_of_{}_pveP_{}".format(image_file[:-4],pve_percentile)
    plt.savefig(path+'/'+Title+'.png')

def main():
    plt.close('all')
    global path
    path = '/home/german.martinez-carvajal/Desktop/These/Blurring_&_pve_analysis/anisotropic_filter'
    
    print('Reading image file')
    image_file = 'B_anisotropic.tif'
    image = tifffile.imread(path + '/' + image_file)
    
    print('Reading edges file')
    edges_file = 'B_anisotropic_edges.tif'
    edges = tifffile.imread(path + '/' + edges_file)
    
    global image_type, bins
    image_type = check_image_type(image)
    bins = Create_Bins(image_type, N_bins=256)

    # Comuting relative depression of the first valley of the image histogram for a certain pve_percentile
    pve_percentile= 100
    n = 10 # neighbors for smoothing
    rel_dep = compute_rel_dep(image, edges, pve_percentile, n)
    
    # Computing histograms
    Histograms, Histograms_without_PVE = compute_histograms(image, edges, pve_percentile, Num_VS=1, N_bins=256)
    Histogram_smooth = create_smooth_histogram(Histograms[0], n)
    Histogram_without_PVE_smooth = create_smooth_histogram(Histograms_without_PVE[0], n )
    
    # plotting    
    plot_histograms(image_file, bins, Histograms[0], Histograms_without_PVE[0], Histogram_smooth, Histogram_without_PVE_smooth, pve_percentile)
    
    # Computing relative deprestion for different pve_percentil values
    list_rel_dep = []
    pves = np.linspace(100,10,10)
    for pve_percentile in pves:
        # Computing relative depression
        rel_dep = compute_rel_dep(image, edges, pve_percentile, n)
        list_rel_dep.append(rel_dep)
        print('relative depression = ',rel_dep)
        # Computing histograms
        Histograms, Histograms_without_PVE = compute_histograms(image, edges, pve_percentile, Num_VS=1, N_bins=256)
        Histogram_smooth = create_smooth_histogram(Histograms[0], n)
        Histogram_without_PVE_smooth = create_smooth_histogram(Histograms_without_PVE[0], n)
        # Plotting
        plot_histograms(image_file, bins, Histograms[0], Histograms_without_PVE[0], Histogram_smooth, Histogram_without_PVE_smooth, pve_percentile)
    
    # Print the results of the former loop    
    for line in (zip(pves,list_rel_dep)):
        print (line)    
main()
plt.close('all')

## Optimization of pve percentile
## Guess of pve percentile
#g_pve_p = 75
## Tolerance of rela
#tol_dep_rel = 0.20
## function to be optimized
#def f(pve_p):
#    return compute_rel_dep(pve_p, n = 30) - tol_dep_rel
#
#best_pve_percentil = scipy.optimize.newton(f, g_pve_p, tol = 0.01)
#print(best_pve_percentil)


#plt.close('all')
print('finished')