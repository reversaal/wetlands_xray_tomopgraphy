# -*- coding: utf-8 -*-
"""
Created on Thu Jun  6 18:21:52 2019

@author: german.martinez-carvajal
"""

import tifffile
import matplotlib.pyplot as plt
import numpy as np
from os import chdir

def figure(diameters_real,volume_fractions,cum_volume_fractions, path_save, file_name):

        # Plotting
        plt.close('all')
        fig, ax1 = plt.subplots()
        ax1.bar(diameters_real, volume_fractions,
                color= "gray",
                label = "Fraction    " ,
                width = 0.06 )
        ax2 = ax1.twinx()  
        
        ax2.plot(diameters_real, cum_volume_fractions, 
                 color= "k",
                 lw=1,
                 label = "Cummulative Fraction",
                 linestyle = '-' )
        ax1.legend(loc = 'upper left'  , bbox_to_anchor =(0.0, 1.1))
        ax2.legend(loc = 'upper right' , bbox_to_anchor =(1.0, 1.1))
        ax1.set_xlabel('Diameter (mm)' )
        ax1.set_ylabel("Fraction (% v/v)")
        ax2.set_ylabel("Cumumative Fraction (% v/v)")
        ax1.set_xlim(0)
        ax1.set_ylim(0)
        ax2.set_ylim(0,1)
        # Saving figure
        chdir(path_save)    
        title  = 'Figure_PSD_from_{}'.format(file_name.replace('.tif', ""))
        fig.set_size_inches(9, 7)
        fig.savefig(title + '.png', dpi = 300)
        
def PSD_from_pore_size_map(pore_size_map, path_read, path_save, resolution):
    print('Reading pore size map ... ', pore_size_map)    
    chdir(path_read)
    image = tifffile.imread(pore_size_map)
    print('Counting_elements')
    unique_elements, counts = np.unique(image, return_counts = True)
    diameters_real = unique_elements*resolution # result in mm
    counts[0] = 0 # count for pores of size 0 is 0
    total = np.sum(counts)    
    volume_fractions = counts/total
    cum_volume_fractions = np.cumsum(volume_fractions)
    print('Creating figure')    
    figure(diameters_real,volume_fractions,cum_volume_fractions, path_save, pore_size_map)
    
def main():
    pore_size_map = 'DistMap_FFT_fusion_of_Segm.tif'
    path_read = '/home/german.martinez-carvajal/Bureau/Tests_convering_cut_fusion'
    path_save = '/home/german.martinez-carvajal/Bureau/Tests_convering_cut_fusion'
    resolution = 1  # mm/cm
    PSD_from_pore_size_map(pore_size_map, path_read, path_save, resolution)

main()