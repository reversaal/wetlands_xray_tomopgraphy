import scipy.ndimage
import tifffile
from os import chdir
import numpy as np

# Directory Reading
Dir = "/home/german.martinez-carvajal/Documents/Tomographie/170411COP/Segmentation(Hashemi)"
chdir(Dir)

# Reading Segmented Image
File_Name = "Sample-100slices-Values(0-128-255)" + ".tif"
Seg = tifffile.imread(File_Name)

# Selecting Air Phase
Air = (Seg == 0)

# Closing Air Phase
Air_Closed = scipy.ndimage.morphology.binary_closing(Air)

# Testing if changes occured
Changes  = - np.alltrue(Air == Air_Closed)

# Saving Floating Objects
if Changes == True:
    Floating = (Air != Air_Closed)
    Floating = 255*np.array(Floating, dtype = np.uint8)
    File_Name = "Floating_Objects" + '.tif'
    tifffile.imsave(File_Name, Floating)
