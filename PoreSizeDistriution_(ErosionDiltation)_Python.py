import scipy.ndimage
import tifffile
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from   os import chdir
import os
import datetime as dt
import pymrt as mrt
import pymrt.geometry

# I just wanted to point out that this script computes PSD by perfomin erosions and dilations in series
# this induces a conceptual error because, for example, 2 erosions in series with a structure of radius = 1, is NOT 
# the same thing as a sibngle erosion with a structure of radius 2


def compute_pore_size_distribution(flag_resume, path_input, path_save, file_name, resolution, labels , flag_relabel = False, time_limit = None, Image_PSD = None, PSD_file = None, increment = 1):
    # Checking time to optimise the time consumed when writing files
    time_start = dt.datetime.now()
    if time_limit != None:
        time_end = time_start + time_limit
     
    # Reading Segmented Image
    print('reafing', file_name)
    chdir(path_input)
    segmentation = tifffile.imread(file_name)
    
    if flag_relabel == True:
        segmentation = relabel_the_outside(segmentation,labels)
    
    # Selecting Air Phase
    air = (segmentation == labels[1] )
    
    # total volume in vox
    volume_total_vox = np.count_nonzero(air)
    
    # Retrieving Memory
    del segmentation
    
    tvv = np.count_nonzero(air)
    print('Total void voxels = ', tvv )
    
    # Erosion element
    struct = mrt.geometry.sphere(3,1)    
    
    # Initializing    
    erosion = air.copy()


    # These lists will store the information for the Pore Size Distribution figures    
    if flag_resume == False:
        last_dilation = air.copy()
        erosion_start = 0
        distance_map = np.zeros( air.shape , dtype = np.uint16 )
        diameters_vox = [0] # list of radii  of pores  (will grow in the next for)
        diameters_real = [0]
        cum_volumes_vox = [0] # list of volumes of pores belonging to a radii class (will grow in the next for)
        cum_volumes_real = [0]
        cum_volume_fractions = [0]
        volumes_vox = [0]
        volumes_real = [0]
        volume_fractions = [0]
        volumes_vox_from_LD = [0] # # for the comutations by comparison with the LD = last dilation
    else:
        chdir(path_save)
        distance_map = tifffile.imread(Image_PSD)
        previous_results = pd.read_csv(PSD_file, sep = ';')
        erosion_start = previous_results.index[-1]+1
        print("starting from erosion step number # ", erosion_start)
        diameters_vox = list(previous_results['diameters_vox']) # list of radii  of pores  (will grow in the next for)
        diameters_real = list(previous_results['diameters_mm'])
        cum_volumes_vox = list(previous_results[ 'cum_volumes_vox']) # list of volumes of pores belonging to a radii class (will grow in the next for)
        cum_volumes_real = list(previous_results['cum_volumes_mm3'])
        cum_volume_fractions = list(previous_results['cum_volume_fractions'])
        volumes_vox = list(previous_results['volumes_vox'])
        volumes_real = list(previous_results['volumes_mm3'])
        volume_fractions = list(previous_results['volume_fractions'])
        volumes_vox_from_LD = list(previous_results['volumes_vox_from_LD'])
        
    # resuming erosion
    for repeat in range(erosion_start-1):
        # if erosion_start > 1
        erosion   = scipy.ndimage.morphology.binary_erosion(erosion, structure = struct)

    # resuming dilation
    for repeat in range(erosion_start-1):
        # if erosion_start > 1
        last_dilation   = scipy.ndimage.morphology.binary_dilation(erosion, structure = struct)

    # loop - computing pore size distribution
    max_diameter  = int(max(air.shape)) # Max_radius will control the main loop of this program
    
    for i in range(erosion_start, max_diameter, increment):
        print('Step: Radius of erosion (in pixels) = ', i)
        
        if i == 0:
            continue

        # Computing erosions
        time_start_erosion = dt.datetime.now()
        for repeat in range(increment):
            erosion   = scipy.ndimage.morphology.binary_erosion(erosion, structure = struct)
        # adjustment if increment > 1
        
        print('Remaining air voxels (after erosion ) = ', np.count_nonzero(erosion))
        
        # Computing dilations  
        dilation = erosion.copy()
        for repeat in range(i):
            dilation  = scipy.ndimage.morphology.binary_dilation(dilation, structure = struct)
        print('Remaining air voxels (after dilation) = ', np.count_nonzero(dilation))    
        time_finish_diltation = dt.datetime.now()
        time_erosion_dilation = time_finish_diltation - time_start_erosion
        
        # Creating "distance map" based in the concept of erosion dilation 
        mask0 = (air) # belongs to air phase ?
        mask1 = (dilation != air) # is a pore that corresponds to this step
        mask2 = (distance_map == 0) # it has not been attributed yet 
        mask3 = mask0*mask1*mask2
        distance_map[mask3] = i*2 + 1 # label the voxel with the corresponding diameter

        # Diameters
        diameters_vox.append(i*2 + 1)
        diameters_real.append((i*2 + 1)*resolution) # result in mm

        # Computations based on the comparison with the original number of void voxels
        # Cumulated volume of pores smaller than radius  (in vox)
        cum_volume_this_step_vox = tvv-np.count_nonzero(dilation)
        cum_volumes_vox.append(cum_volume_this_step_vox)
        # Cululated volume of pores smaller than radius  (in mm)
        cum_volumes_real.append(cum_volume_this_step_vox*(resolution**3))
        # Volume for this radius class
        volume_vox = cum_volumes_vox[-1]-cum_volumes_vox[-2]
        volumes_vox.append(volume_vox)
        volumes_real.append(volume_vox*(resolution**3))
        # volume fractions        
        cum_volume_fractions.append(cum_volume_this_step_vox/volume_total_vox)
        volume_fractions.append(volume_vox/volume_total_vox)
        print('Attributed voxels in this step = ', cum_volumes_vox[-1]-cum_volumes_vox[-2])

        # Same computations but based on the comparison of the last dilation
        volume_this_step = np.count_nonzero(last_dilation)- np.count_nonzero(dilation)# in vox 
        volumes_vox_from_LD.append(volume_this_step)
        cum_volumes_vox_from_LD = np.cumsum(volumes_vox_from_LD)
        last_dilation = dilation

        # saving
        # Data frame
        data = np.array([diameters_vox, diameters_real, cum_volumes_vox, cum_volumes_real, cum_volume_fractions, volumes_vox,volumes_real, volume_fractions, volumes_vox_from_LD,cum_volumes_vox_from_LD ])
        data = data.transpose()
        result = pd.DataFrame(data, columns = ['diameters_vox', 'diameters_mm', 'cum_volumes_vox', 'cum_volumes_mm3', 'cum_volume_fractions', 'volumes_vox','volumes_mm3', 'volume_fractions', 'volumes_vox_from_LD','cum_volumes_vox_from_LD'])
        output_name = 'PSD_series_of_{}_inc_{}.csv'.format(clean_name(file_name), increment)
        chdir(path_save)
        result.to_csv(output_name, sep = ";")
        
        #figure(diameters_real, volume_fractions, cum_volume_fractions, path_save, file_name )
                
        # Saving the distance map if the time limit aproaches()       
        check_time = dt.datetime.now() + 3*time_erosion_dilation        
        if (time_limit != None)*(check_time > time_end):
            print('...saving distance_map...')
            chdir(path_save)
            name =  'DistMap_PSD_series_of_{}_inc_{}.tif'.format(clean_name(file_name), increment)
            tifffile.imsave(name, distance_map)
            
        # Breaking if procedure completed
        if np.count_nonzero(erosion) == 0:
            assert (tvv == cum_volumes_vox[-1]), 'Total void voxels ({}) != Total atributed voxels({})'.format(tvv, cum_volumes_vox[-1])
            print("iteration stoped at radius = ", i)
            figure(diameters_real, volume_fractions, cum_volume_fractions, path_save, file_name,increment )
            print(np.unique(distance_map, return_counts= True))
            print('...saving distance_map...')
            chdir(path_save)
            name =  'DistMap_PSD_series_of_{}_inc_{}.tif'.format(clean_name(file_name), increment)
            tifffile.imsave(name, distance_map)
            break
        
        
    print("Finished computing PSD of {}!".format(file_name))   


def figure(diameters_real,volume_fractions,cum_volume_fractions, path_save, file_name, increment):
    
    
        # Plotting
        plt.close('all')
        fig, ax1 = plt.subplots()
        ax1.bar(diameters_real, volume_fractions,
                color= "gray",
                label = "Fraction    " ,
                width = 0.06 )
        ax2 = ax1.twinx()  
        
        ax2.plot(diameters_real, cum_volume_fractions, 
                 color= "k",
                 lw=1,
                 label = "Cummulative Fraction",
                 linestyle = '-' )
        ax1.legend(loc = 'upper left'  , bbox_to_anchor =(0.0, 1.1))
        ax2.legend(loc = 'upper right' , bbox_to_anchor =(1.0, 1.1))
        ax1.set_xlabel('Diameter (mm)' )
        ax1.set_ylabel("Fraction (% v/v)")
        ax2.set_ylabel("Cumumative Fraction (% v/v)")
        ax1.set_xlim(0)
        ax1.set_ylim(0)
        ax2.set_ylim(0,1)
        # Saving figure
        chdir(path_save)    
        title  = 'PSD_series_of_{}_inc_{}'.format(clean_name(file_name), increment)
        fig.set_size_inches(9, 7)
        fig.savefig(title + '.png', dpi = 300)
        
def clean_name(image_name):
    # Cleaning the name of the file
    name = image_name.replace(".tif","")
    name = name.replace("_ROI_Filtered_aniso", "")
    name = name.replace("Hash_Segm_of_", "")
    name = name.replace("Hashemi_Segmentation_of_", "")
    name = name.replace("SS_Profile_of_", "")
    return name
    
def relabel_the_outside(image,labels):
    """ if pixels outside the cylinder that contains the sample have the same value for air
        they will be relabeled with the value label[1]
        so, if you follow the conventions for label values
        label [0]= 1 = pixels outside the cylinder, 
        label [1]= 0 = air (i.e voids) pixels
        label [2]= 128 = fouling material
        label [3]= 255 = gravel
    """
    assert (np.count_nonzero(image == labels[0]) == 0 ), 'You must reserve label[0] = 1, for pixels outside the cylinder'
    print("relabeling from 0,128,255 to 0,1,1298255")

    # CREATING CIRCULAR MASK
    # The cylinder is supposed to be the biggest "circle" inscribed in a squared image
    z,x,y = np.shape(image)[0], np.shape(image)[1], np.shape(image)[2]
    center = int(x/2), int(y/2)
    radius = int(x/2)
    #Selecting circular ROI (region of interest)
    mask_2D = sector_mask((x,y),center,radius,(0,360 ))       
    mask_3D = np.zeros(image.shape, dtype = np.bool)
    for j in range(z):
        mask_3D[j] = mask_2D
    image[np.logical_not(mask_3D)] =  labels[0] # outside the cilinder label = 1
    print("{} pixels have been attributed to tha phase : outside the cylinder".format(np.count_nonzero(image == 1)))
    return image
    
def check_paths_and_files(path_input, path_save, image_name, Image_PSD, PSD_file, flag_resume):
    assert (os.path.isdir(path_input)) , "Folder: "    + path_input       + " IS NOT A FOLDER!"
    
    chdir (path_input)
    assert (os.path.exists(image_name)), "Scan_File: " + image_name + " DOES NOT EXIST!"
    
    assert (os.path.isdir(path_save)), "Folder: " + path_save + " IS NOT A FOLDER!"

    if flag_resume == True:
        chdir (path_save)
        assert (os.path.exists(Image_PSD)), "File: " + Image_PSD + " DOES NOT EXIST!"
        assert (os.path.exists(PSD_file)), "File: " + PSD_file + " DOES NOT EXIST!"
    

def sector_mask(shape,centre,radius,angle_range):
    """
    CODE FROM STACK OVER FLOW
    Return a boolean mask for a circular sector. The start/stop angles in  
    `angle_range` should be given in clockwise order.
    """

    x,y = np.ogrid[:shape[0],:shape[1]]
    cx,cy = centre
    tmin,tmax = np.deg2rad(angle_range)

    # ensure stop angle > start angle
    if tmax < tmin:
            tmax += 2*np.pi

    # convert cartesian --> polar coordinates
    r2 = (x-cx)*(x-cx) + (y-cy)*(y-cy)
    theta = np.arctan2(x-cx,y-cy) - tmin

    # wrap angles between 0 and 2*pi
    theta %= (2*np.pi)

    # circular mask
    circmask = r2 <= radius*radius

    # angular mask
    anglemask = theta <= (tmax-tmin)

    return circmask*anglemask

def decorateur_timeit(a_function, path_save, file_name, increment):
    def wrapper (*args, **kwargs):
        print(*args)
        start = start = dt.datetime.now()
        res = a_function(*args, **kwargs)
        end = dt.datetime.now()

        chdir(path_save)
        file = open("time_report_PSD_series_{}_inc_{}.txt".format(file_name.replace('.tif', ""), increment), 'w')
        file.write(" Start:\n" )
        file.write(start.strftime('%c'))
        file.write("\n End: \n" )
        file.write(end.strftime('%c'))
        file.write("\n Duration: \n")
        file.write(str(end-start))
        file.close()
    return wrapper
        
def main():
    # Directory input
    path_input  = "/home/german.martinez-carvajal/Desktop/These/Pore_Size_Distribution/Test_cylindre"
    # Directory output    
    path_save  = "/home/german.martinez-carvajal/Desktop/These/Pore_Size_Distribution/Test_cylindre"
    # file name of the tomograpĥy after segmentation
    file_name = 'cylindre15.tif'
    
    flag_resume = False 
    # read preavious results in consequense
    # which are located already in the path_save durectory
    Image_PSD = "DistMap_PSD_series_of_cylindre15_inc_1.tif"
    PSD_file = "PSD_series_of_cylindre15_inc_1.csv"
    
    # checking
    check_paths_and_files(path_input, path_save, file_name, Image_PSD, PSD_file, flag_resume)

    # Resolution
    resolution = 1 #mm/vox
    # Relabel ? (see relabel function)
    flag_relabel = False     
    # Labels of the phases 
    labels = [1,0,128,255] # 1 = outside the cylinder, 0 = voids, 128 = fouling material, 255 = gravel
    # time_limit
    time_limit = dt.timedelta(hours = 24)
    # increment in vox
    increment = 1

    # assembling all parameters
    params = []
    params.append(flag_resume)
    params.append(path_input)
    params.append(path_save)
    params.append(file_name)
    params.append(resolution)
    params.append(labels)
    params.append(flag_relabel)
    params.append(time_limit)
    params.append(Image_PSD)
    params.append(PSD_file)
    params.append(increment)
    # calling the function
    # Computing
    f = decorateur_timeit(compute_pore_size_distribution, path_save, file_name,increment)
    f(*params)


        
        
main()