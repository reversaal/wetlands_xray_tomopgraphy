# -*- coding: utf-8 -*-
"""
Created on Wed May  8 20:41:55 2019

@author: german.martinez-carvajal
"""

import pandas as pd
import numpy as np
from os import chdir
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams.update({'font.size': 25}) 


def mov_av_window (A,n):
    A = np.array(A)
    extended = np.concatenate(([A[0]]*n, A, [A[-1]]*n))
    smooth   = []
    for i in range(n,len(A)+n):
        smooth.append(np.mean(extended [i-n:i+n+1]))
    return np.array(smooth)
    
def compute_eg(data, path, num_phases, file):
    # keys corresponding to volume fractions of each phase   
    if num_phases == 3:
        VFS = ['VF_Voids','VF_FM', 'VF_Grv']
    elif num_phases == 4:
        VFS = ['VF_Voids','VF_FM', 'VF_Grv', 'VF_Reeds', 'VF_Voids*','VF_FM*', 'VF_Grv*', 'VF_Reeds*' ]
    
    # column for deltas
    data['delta_rel'] = np.diff(data.Rel_Box_Lengths, prepend = np.nan)
    data['delta_rel'] = data['delta_rel'].fillna(method = 'bfill')
    
    for VF in VFS:
        # eg = (a/b)/d
        
        # extending the borders of the array
        global VF_ext, a
        VF_ext = np.append(data[VF][0], data[VF])
        VF_ext = np.append(VF_ext, data[VF][len(data[VF])-1])
        
        l = len(data)            
        a, b = np.zeros(l) , np.zeros(l)
        
        for i in range(1,len(VF_ext)-1):
            a[i-1] = abs(VF_ext[i+1]-VF_ext[i-1])
            b[i-1] = abs(VF_ext[i+1]+VF_ext[i-1])
        
        d = data['delta_rel']
        key = 'eg_{}_rel'.format(VF)
        data[key] = ((a)/(b))/d
        
    chdir(path)
    data.to_csv(file.replace(".csv",'_eg.csv'), sep = ';')
    return data

def figure_eg(data_eg, path, num_phases, file, z_slice, resolution):
    plt.close('all')
    # a figure per slice
    # one line per phase
    
    if num_phases == 3:
        VFS = ['VF_Voids','VF_FM', 'VF_Grv']
    elif num_phases == 4:
        VFS = ['VF_Voids','VF_FM', 'VF_Grv', 'VF_Reeds']

    color_dict = {'VF_Voids':(145/255,0,210/255), 'VF_FM':(240/255,79/255,0), 'VF_Grv':(255/255, 206/255,0), 'VF_Reeds':'g'}
    color_dict = {'VF_Voids':'blue', 'VF_FM':'red', 'VF_Grv':'black', 'VF_Reeds':'g'}  
    
    fig, ax = plt.subplots()
    for VF in VFS:
        key = 'eg_{}_rel'.format(VF)
        y = data_eg[key]
        y = y.fillna(method = 'bfill') 
        y = y.fillna(method = 'ffill')
        y = mov_av_window(y,25)
        ax.plot(data_eg.Rel_Box_Lengths, y, label = VF.replace('VF_',""), color = color_dict[VF], lw = 1)
    
    sup = 'Depth = {}mm'.format(int(-z_slice*resolution))
    ax.set_title(sup)
    ax.set_xlim(0,1)
    ax.set_ylim(0,1.6)
    
    L_max = max(data_eg.Box_Lengths)    
    annotation = r"(d$_{max}$" + "= {}mm)".format(int(L_max*resolution))
    plt.xlabel('Relative diameter ' + annotation)
    ax.set_ylabel(r'$\varepsilon_{g}$')
    ax.grid(axis = 'both', which = 'both')
    ax.minorticks_on()
    fig.set_size_inches(11,11)
    ax.legend(loc = 'best')
    ax.axhline(y=0.2, xmin=0.0, xmax=1.0, color='black', linestyle = '--', lw = '2')
    chdir(path)
    title = "figure_eg_{}.png".format(file.replace('.csv', ""))
    fig.savefig(title, dpi = 300)        

def figure_eg_exc(data_eg, path, file, z_slice, resolution):
    plt.close('all')
    # a figure per slice
    # one line per phase
    
    VFS = ['VF_Voids*','VF_FM*', 'VF_Grv*', 'VF_Reeds*']

    color_dict = {'VF_Voids*':(145/255,0,210/255), 'VF_FM*':(240/255,79/255,0), 'VF_Grv*':(255/255, 206/255,0), 'VF_Reeds*':'g'}
   
    fig, ax = plt.subplots()
    for VF in VFS:
        key = 'eg_{}_rel'.format(VF)
        y = data_eg[key]
        y = y.fillna(method = 'bfill') 
        y = y.fillna(method = 'ffill')
        y = mov_av_window(y,25)
        ax.plot(data_eg.Rel_Box_Lengths, y, label = VF.replace('VF_',""), color = color_dict[VF], lw = 1)
    
    sup = 'Depth = {}mm'.format(int(-z_slice*resolution))
    ax.set_title(sup)
    ax.set_xlim(0,1)
    ax.set_ylim(0,2)
    
    L_max = max(data_eg.Box_Lengths)    
    annotation = r"(d$_{max}$" + "= {}mm)".format(int(L_max*resolution))
    plt.xlabel('Relative diameter ' + annotation)
    ax.set_ylabel(r'$\varepsilon_{g}$')
    ax.grid(axis = 'both', which = 'both')
    ax.minorticks_on()
    fig.set_size_inches(11,11)
    ax.legend(loc = 'best')
    ax.axhline(y=0.2, xmin=0.0, xmax=1.0, color='k', linestyle = '--')
    chdir(path)
    title = "figure_eg_exc_{}.png".format(file.replace('.csv', ""))
    fig.savefig(title, dpi = 300)     

def main():
    path1 = '/home/german.martinez-carvajal/Desktop/These/VER_3phases/MON1A_20180723/Centered'
    path2 = '/home/german.martinez-carvajal/Desktop/These/VER_4phases/MON1A_20180723/centered'
    
    files = ['data_VER_centered_circle_slice_170.csv']
    files.append('data_VER_centered_circle_slice_675.csv')
    files.append('data_VER_centered_circle_slice_1450.csv')
    files.append('data_VER_centered_circle_slice_2600.csv')
    files.append('data_VER_centered_circle_slice_3700.csv')
    files.append('data_VER_4phases_centered_circle_slice_675.csv')
    files.append('data_VER_4phases_centered_circle_slice_3700.csv')
    
    z_slices = [170,675,1450,2600,3700,675,3700]
    resolution = 0.035 #mm/vox
    
    assert (len(z_slices) == len(files))
    # loop over files
    for i in range(len(files)):
        file = files[i]
        z_slice = z_slices[i]
        path = path2 if '4phases' in file else path1
        num_phases = 4 if '4phases' in file else 3
        
        # read data for each file
        chdir(path)
        data = pd.read_csv(file, sep = ';')
        
        # compute
        data_eg = compute_eg(data, path, num_phases, file)
                
        if num_phases == 3:
            figure_eg(data_eg, path, num_phases, file, z_slice, resolution)
            
        if num_phases == 4:
            # figure in base excenta de VF_Reeds
            figure_eg_exc(data_eg, path, file, z_slice, resolution)
        print('main routine finished for file {}'.format(file))
    print('FINISHED')
main()
