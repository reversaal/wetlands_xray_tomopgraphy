import tifffile
import os
import numpy as np
import pandas as pd
import scipy.ndimage 
from matplotlib import pyplot as plt
from sys import getsizeof
from os import chdir 

def sector_mask(shape,centre,radius,angle_range):
    """
    CODE FROM STACK OVER FLOW
    Return a boolean mask for a circular sector. The start/stop angles in  
    `angle_range` should be given in clockwise order.
    """

    x,y = np.ogrid[:shape[0],:shape[1]]
    cx,cy = centre
    tmin,tmax = np.deg2rad(angle_range)

    # ensure stop angle > start angle
    if tmax < tmin:
            tmax += 2*np.pi

    # convert cartesian --> polar coordinates
    r2 = (x-cx)*(x-cx) + (y-cy)*(y-cy)
    theta = np.arctan2(x-cx,y-cy) - tmin

    # wrap angles between 0 and 2*pi
    theta %= (2*np.pi)

    # circular mask
    circmask = r2 < radius*radius

    # angular mask
    anglemask = theta <= (tmax-tmin)

    return circmask*anglemask
    
def SurfaceCouchyCrofton_2(Image, Resolution, p2, Surface_of_p1):

    global  Progress   
    assert(Image.shape == Surface_of_p1.shape)
    
    assert(p2 != 0), 'arbitrarily, the value of phase 2 (p2) MUST be different from 0, CHANGE the labels from your original image'
    
    # Relabeling
    num_sur_voxels = np.count_nonzero(Surface_of_p1 == 255)   

    # An eveloping ROI box is needed
    Shape = np.array(Image.shape) + 2
    ROI   = np.zeros(Shape, dtype = np.uint8)
    ROI_surface = np.zeros(Shape, dtype = np.uint8)

    # Filing ROI with Image Values
    ROI[1:-1,1:-1,1:-1] = Image
    ROI_surface[1:-1,1:-1,1:-1] = Surface_of_p1
 
   # Saving coordinates of every element in phase 1
    print('Getting Coordinates with numpy.where')
    Coordinates = np.where(ROI_surface == 255)

    # Retrieving memory
    del Image, Surface_of_p1, ROI_surface
    
    # Saving coordinates into a panda frame
    Dict             = {'z':Coordinates[0], 'x':Coordinates[1],'y':Coordinates[2]} 
    FrameCoord       = pd.DataFrame(Dict)
    Size_FrameCoord  = getsizeof(FrameCoord) /2**20 #  in MB
    Max = len(FrameCoord)
    
    assert num_sur_voxels != 0 , 'No surface pixels'
    #print('# Evaluations to perform = ', Max, num_sur_voxels )    
    
    # Some counters are needed
    # Connectivity = root(1) neighbors
    Family1 = 0
    NeighborsF1 = np.zeros(6)
    # Connectivity = root(2) neighbors
    Family2 = 0
    NeighborsF2 = np.zeros(12)
    # Connectivity = root(3) neighbors
    Family3 = 0
    NeighborsF3 = np.zeros(8)
    
 
        
    print('Computing contributions...')
    for index in FrameCoord.index :
        z = FrameCoord.loc[index,'z']
        x = FrameCoord.loc[index,'x']
        y = FrameCoord.loc[index,'y']
        # Evaluating neighbors with connectivity = root(1)     
        NeighborsF1[0]  = (ROI[z+1, x  , y  ] == p2)
        NeighborsF1[1]  = (ROI[z  , x+1, y  ] == p2)
        NeighborsF1[2]  = (ROI[z  , x  , y+1] == p2)
        NeighborsF1[3]  = (ROI[z-1, x  , y  ] == p2)
        NeighborsF1[4]  = (ROI[z  , x-1, y  ] == p2)
        NeighborsF1[5]  = (ROI[z  , x  , y-1] == p2)
        Family1        += np.sum(NeighborsF1)
        # Evaluating neighbors with connectivity = root(2)    
        NeighborsF2[0]  = (ROI[z  , x+1, y+1] == p2)
        NeighborsF2[1]  = (ROI[z  , x-1, y+1] == p2)
        NeighborsF2[2]  = (ROI[z  , x+1, y-1] == p2)
        NeighborsF2[3]  = (ROI[z  , x-1, y-1] == p2)
        NeighborsF2[4]  = (ROI[z+1, x  , y-1] == p2)
        NeighborsF2[5]  = (ROI[z+1, x  , y+1] == p2)
        NeighborsF2[6]  = (ROI[z+1, x+1, y  ] == p2)
        NeighborsF2[7]  = (ROI[z+1, x-1, y  ] == p2)
        NeighborsF2[8]  = (ROI[z-1, x  , y-1] == p2)
        NeighborsF2[9]  = (ROI[z-1, x  , y+1] == p2)
        NeighborsF2[10] = (ROI[z-1, x+1, y  ] == p2)
        NeighborsF2[11] = (ROI[z-1, x-1, y  ] == p2)
        Family2        += np.sum(NeighborsF2)
        # Evaluating neighbors with connectivity = root(3)
        NeighborsF3[0]  = (ROI[z+1, x+1, y+1] == p2)
        NeighborsF3[1]  = (ROI[z+1, x-1, y+1] == p2)
        NeighborsF3[2]  = (ROI[z+1, x+1, y-1] == p2)
        NeighborsF3[3]  = (ROI[z+1, x-1, y-1] == p2)
        NeighborsF3[4]  = (ROI[z-1, x+1, y+1] == p2)
        NeighborsF3[5]  = (ROI[z-1, x-1, y+1] == p2)
        NeighborsF3[6]  = (ROI[z-1, x+1, y-1] == p2)
        NeighborsF3[7]  = (ROI[z-1, x-1, y-1] == p2)
        Family3        += np.sum(NeighborsF3)
        
               
        if index % int(Max/100) == 0:
            Progress = str(int(index/Max*100)) + '%'
    
    # This are the discretization weight associated to directions
    # taken from the Osher mucklish book
    VoronoiFactor1 = 0.045777895 
    VoronoiFactor2 = 0.0369806261       
    VoronoiFactor3 = 0.035195639
    
    # Surface of the cuting element
    Lambda_1 = np.sqrt(1)/(Resolution**2)
    Lambda_2 = np.sqrt(2)/(Resolution**2)
    Lambda_3 = np.sqrt(3)/(Resolution**2)
    
    # Computing family contributions
    CC1 = VoronoiFactor1/Lambda_1 * Family1
    CC2 = VoronoiFactor2/Lambda_2 * Family2
    CC3 = VoronoiFactor3/Lambda_3 * Family3
    
    # Computing the surface
    Surface = 4*(CC1 + CC2 + CC3)
    print('Computation of contributions finished')
    
    # check
    #assert Family1 + Family2 + Family3 !=0, 'No contribution computed'
    
    return (Surface, Max, (Family1, Family2, Family3))
#################################################

def relabel_the_outside(image,label_outside, rw):
    """ if pixels outside the cylinder that contains the sample have the same value for air
        they will be relabeled with the value label[1]
        so, if you follow the conventions for label values
        label [0]= 1 = pixels outside the cylinder, 
        label [1]= 0 = air (i.e voids) pixels
        label [2]= 128 = fouling material
        label [3]= 255 = gravel
    """
    assert (np.count_nonzero(image == label_outside) == 0 ), 'You must reserve label[0] = 1, for pixels outside the cylinder'
    print("relabeling from 0,128,255 to 0,1,129, 255")

    # CREATING CIRCULAR MASK
    # The cylinder is supposed to be the biggest "circle" inscribed in a squared image
    z,x,y = np.shape(image)[0], np.shape(image)[1], np.shape(image)[2]
    center = int(x/2), int(y/2)
    radius = int(x/2)*rw
    #Selecting circular ROI (region of interest)
    mask_2D = sector_mask((x,y),center,radius,(0,360 ))       
    mask_3D = np.zeros(image.shape, dtype = np.bool)
    for j in range(z):
        mask_3D[j] = mask_2D
    image[np.logical_not(mask_3D)] =  label_outside # outside the cilinder label = 1
    print("{} pixels have been attributed to the phase : outside the cylinder".format(np.count_nonzero(image == 1)))
    return image

def check_paths_and_files(path, path_save, file, file_SV, flag_SV ):
    assert (os.path.isdir(path)) , "Folder: "    + path       + " IS NOT A FOLDER!"
   
    chdir (path)
    assert (os.path.exists(file)), "Scan_File: " + file + " DOES NOT EXIST!"

    assert (os.path.isdir(path_save))        , "Folder: "    + path_save       + " IS NOT A FOLDER!"

    chdir(path)    
    if flag_SV == False:
        assert (os.path.exists(file_SV)), "SV_file {} DOES NOT EXIST!".format(file_SV)


def extract_surface_voxels(image, phase1, file_name):
    global list_messages_out
        # Identifying surface voxels
    print('Identifying pixels from phase1')
    # First select one of the phases
    
    print('# of pixels Phase 1 = {}'.format(np.count_nonzero(image==phase1)))
    value = 255
    
    # phase of interest
    whole_phase = np.array((image == phase1)*value, dtype = np.uint8)

    # then perform a mean filter
    convolved = scipy.ndimage.convolve(whole_phase, weights = np.full(shape = (3,3,3), fill_value = 1.0/27) )
    message = 'convolution (surface voxels extraction) succesful'
    list_messages_out = add_message(message, list_messages_out, path_save, file_name)
    
    # if after mean filter pixel the value is still the same -> then they are inner pixels
    print('# of inner pixels Phase 1 = {}'.format(np.count_nonzero(convolved==value)))    
    assert(np.count_nonzero(convolved==value)!=0), "Nummer of inner pixels = 0, bug in the extraction of surface voxels"
    
    # in the other case they are surface pixels
    surface = (convolved != value)*(image == phase1)
    print('# of surface pixels Phase 1 = {}'.format(np.count_nonzero(surface))) 
    
    # saving    
    surface = np.array(surface, dtype = np.uint8)*255
    chdir(path_input)
    tifffile.imsave('surface_pixels_of_{}.tif'.format(sample_name),surface)
    
    # saving last message
    message = 'extraction of surface voxels succesful'
    list_messages_out = add_message(message, list_messages_out, path_save, file_name)
    
    return(surface)

def clean_name(image_name):
    # Cleaning the name of the file
    name = image_name.replace(".tif","")
    name = name.replace("_ROI_Filtered_aniso", "")
    name = name.replace("Hash_Segm_of_", "")
    name = name.replace("Hashemi_Segmentation_of_", "")
    name = name.replace("SS_Profile_of_", "")
    return name

def plot_ssp(path, file):
    chdir(path)
    data = pd.read_csv(file, sep = ';' )
    plt.close('all')
    figure, ax = plt.subplots()
    ax.plot(data['SS (mm-1) / FM'], data['Depth(mm)'], linewidth = 1, linestyle = '-', color = 'red', label = "per FM volume")
    ax.plot(data['SS (mm-1) / Bulk'], data['Depth(mm)'], linewidth = 1, linestyle = '-', color = 'purple', label = "per bulk volume")
    ax.plot(data['SS (mm-1) / Voids'], data['Depth(mm)'], linewidth = 1, linestyle = '-', color = 'blue', label = "per voids volume")
    ax.plot(data['SS (mm-1) / Voids + FM'], data['Depth(mm)'], linewidth = 1, linestyle = '-', color = 'black', label = "per voids + FM volume")
 
    ax.set_xlim(0)
    ax.legend(loc = 'best')
    plt.xlabel(r'Specific Surface $mm^{2}/mm^{3}$') 
    plt.ylabel('Depth (mm)')
    plt.xlim(0)
    plt.ylim(min(data['Depth(mm)']),0)
    figure.savefig(file.replace('csv','png'))

def add_message(message, list_messages_output, path_save, file_name):
    list_messages_output.append(message)
    frame = pd.DataFrame({'Message': list_messages_output})
    chdir(path_save)
    name = 'messages_SSP_{}.csv'.format(sample_name)
    frame.to_csv(name)
    return(list_messages_output)
    
def compute_specific_surface_profile(start_VS, path_input, path_save, file_name, sample_name, label_outside, p1, p2, size_VS, resolution, flag_relabel=False, flag_SV=True, file_SV=None, rw = 1):
    """
    The computation works like this:
    The file (i.e. the image (file_name)) may have different labels for different phases 
    By convention (0 = air, 128 = fouling material, gravel = 255)
    p1 is the label for phase 1, p2 is the label for phase2        
    
    The surface to be computed corresponds to phases with values p1 and p2, that must be contained in the list "labels"
    Example, if lables = (0. 128) then the surface between phases labeled 0 and 128 will be computed 

    The image is divided into vertical sections (VS) of lenght = size_VS (in voxels) 
    This means that the number of VS the number depends on the total number of z-slices of the image, approximately Num_VS = len(image)/size_VS
    Then, for each VS a computation of the specific surface is done (using couchy crofton function)
    a larger size_VS will give a smoother specific surface profile
    
    input resolution, float, units; mm/vox
    """
    
    # Saving check messages
    global list_messages_out    
    list_messages_out = []

    print      ('Reading Image...')
    chdir      (path_input)
    image      = tifffile.imread(file_name)
    
    list_messages_out = add_message('Reading succesful', list_messages_out, path_save, file_name)
    
        
    # extracting surface pixels from phase1 (p1)
    if flag_SV == True:
        print("Extracting surface voxels...")
        SurfaceVoxels = extract_surface_voxels(image, p1, file_name)
    else:
        print("Reading surface voxels...")
        chdir(path_input)
        SurfaceVoxels = tifffile.imread(file_SV)
    
    assert SurfaceVoxels.shape == image.shape
    # Relabel (to avoid pixels outside the cylinder)
    if flag_relabel == True:
        chdir(path_input)
        image = relabel_the_outside(image, label_outside, rw)    
        
    # computing indexes for each vertical section 
    lenght     = len(image)
    limits_of_VS     = np.arange(0,lenght+size_VS,size_VS) # Limits stores the index of the boxes where a profile will be performed
    
    # Initilializing the variables that will be computed for each section
    # Volumes
    VolumesOrM = list() 
    VolumesAir = list()
    Volumes = list() # sum of air, FM and gravel
    # Surface
    SurfAir_OrM= list()
    # Specific surfaces (depending "volume" you will use to divide)    
    SpcSrfcs_1   = list() # Voids-FM surface / FM volume
    SpcSrfcs_2  = list() # Voids-FM surface / air volume
    SpcSrfcs_3 = list() # Voids-FM surface / volume air and FM volume
    SpcSrfcs_4 = list() # Voids-FM surface / volume air, FM, and gravel volume
    # Depth
    Depth = list()
    # Count of voxels located at the surface
    num_sur_voxs = list()
    # Contributions of each family in the couchy crofton's method    
    F1s = list()
    F2s = list()
    F3s = list()

    # Computing the specific surface small sections with thickness = SizeBox
    for i in range(start_VS,len(limits_of_VS)-1):

        # Saving messages
        message = 'Computing profile, step {}/{}'.format(i+1,len(limits_of_VS)-1 )
        list_messages_out = add_message(message, list_messages_out, path_save, file_name)
        print('Step: {}/{}'.format(i+1,len(limits_of_VS)-1 ))
        
        # Moving the window downwars to perform the computations
        Selection1 = image        [limits_of_VS[i]:limits_of_VS[i+1]].copy()
        Selection2 = SurfaceVoxels[limits_of_VS[i]:limits_of_VS[i+1]].copy()

        # Volume of Fouling Material in the section 
        VolumeOrM  = np.count_nonzero(Selection1 == p2)
        VolumeOrM  = VolumeOrM*resolution**3 # in mm3
        #print('FM Volume of section =', VolumeOrM)
        
        # Volume of air in the section
        VolumeAir  = np.count_nonzero(Selection1 == p1)
        VolumeAir  = VolumeAir*resolution**3 # in mm3
                
        # volume of the section (take out pixels outside the cylinder)
        Volume = np.count_nonzero(Selection1 != label_outside ) * resolution**3
        Volume1 = (np.count_nonzero(Selection1 == 0) + np.count_nonzero(Selection1 == 128) + np.count_nonzero(Selection1 == 255))*resolution**3
        assert Volume == Volume1
                
        Surface, num_sur_vox, (F1, F2, F3)    = SurfaceCouchyCrofton_2(Selection1, resolution, p2, Selection2)
        #print('Surface of section (mm2)=', Surface)
        
        # Storing data
        # Volumes
        VolumesOrM.append(VolumeOrM)
        VolumesAir.append(VolumeAir)
        Volumes.append(Volume)
        # Suface        
        SurfAir_OrM.append(Surface)
        # Specific surfaces
        SpcSrfcs_1.append(Surface/VolumeOrM)
        SpcSrfcs_2.append(Surface/VolumeAir)
        SpcSrfcs_3.append(Surface/(VolumeOrM+VolumeAir))
        SpcSrfcs_4.append(Surface/(Volume))
        # Count of surface voxels and contributions
        num_sur_voxs.append(num_sur_vox)
        F1s.append(F1)
        F2s.append(F2)
        F3s.append(F3)
        # Depth
        Depth.append(limits_of_VS[i]*(-1)*(resolution)) # in mm
        
        print('value SpecSurf / FM volume',limits_of_VS[i],'-',limits_of_VS[i+1], '=', Surface/VolumeOrM)

        # Save and plot (little by little)      
        chdir(path_save)
        Frame = pd.DataFrame()
        Frame['Depth(mm)'] = Depth
        Frame['Surf Voids/OrM (mm^2)']= SurfAir_OrM
        Frame['Volumes FM (mm^3)'] = VolumesOrM
        Frame['Volumes Voids (mm^3)'] = VolumesAir
        Frame['Volumes Voids + FM (mm^3)'] = np.array(VolumesAir) + np.array(VolumesOrM)
        Frame['Volumes Bulk (mm^3)'] = Volumes
        Frame['SS (mm-1) / FM'] = SpcSrfcs_1
        Frame['SS (mm-1) / Voids'] = SpcSrfcs_2
        Frame['SS (mm-1) / Voids + FM'] = SpcSrfcs_3
        Frame['SS (mm-1) / Bulk'] = SpcSrfcs_4
        Frame['Count_Sur_Vox'] = num_sur_voxs
        Frame['F1'] = F1s
        Frame['F2'] = F2s
        Frame['F3'] = F3s
        Name = 'SS_Profile_of_{}_rw_{}.csv'.format(sample_name, rw)
        Frame.to_csv(Name, sep = ';')
        plot_ssp(path_save, Name)
        
    message  = 'Finished'
    list_messages_out = add_message(message, list_messages_out, path_save, file_name)
    
    print('FINISHED')

def main():
    global path_input, path_save, size_VS, start_VS, sample_name

    # Directory input
    path_input ='/home/german.martinez-carvajal/Desktop/These/Hashemi_segmentation/Tests/Test9_Rapide/Results/heavy_results'
    
    # Directory outputs
    path_save = "/home/german.martinez-carvajal/Desktop/These/Specific_Surface_Profile/test"
    
    # Image Name
    file_name = "Hash_Segm_of_Image_Filtered_PVE_50_Tol_0.75_NumVS_1.tif"
    sample_name = "MONxx_part2"
    # flag to reag SV file (surface voxels file) if already identified
    # Important : surface pixels are labeled 255
    flag_SV = True # True - Compute surface voxels /// False - read existing image with surface voxels
    file_SV = ""
    
    # Constant parameters    
    # Image resolution
    resolution        = 0.035 #mm per vox
    
    # Labels of the phases p1 is ALWAYS the label for air, and p2 is ALWAYS the label for OrM (Organig matter or Fouling Material)
    label_outside = 1 # 1 = outside the cylinder, 0 = voids, 128 = fouling material, 255 = gravel
    p1 = 0 # computation will be done for interface between phases p1 and p2
    p2 = 128
    
    # Relabel ? (see relabel function)
    flag_relabel = True 
    rw = 1.0
    
    # size_VS
    size_VS =  2    # pixels (the same lenght of the smoothing for volume fraction profiles)
    
    # start at vertical section #...
    # useful if you want to resume the computation from a certain point
    start_VS = 0

    # cheking
    check_paths_and_files(path_input, path_save, file_name, file_SV, flag_SV)

    # running
    print('computing for file {}'.format(file_name))
    params = []
    params.append(start_VS)
    params.append(path_input)
    params.append(path_save)
    params.append(file_name)
    params.append(sample_name)
    params.append(label_outside)
    params.append(p1)
    params.append(p2)
    params.append(size_VS)
    params.append(resolution)
    params.append(flag_relabel)
    params.append(flag_SV)
    params.append(file_SV)
    params.append(rw)
    compute_specific_surface_profile(*params)

main()