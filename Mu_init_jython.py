from ij import IJ, ImagePlus
from ij.measure import Measurements
from ij.gui import HistogramWindow

def mu_init (Num_VS, Dir, File_Name):
	# Open the file
	arg = "open=" + Dir + "/" + File_Name
	IJ.run("TIFF Virtual Stack...", arg);
	File = IJ.getImage()
	
	# Image Type
	types = {ImagePlus.GRAY8 : "8-bit",  
	         ImagePlus.GRAY16 : "16-bit",  
	         ImagePlus.GRAY32 : "32-bit",  
	         ImagePlus.COLOR_256 : "8-bit color",
	         ImagePlus.COLOR_RGB : "RGB"}
	print(types[0],types[1],types[2],types[3], types[4])
	assert ((File.type in types, 'Image should be only in 8-bit or 16-bit format') ) 
	File_type = types[File.type]
	print('Types',File.type, File_type)
	
	# Max gray value
	if File_type == '16-bit':
		Max_gray_value = 2**16-1
	elif File_type == '8-bit':
		Max_gray_value = 2**16
		
	# Num_Slices 
	Num_Slices = File.getNSlices()
	
	# Creating output_file
	output_file_name = 'Mu_init_of_' + File_Name[:-4] + ' Num_VS_' + str(Num_VS) + '.txt'
	output_file = open (Dir + "/" + output_file_name, 'w')
	output_file.write('[Num_VS, mu1, mu2, mu3]\n')
	
	# Duplicate Loop
	for i in range(0,Num_VS):
		print ('Section= ', i)
		# Duplicating a section,
		title = "title=Section_" + str(i)
		Range = "range={}-{}".format(1+i*Num_Slices/Num_VS, (i+1)*Num_Slices/Num_VS)
		print('Range = ',Range)
		arg = title + " duplicate " + Range
		IJ.run(File, "Duplicate...", arg);
		Section = IJ.getImage()
		
		# Computing histogram of the section
		stats = Section.getStatistics()
		hist = stats.histogram
		len_hist = len(hist)
		Max_gray_value_real = stats.max
		print('Max=',Max_gray_value_real)
		
		# saving histogram's image 
		hist_win = HistogramWindow(Section)
		plotimage = hist_win.getImagePlus()
		IJ.save(plotimage, Dir  + '/' + "Hist_of_section_{}.png".format(i)) 
		hist_win.close()
		
		# Finding values for Mu initialization
		#   First find maximul peak heights
		A1 = max(hist[                1:int(len_hist*1/4)])
		A2 = max(hist[int(len_hist*1/4):int(len_hist*2/4)])
		A3 = max(hist[int(len_hist*2/4):-1])
		#   Then find their index
		index1 = hist.index(A1)
		index2 = hist.index(A2)
		index3 = hist.index(A3)
		#   Then find the corresponding gray vale
		mu1 = float(index1)/len_hist*Max_gray_value_real
		mu2 = float(index2)/len_hist*Max_gray_value_real
		mu3 = float(index3)/len_hist*Max_gray_value_real
		#   Write mu1, mu2 and mu3 in a text file
		output_file.write('[{}, {}, {}, {}]\n'.format(i+1,mu1,mu2,mu3))
		print(index1,len_hist, Max_gray_value_real, mu1)
		print(index2,len_hist, Max_gray_value_real, mu2)
		print(index3,len_hist, Max_gray_value_real, mu3)
		# Close the current vertical section
		Section.close()
		break
	File.close()
	output_file.close()
	print('Finished')

# MAIN
# Paths
Dir_list = []
Dir_list.append('/home/german.martinez-carvajal/Desktop/These/Hashemi_segmentation/Tomos_2018/Manip_juillet/MON1A_20180723')
Dir_list.append('/home/german.martinez-carvajal/Desktop/These/Hashemi_segmentation/Tomos_2018/Manip_juillet/MON1B_20180723')
Dir_list.append('/home/german.martinez-carvajal/Desktop/These/Hashemi_segmentation/Tomos_2018/Manip_juillet/MON2A_20180723')
Dir_list.append('/home/german.martinez-carvajal/Desktop/These/Hashemi_segmentation/Tomos_2018/Manip_juillet/MON2B_20180723')
Dir_list.append('/home/german.martinez-carvajal/Desktop/These/Hashemi_segmentation/Tomos_2018/Manip_juillet/MON3A_20180723')
Dir_list.append('/home/german.martinez-carvajal/Desktop/These/Hashemi_segmentation/Tomos_2018/Manip_juillet/MON3B_20180723')
# Names of the files
File_Names_list = []
File_Names_list.append('MON1A_20180723_ROI_Filtered_aniso.tif')
File_Names_list.append('MON1B_20180723_ROI_Filtered_aniso.tif')
File_Names_list.append('MON2A_20180723_ROI_Filtered_aniso.tif')
File_Names_list.append('MON2B_20180723_ROI_Filtered_aniso.tif')
File_Names_list.append('MON3A_20180723_ROI_Filtered_aniso.tif')
File_Names_list.append('MON3B_20180723_ROI_Filtered_aniso.tif')
# RUN
for i in range(len(Dir_list)):
	Num_VS = 4
	Dir, File_Name = Dir_list[i], File_Names_list[i]
	print('Treating file {}'.format(File_Name))
	mu_init (Num_VS, Dir, File_Name)
	
	
