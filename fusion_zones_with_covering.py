# -*- coding: utf-8 -*-
"""
Created on Thu Jun  6 17:45:39 2019

@author: german.martinez-carvajal
"""

import tifffile
from os import chdir
import numpy as np

path_original_image = '/home/german.martinez-carvajal/Bureau/Tests_convering_cut_fusion'
chdir(path_original_image)
file_name = 'Segm.tif'
original_image = tifffile.imread(file_name)

path_of_sections = '/home/german.martinez-carvajal/Bureau/Tests_convering_cut_fusion'
chdir(path_of_sections)
num_sections = 2
covering_size = 1
for i in range(num_sections):
    section_name = 'DistMap_FFT_of_Segm_section_{}_inc_1.tif'.format(i)
    section = tifffile.imread(section_name)
    print(section.shape)
    if i == 0:
        fusion = section[0:-covering_size]
    elif i == num_sections-1:
        fusion = np.append(fusion, section[covering_size:], axis = 0)
    else:
        fusion = np.append(fusion, section[covering_size:-covering_size], axis = 0)
tifffile.imsave('DistMap_FFT_fusion_of_Segm.tif', fusion)        
print('checking shapes are equal ?', original_image.shape,fusion.shape )    
assert original_image.shape == fusion.shape

    
    