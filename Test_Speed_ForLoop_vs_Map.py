import numpy as np
import pandas as pd
from   numpy.random import randint as randint
import datetime

number_radii  = 20
number_boxes  = 20
size = (10,10)

def create_list_of_boxes():
    print('Creating list of voxels')
    global list_of_boxes
    start    = datetime.datetime.today()
    A = randint(0,3, size = size, dtype = np.uint8) 
    list_of_boxes = [[A for j in range (number_boxes)] for i in range (number_radii)]
    end      = datetime.datetime.today()
    duration = end - start
    duration = str(duration)
    print('Boxes CreationV1 - as a list of lists')
    print('(microseconds): ', duration)

def create_list_of_boxesV2():
    global Frame
    start    = datetime.datetime.today()
    Radii      = [] 
    Box_Number = []
    Boxes      = []
    A = randint(0,3, size = size, dtype = np.uint8) 
    for Radius in range(number_radii):
        for Box in range(number_boxes):
            Radii     .append(Radius)
            Box_Number.append(Box)
            Boxes     .append(A)
    Frame = pd.DataFrame({'Radii':Radii, 'Box_Number':Box_Number,'Boxes':Boxes})
    end      = datetime.datetime.today()    
    duration = end - start
    duration = str(duration)
    print('Boxes CreationV2 - as a panda frame passing through a for')
    print('(microseconds): ', duration)

    
def Compute_Volume_Fractions (box):
    assert (type(Box) == np.ndarray), 'Input "Box" must be a numpy.ndarray'
    Air_Voxels  = np.count_nonzero((box==0))  #Air
    OrM_Voxels  = np.count_nonzero((box==1)) #OrM
    Grv_Voxels  = np.count_nonzero((box==2)) #Grv
    Test =  ( np.product(box.shape) == ( Air_Voxels + OrM_Voxels + Grv_Voxels ) )   
    if not (Test == True):
        raise AssertionError("You must verify that all phase-labels in Box are 0, or, 128, or 255")
    Volume      =  Air_Voxels + OrM_Voxels + Grv_Voxels
    VF_Air, VF_OrM, VF_Grv = Air_Voxels/Volume, OrM_Voxels/Volume, Grv_Voxels/Volume
    return (VF_Air, VF_OrM, VF_Grv)

# Creating list of boxes 
# First way
create_list_of_boxes()
# Second way
create_list_of_boxesV2()

print('Computing Volume Fractions:  ')
# Way 1
print    ('Way 1: For_loop   (microseconds): ' , str(duration))
start    = datetime.datetime.today()
VF       = [[Compute_Volume_Fractions(Box) for Box in list_of_boxes [i]] for i in range (number_radii)]
VF       = np.array(VF)
end      = datetime.datetime.today()
duration = end - start
duration = str(duration)

# Way 2
print    ('Way 2: For + Map  (microseconds): ' , str(duration))
start    = datetime.datetime.today()
VF2      = [list(map(Compute_Volume_Fractions, list_of_boxes[i])) for i in range (number_radii) ]
VF2      = np.array(VF2)
end      = datetime.datetime.today()
duration = end - start
duration = str(duration)

# Way 3
print    ('Way 3: Map Only   (microseconds): ' , str(duration))
start    = datetime.datetime.today()
VF3      = list(map(Compute_Volume_Fractions, Frame['Boxes']))
VF3      = np.array(VF3)
end      = datetime.datetime.today()
duration = end - start
duration = str(duration)


print ('ok?:  ', np.alltrue(VF ==VF2))